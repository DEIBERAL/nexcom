<?php

$services[] =array(
		'nombre'=>'Cargar producto Cesta',
		'url'=>'services/Cesta_serv/addCesta_serv',
		'descripcion'=>'Carga un producto a la cesta',
		'color'=>'',
		'campos'=>array(
			array('nombre'=>'producto_id','valor'=>'1','info'=>'Id del producto'),
			array('nombre'=>'cantidad','valor'=>'1','info'=>'Cantidad de producto a cargar')
		)
);

$services[] =array(
		'nombre'=>'Mostrar total carrito compras',
		'url'=>'services/Cesta_serv/totalcarrito_serv',
		'descripcion'=>'',
		'color'=>'',
		'campos'=>array(
		)
);

$services[] =array(
		'nombre'=>'Cantidad Productos cesta',
		'url'=>'services/Cesta_serv/cantidadProd_serv',
		'descripcion'=>'Muestra Cantidad Productos Cesta',
		'color'=>'',
		'campos'=>array(
		)
);

$services[] =array(
		'nombre'=>'Carga multiples datos al carrito',
		'url'=>'services/Cesta_serv/cargaProd_serv',
		'descripcion'=>'',
		'color'=>'',
		'campos'=>array(
			array('nombre'=>'producto','valor'=>'producto_1=12&producto_2=5','info'=>'ID_Cantidad'),
		)
);

$services[] =array(
		'nombre'=>'Vaciar Cesta',
		'url'=>'services/Cesta_serv/vaciarCesta_serv',
		'descripcion'=>'',
		'color'=>'',
		

);

?>
