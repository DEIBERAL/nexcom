<?php
session_start();

function viendoModulo($pagina){
    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    if (strpos($actual_link, $pagina) > -1) {
        return true;
    }
    return false;
}

/*
  RETURN JSON
  Rerotnar la respuesta en JSON
  $res: True,False,Null	| Si es positiva o Negatico el resultado del JSON
*/
function getUserId() {
    return $_SESSION['idUser'];
    //return 1;
}

function meses($mes,$corto=false)
{
  $data = array(1=>"Enero",2=>"Febrero",3=>"Marzo",4=>"Abril",5=>"Mayo",6=>"Junio",7=>"Julio",8=>"Agosto",9=>"Septiembre",10=>"Octubre",11=>"Noviembre",12=>"Diciembre");
  if ($corto) {
    return substr($data[$mes],0,3);
  }else{
    return $data[$mes];
  }
}
function getUserTipo()
{
  // return 1;
  return $_SESSION['userCurrent']['tipo_id'];
}
function is_session()
{
   if ($_SESSION['userCurrent']) {
      return true;
   }
    return false;

}

function is_login()
{
   if ($_SESSION['userCurrent']) {
      return true;
   }
   redirect('login/Logingest');
}

function nombre_usuario_id($id_usuario) {
    $CI = get_instance();
    $CI->db->from('usuarios');
    $CI->db->where('usuario_id', $id_usuario);
    $usuario = $CI->db->get()->row_array();
    return $usuario['pri_nombre'] . ' ' . $usuario['pri_apellido'];
}

/*
  RETURN JSON
  Rerotnar la respuesta en JSON
  $res: True,False,Null	| Si es positiva o Negatico el resultado del JSON
 */

function returnJson($res = false, $datos = array(), $msg='') {
    echo json_encode(array('res' => $res, 'dataObj' => $datos, 'msg' => $msg));
}

function pre($vl){
	echo '<pre>';
		print_r($vl);
	echo '</pre>';
}

function iSql($vl){
	return 	$vl;
}

?>