<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GestAdminuser extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
		$this->load->model('productos/Cesta_model');
		$this->load->model('usuarios/Usuarios_model');
    }
	
	public function compras(){
		$cantidadProd = $this->Cesta_model->cantidadProd('directo');
		$this->template
		->set('cantidadProd',$cantidadProd)
		->set_layout('adminUser')
		->build('administradorUser/compras');
	}
	

	
	
	public function form_pago(){
		$is_login = $_SESSION['allData']; 
		$cantidadProd = $this->Cesta_model->cantidadProd('directo');
		$datos = $this->Cesta_model->totalCarrito();
		if($is_login){
		$this->template
		->set('is_login',$is_login) 
		->set('cantidadProd',$cantidadProd)
		->set('datos',$datos[1])
		->set_layout('principal')
		->build('productos/form_pago');
		}else{
			$this->template
			->set('cantidadProd',$cantidadProd)
			->set_layout('principal')
			->build('usuarios/login');
		}
	}
	
	public function carrito(){
		$cantidadProd = $this->Cesta_model->cantidadProd('directo');
		$datos = $this->Cesta_model->totalCarrito();
		$this->template
		->set('cantidadProd',$cantidadProd)
		->set('datos',$datos[1])
		->set_layout('principal')
		->build('productos/vercarrito');
		
	}
	public function ListCatProductos($idCategoria){	
		$cantidadProd = $this->Cesta_model->cantidadProd('directo');
		$productos = $this->Productos_model->obtenerDatosCategorias($idCategoria);
		$cat = $this->Productos_model->listarCategorias();
		$this->template
		->set('cantidadProd',$cantidadProd)
		->set('cat',  $cat[1])
		->set('productos',  $productos[1])
		->set_layout('principal')
		->build('productos/listadoCategorias');
	}
	
	

}