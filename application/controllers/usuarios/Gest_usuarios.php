<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Gest_usuarios extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
		$this->load->model('usuarios/Usuarios_model');
		$this->load->model('productos/Cesta_model');
    }
	
	public function registro(){
		$cantidadProd = $this->Cesta_model->cantidadProd('directo');
		$this->template
		->set('cantidadProd',$cantidadProd)
		->set_layout('principal')
		->build('registro/registro');
	}
	
	public function login(){
		$cantidadProd = $this->Cesta_model->cantidadProd('directo');
		$this->template
		->set('cantidadProd',$cantidadProd)
		->set_layout('principal')
		->build('registro/login');
	}
	
	public function compras(){
		
		$listar = $this->Usuarios_model->listarCompras();
		$this->template
		->set('listar',$listar)
		->set_layout('adminUser')
		->build('administradorUser/compras');
		
	}
	public function detalle($idCompra){
        $listar= $this->Usuarios_model->detalleCompra($idCompra);
        $compra= $this->Usuarios_model->compra($idCompra);
        $this->template
            ->set('listar',$listar)
            ->set('compra',$compra)
            ->set_layout('adminUser')
            ->build('administradorUser/detalleCompra');
    }
	
	public function salir(){
		unset($SESSION);
		session_destroy();
		
		header('Location:../Gest_usuarios/login');
	}
	
}