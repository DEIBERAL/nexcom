<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GestLogin extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
    }
	
	public function login(){
		
			$this->template
			->set_layout('login')
	        ->build('login/login');
	}

}