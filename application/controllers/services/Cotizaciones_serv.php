<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header('content-type: application/json; charset=utf-8');

class Cotizaciones_serv extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('cotizaciones/Cotizaciones_model');
    }


	
	public function registrarCotizacion_serv(){
		
		$usuario_id  = $this->input->post('usuario_id');
		$nombre 	 = $this->input->post('nombre');
		$apellido	 = $this->input->post('apellido');
		$pais 		 = $this->input->post('pais');
		$localidad   = $this->input->post('localidad');
		$region    = $this->input->post('region');
		$direccion 	 = $this->input->post('direccion');
		$telefono    = $this->input->post('telefono');
		$correo      = $this->input->post('correo');
		$descripcion 	 = $this->input->post('descripcion');
			
		
		$result = $this->Cotizaciones_model->gestCotizacion($usuario_id,$nombre,$apellido,$pais,$localidad,$region,$direccion,$telefono,$correo,$descripcion);
		returnJson($result[0],$result[1],$result[2]);
	 }
	
	public function ListarCotizacion_serv($idUser){
		
		$idUser  = $this->input->post('usuario_id');
		$result = $this->Cotizaciones_model->cotizacionesCliente($idUser);
		returnJson($result[0],$result[1],$result[2]);
		
	}
	
	public function InfoCotizaciones_serv($idCompra){
		
		$idCompra  = $this->input->post('compra_id');
		$result = $this->Cotizaciones_model->detalleCotizaciones($idCompra);
		returnJson($result[0],$result[1],$result[2]);
		
	}
}


