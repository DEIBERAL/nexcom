<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header('content-type: application/json; charset=utf-8');

class Cesta_serv extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('productos/Cesta_model');
    }
	
	public function addCesta_serv($idProd,$cantidad){
	 	$idProd = $this->input->post('producto_id');
	 	$cantidad = $this->input->post('cantidad');
		$result = $this->Cesta_model->addProducto($idProd,$cantidad);
		returnJson($result[0],$result[1],$result[2]);
		

	 }
	
	public function totalcarrito_serv(){
		$result = $this->Cesta_model->totalCarrito();
		returnJson($result[0],$result[1],$result[2]);
	}
	
	public function cantidadProd_serv(){
		$result = $this->Cesta_model->cantidadProd();
		returnJson(true,$result,'ok');
	}
	
		
	public function vaciarCesta_serv(){
		$this->Cesta_model->vaciar();
		returnJson(true,array(),'ok');	
	}
	
	public function cargaProd_serv(){
		$idProdCantd = $this->input->post('producto');
		$result = $this->Cesta_model->actualizarProd($idProdCantd);
		returnJson($result[0],$result[1],$result[2]);
	}
	
	

}
