<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header('content-type: application/json; charset=utf-8');

class Compras_serv extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('compras/Compras_model');
    }
	 public function listarComprasClient_serv($iduser){ 
		 $iduser = $this->input->post('usuario_id');
		 $result = $this->Compras_model->comprasCliente($iduser);
		 returnJson($result[0],$result[1],$result[2]);
	 }
	
		public function detallecompras_serv($compra_id){ 
		 $compra_id = $this->input->post('compra_id');
		 $result = $this->Compras_model->detalleCompras($compra_id);
		 returnJson($result[0],$result[1],$result[2]);
	}
	
	


}

