<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header('content-type: application/json; charset=utf-8');

class Pago_serv extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('pago/Pago_model');
    }

	public function registrarCompra_serv(){
		
		$usuario_id  = $this->input->post('usuario_id');
		$nombre 	 = $this->input->post('nombre');
		$apellido	 = $this->input->post('apellido');
		$empresa 	 = $this->input->post('empresa');
		$pais 		 = $this->input->post('pais');
		$direccion   = $this->input->post('direccion');
		$localidad   = $this->input->post('localidad');
		$region    = $this->input->post('region');
		$telefono    = $this->input->post('telefono');
		$correo      = $this->input->post('correo');
		
	
		
		$result = $this->Pago_model->gestRegistroPago($usuario_id,$nombre,$apellido,$empresa,$pais,$direccion,$localidad,$region,$telefono,$correo);
		returnJson($result[0],$result[1],$result[2]);
	 }
	
		
}


