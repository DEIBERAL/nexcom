<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header('content-type: application/json; charset=utf-8');

class Productos_serv extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('productos/Productos_model');
    }

	public function listarproductos_serv(){
		$result = $this->Productos_model->listarProductos();
		returnJson($result[0],$result[1],$result[2]);
	 }
	
	public function listarcategorias_serv(){
		$result = $this->Productos_model->listarCategorias();
		returnJson($result[0],$result[1],$result[2]);
	 }
	
	 public function obtenerDatosCategorias_serv($idcat){
	 	$idcat = $this->input->post('categoria_id');
		$result = $this->Productos_model->obtenerDatosCategorias($idcat);
		returnJson($result[0],$result[1],$result[2]);

	 }
	
	 public function obtenerInfoProducto_serv($idProd){
	 	$idProd = $this->input->post('producto_id');
		$result = $this->Productos_model->obtenerInfoProducto($idProd);
		return returnJson($result['resp'], $result['data']);

	 }

}
