<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header('content-type: application/json; charset=utf-8');

class Usuarios_serv extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('usuarios/Usuarios_model');
    }

	public function registrar_serv(){
		
		$idEdit = $this->input->post('idEdit');
		$rol_id  = $this->input->post('rol_id');
		$nombre = $this->input->post('nombre');
		$apellido = $this->input->post('apellido');
		$cedula = $this->input->post('cedula');
			$telefono = $this->input->post('telefono');
		$ciudad = $this->input->post('ciudad');
		$direccion = $this->input->post('direccion');
		$correo = $this->input->post('correo');
		$password = md5($this->input->post('password'));
		
		$result = $this->Usuarios_model->gestRegistro($idEdit,$rol_id,$nombre,$apellido,$cedula,$telefono,$ciudad,$direccion,$correo,$password);
		returnJson($result[0],$result[1],$result[2]);
	 }
	
	
	public function login_serv(){  
		$username = $this->input->post('correo');
		$password = $this->input->post('password');
		$rs = $this->Usuarios_model->login($username,$password);
		returnJson($rs[0],$rs[1],$rs[2]);
	}
	
	public function listarCompras_serv($iduser){ 
		 $iduser = $this->input->post('usuario_id');
		 $result = $this->Usuarios_model->listarCompras($iduser);
		 returnJson($result[0],$result[1],$result[2]);
	}
	
	public function Obtenerdatoscliente_Serv($id_usuario){
		
	 	$id_usuario = $this->input->post('id_usuario');
		$result = $this->Usuarios_model->obtenerDatosCliente($id_usuario);
		return returnJson($result['resp'], $result['data']);

	}
	
	public function recuperarPassword_Serv(){
		
		$username = $this->input->post('username');
		$result = $this->Usuarios_model->recuperarContrasena($username);

		return returnJson($result['resp'], $result['datos'], $result['data']);
	}


}

