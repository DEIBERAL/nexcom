<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function admin(){

        $this->template
            ->set_layout('adminUser')
            ->build('panel/admin');
    }

    public function cliente(){

        $this->template
            ->set_layout('adminUser')
            ->build('panel/cliente');
    }

}