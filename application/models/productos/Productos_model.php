<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Productos_model extends CI_Model{
	
	public function __construct(){
		parent::__construct();
	}
	
		
	public function listarProductos(){
		
		$this->db->select('*');    
		$this->db->from('productos');
		$this->db->limit(6);
 		$productos = $this->db->get()->result_array();
		$info = array();
		foreach($productos as $cl => $vl){
			
			$info[] = $vl;
		}
		return array(true, $info, 'Listado de productos');
	}
	
	public function infoProducto($productoId){
		$infoProd = $this->db->get_where('productos',array('producto_id'=>$productoId))->result_array();
		foreach($infoProd as $cl => $vl){
			
			return $infoProd[0] = $vl;
		}
		
	}
	
	
	public function listarCategorias(){
		$arbol = array();
		$this->db->select('*');
		$this->db->from("categorias");
		$this->db->where('padre',0);
		$categorias = $this->db->get()->result_array();
		$data = array();
		foreach($categorias as $cl => $vl){
			// Hijas del pladre
			$vl['hijas'] = $this->db->get_where('categorias',array('padre'=>$vl['categorias_id']))->result_array();
			$arbol[] = $vl;
		}
		
		
		return array(true,$arbol, 'Listado de categorias');
	}
	
	
	
	public function obtenerDatosCategorias($idcategoria){

		$this->db->select('cat.padre as idpadre,cat.categorias_id as idCategoria, cat.nombre as nombreCategoria, Prod.nombre as nombreProd, Prod.descripcion as descripciónProd,Prod.precio as precioProd,Prod.producto_id as producto_id,Prod.img1 as img1 ');
		$this->db->from('categorias as cat');
		$this->db->join("productos as Prod" ,"cat.categorias_id = Prod.categorias_id");
		$this->db->where('cat.categorias_id', $idcategoria);
		$consult = $this->db->get()->result_array();
		$data = array();
		foreach($consult as $cl => $vl){
			$data[] = $vl;
		}
		return array(true,$data, 'Listado de categorias');
		
	}
	
	public function obtenerInfoProducto($idProd){

		$this->db->select('*');
		$this->db->from('productos');
		$this->db->where('producto_id', $idProd);
		$consult = $this->db->get()->row_array();
		if ($consult) {
			
			$data['producto_id'] = $consult['producto_id'];
			$data['nombre']      = $consult['nombre'];
			$data['precio']      = $consult['precio'];
			$data['descripcion'] = $consult['descripcion'];
			$data['img1']        = $consult['img1'];
			$data['img2']        = $consult['img2'];
			$data['img3']        = $consult['img3'];
			
			
			
			$result['resp'] = true;
			$result['data'] = $data;
		}else{

			$result['resp'] = false;
			$result['data'] = 'No se encontraron Datos';
		}
		return $result;
		
	}
	
	

}