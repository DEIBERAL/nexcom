<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cesta_model extends CI_Model {


	public function __construct()
	{
		parent::__construct();
		$this->load->model('productos/Productos_model');
	}
	
	function puntos($precio){
		return number_format($precio,0,',','.');
	}
	
	
	public function totalCarrito(){
		$productos = !empty($_SESSION['CarritoCompras'])?$_SESSION['CarritoCompras']:array();
		$infoProds = array();
		$iva =0;
		$subTotal=0;
		$total=0;
		foreach($productos as $idProducto => $cantidad){
			$infoProd = $this->Productos_model->infoProducto($idProducto);
			$vl['cantidad'] = $cantidad;
			$vl['productoId'] = $idProducto;
			$vl['iva'] = ($infoProd['precio'] * $cantidad ) / 100;
			$vl['total'] = (int) $infoProd['precio'] * $cantidad;
			$vl['infoGeneral'] = $infoProd;
			$infoProds[$idProducto] = $vl;
			$iva += $vl['iva'];
			$subTotal +=  $vl['total'];
			$total += $vl['iva'] + $vl['total'];
		}
		if($infoProds){ 
		
		$return = array(true,
            array($infoProds,
                array('iva' =>$iva),
                array('subtotal' => $subTotal),
                array('total' => $total)
            ),'listado de productos');
		return $return;
		die();
			}else{
			
			return array(true,array(),'no hay datos');
		}
	}
	
	public  function addProducto($productoId,$cantidad){
		
		if(!$cantidad){$cantidad=1;}
		$_SESSION['CarritoCompras'][$productoId] = $cantidad;
		
		// actualizar 
		return array(true, $_SESSION['CarritoCompras'] ,'Producto agregado');
	}

	
	public  function cantidadProd($tipo=false){
		$return = array();
        $result = $this->Cesta_model->totalCarrito();
        $total=0;
        $sum=0;
        if($result[1][3]['total']>0){
            $total=$result[1][3]['total'];
            $sum=array_sum($_SESSION['CarritoCompras']);
        }
		if($tipo=='directo'){

		$return['cant']= $sum;
		$return['total']=$total;
		}else{
            $return['cant']= $sum;
            $return['total']=$total;
        }
		return $return;
	}
	
	public function actualizarProd($datosCesta){
		$data = array();
		parse_str($datosCesta,$info);
		foreach($info as $cl => $cantidad){
			$info = explode('_',$cl);
			$productoId  = $info[1];
			$this->addProducto($productoId,$cantidad);
			$infoProd = $this->totalCarrito($productoId);
		}
		
		$data[0] = $_SESSION['CarritoCompras'];
		$data[1] = serialize($data[0]);
		//return array(true, $data ,'Producto cargado con exito');
		return array(true, $infoProd ,'Producto cargado con exito');
	
	}
	
	public function vaciar(){
		unset($_SESSION['CarritoCompras']);
		$_SESSION['CarritoCompras'] = array();
	}

}