<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Panel_model extends CI_Model{
	
	public function __construct(){
		parent::__construct();
	}


    public function listarComprasGeneral(){


            $this->db->select('com.compra_id,
                               com.fechar,       
                               com.valor_total,
                               SUM(det.cantidad) items,
                               us.usuario_id,
                               CONCAT()',
                                );
            $this->db->from('det');
            $this->db->join("com","com.compra_id = det.compra_id");
            $this->db->join("us","us.usuario_id = com.usuario_id");
            $this->db->where('com.tipo', 1);
            $this->db->group_by("com.compra_id");
            $this->db->order_by('com.fechar', 'DESC');
            $consult = $this->db->get()->result_array();
            if($consult){
                $result['resp'] = true;
                $result['data'] = $consult;
            }else{
                $result['resp'] = false;
                $result['data'] = 'No se encontraron Datos';
            }
            return $result;

    }
}