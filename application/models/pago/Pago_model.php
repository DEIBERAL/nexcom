<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pago_model extends CI_Model{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('productos/Cesta_model');
	
	}
	

	public function gestRegistroPago($usuario_id,$nombre,$apellido,$empresa,$pais,$direccion,$localidad,$region,$telefono,$correo){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('usuario_id', 	'usuario_id', 'required');
			$this->form_validation->set_rules('nombre', 	'nombre', 'required');
			$this->form_validation->set_rules('apellido', 	'apellido', 'required');
			$this->form_validation->set_rules('empresa', 'empresa', 'required');
			$this->form_validation->set_rules('pais', 'pais', 'required');
			$this->form_validation->set_rules('direccion',  'direccion', 'required');
			$this->form_validation->set_rules('localidad',    'localidad', 'required');
			$this->form_validation->set_rules('region',    'region', 'required');
			$this->form_validation->set_rules('telefono',    'telefono', 'required');
			$this->form_validation->set_rules('correo',    'correo', 'required');
			$this->form_validation->set_message('required',  'Debe completar el campo %s.');	
			if ($this->form_validation->run() == FALSE){
				returnJson(false, array(), validation_errors());
			}else{
				 $productos = $this->Cesta_model->totalCarrito();
				 $this->db->set('fechar','NOW()',FALSE);
				 $this->db->insert('compras', array(
					'usuario_id'=>$usuario_id,
					'nombre'	=>$nombre,
					'apellido'	=>$apellido,
					'empresa'   =>$empresa,
					'pais'	    =>$pais,
					'direccion'	=>$direccion,
					'localidad'	=>$localidad,
					'region'	=>$region,
					'telefono'	=>$telefono,
					'correo'	   =>$correo,
					//'valor_total'  =>$valor_total,
					'valor_total'=>$productos[1][3]['total'],
					'tipo'=>1,
				));	
				
				$idCompra = $this->db->insert_id();
				foreach($productos[1][0] as $cl => $infoProd){
					//pre($productos[1][0]);
				//die();
					$this->db->insert('com_detalle',array('compra_id'=>$idCompra,'producto_id'=>$infoProd['productoId'],'cantidad'=>$infoProd['cantidad'],'valor'=>$infoProd['total']));
				}
				 $this->Cesta_model->vaciar();
				 return array(true, array(), 'compras  guardada con exito');
			}	
		}
	
	
	
	
}