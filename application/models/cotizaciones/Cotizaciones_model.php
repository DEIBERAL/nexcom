<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cotizaciones_model extends CI_Model{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('productos/Cesta_model');
	}
	
	public function gestCotizacion($usuario_id,$nombre,$apellido,$pais,$localidad,$region,$direccion,$telefono,$correo,$descripcion){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('usuario_id', 	'usuario_id', 'required');
			$this->form_validation->set_rules('nombre', 	'nombre', 'required');
			$this->form_validation->set_rules('apellido', 	'apellido', 'required');
			$this->form_validation->set_rules('pais', 'pais', 'required');
			$this->form_validation->set_rules('direccion',  'direccion', 'required');
			$this->form_validation->set_rules('localidad',    'localidad', 'required');
			$this->form_validation->set_rules('region',    'region', 'required');
			$this->form_validation->set_rules('telefono',    'telefono', 'required');
			$this->form_validation->set_rules('correo',    'correo', 'required');
			$this->form_validation->set_message('required',  'Debe completar el campo %s.');	
			if ($this->form_validation->run() == FALSE){
				returnJson(false, array(), validation_errors());
			}else{
				
					if(!$usuario_id){
						$usuario_id = getUserId();
					}
				if($usuario_id){
				 $productos = $this->Cesta_model->totalCarrito();
				 $this->db->set('fechar','NOW()',FALSE);
				 $this->db->insert('compras', array(
					'usuario_id'=>$usuario_id,
					'nombre'	=>$nombre,
					'apellido'	=>$apellido,
					'pais'	    =>$pais,
					'localidad'	=>$localidad,
					'region'	=>$region,
					 'direccion'=>$direccion,
					'telefono'	=>$telefono,
					'correo'	   =>$correo,
					
					//'valor_total'  =>$valor_total,
					'valor_total'=>$productos[1][3]['total'],
					'tipo'=>2,
					 'descripcion'	   =>$descripcion,
				));	
				
				$idCompra = $this->db->insert_id();
				foreach($productos[1][0] as $cl => $infoProd){
					//pre($productos[1][0]);
				//die();
					$this->db->insert('com_detalle',array('compra_id'=>$idCompra,'producto_id'=>$infoProd['productoId'],'cantidad'=>$infoProd['cantidad'],'valor'=>$infoProd['total']));
				}
				 $this->Cesta_model->vaciar();
				 return array(true, array(), 'compras  guardada con exito');
			}
		}	
	}
	
		public function cotizacionesCliente($getUserId){
		if(!$getUserId){
			$getUserId = getUserId();
		}
		
		if($getUserId){
		 	$this ->db->select("*");
			$this->db->from("compras as Comp");
			$this->db->join('usuarios as User' , 'User.usuario_id = Comp.usuario_id');
			$this->db->where('User.rol_id', 2);
			$this->db->where('Comp.tipo', 2);
			$this->db->where('Comp.usuario_id', $getUserId);
			$data = $this->db->get()->result_array();
			$info = array();
			foreach($data as $cl => $vl){
				$info[] = $vl;
			}
			return array(true, $info, 'Listado de cotizaciones');
			
		}
		
	}
	
		public function detalleCotizaciones($idCompra){
		
		$this ->db->select("Conmdetalle.*,Comp.*");
		$this->db->from("compras as Comp");
		$this->db->join('usuarios as User' , 'User.usuario_id = Comp.usuario_id');
		$this->db->join('com_detalle as Conmdetalle' , 'Comp.compra_id = Conmdetalle.compra_id');
		$this->db->join('productos as Prod' , 'Prod.producto_id = Conmdetalle.producto_id');
		$this->db->where('Comp.tipo',2);
		$this->db->where('Conmdetalle.compra_id', $idCompra);
		$data = $this->db->get()->result_array();
			$info = array();
		
			foreach($data as $cl => $vl){
				$info[] = $vl;
			}
			return array(true, $info, 'Detalle de  las cotizaciones');
		
	}
	
		
}