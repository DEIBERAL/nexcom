<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios_model extends CI_Model{
	
	public function __construct(){
		parent::__construct();
	}
	

	public function gestRegistro($idEdit = null,$rol_id,$nombre,$apellido,$cedula,$telefono,$ciudad,$direccion,$correo,$password){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('nombre', 	'Nombre', 'required');
			$this->form_validation->set_rules('apellido', 	'Apellido', 'required');
			$this->form_validation->set_rules('cedula', 'Cedula', 'required');
			$this->form_validation->set_rules('telefono', 'Telefono', 'required');
			$this->form_validation->set_rules('ciudad',     'Ciudad', 'required');
			$this->form_validation->set_rules('direccion',  'Direccion', 'required');
			$this->form_validation->set_rules('correo',    'Correo', 'required');
			//$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_message('required',  'Debe completar el campo %s.');
		
		
			if ($this->form_validation->run() == FALSE){
				returnJson(false, array(), validation_errors());
			}else{
					if($idEdit){

					$this->db->where('usuario_id', $idEdit);
					$this->db->update('usuarios', array(
						
						'rol_id'	=>$rol_id,
						'nombre'	=>$nombre,
						'apellido'	=>$apellido,
						'cedula'    =>$cedula,
						'telefono'	=>$telefono,
						'ciudad'	=>$ciudad,
						'direccion'	=>$direccion,
						'correo'	=>$correo,
						'password'	=>$password,
					));				
			
					return array(true, array(), 'usuario editado con exito');
				
				}else{
				
				$existeCorreo = $this->db->get_where('usuarios',array('correo'=>$correo))->num_rows();
					if($existeCorreo)return array(false, array(), 'el correo ya existe');
					$this->db->set('fechar','NOW()',FALSE);
					$this->db->insert('usuarios', array(
						
						'rol_id'	=>$rol_id,
						'nombre'	=>$nombre,
						'apellido'	=>$apellido,
						'cedula'    =>$cedula,
						'telefono'	=>$telefono,
						'ciudad'	=>$ciudad,
						'direccion'	=>$direccion,
						'correo'	=>$correo,
						'password'	=>$password,
					));				
						
					return array(true, array(), 'usuario guardado con exito');
				}
			}	
		}
	
		public function recuperarContrasena($username){
		
			$correo = $this->getDataUser($username);

			$nuevoPassword = rand(100000, 999999);

			$this->load->library('email');
			$config['mailtype'] = 'html';
			$this->email->initialize($config);

			$this->email->from($correo, 'Nexcom');
			$this->email->to($correo);

			$this->email->subject('Recuperación de contraseña');
			$this->email->message('Su nueva contraseña de Nexcom   es la siguiente <b>'.$nuevoPassword.'</b>');

			if ($this->email->send()) {
				$this->changePassword($correo, $nuevoPassword);
				$result['resp'] = true;
				$result['data'] = 'La nueva contraseña ha sido enviada a su correo';
				return $result;
			}else{
				$result['resp'] = false;
				$result['data'] = 'No hemos podido contactarnos con usted';
				return $result;
			}
	}
	
	private function getDataUser($username){
		$this->db->select('correo');
		$this->db->from('usuarios');
		$this->db->where('correo', $username);
		$consult = $this->db->get()->row_array();
		if ($consult) {
			return $consult['correo'];
		}else{
			return false;
		}
	}
	
		public function login($username,$password){

		$this->form_validation->set_rules('correo','Correo', 'trim|required|valid_email');
		$this->form_validation->set_rules('password',  'Password', 'trim|required|min_length[3]');
		$this->form_validation->set_message('required', 'Debe completar el campo %s.');

		$r = $this->form_validation->run();
		if (!$r){
        	return array(false,array(),validation_errors());
        }
		
		$this->db->where('correo', $username);
		$this->db->where('password', md5($password));
		$existe = $this->db->get('usuarios')->row_array();
		if($existe){
			$_SESSION['userActive'] = true;
			$_SESSION['userCurrent'] = $existe;
			$_SESSION['idUser'] = $existe['usuario_id'];
			$_SESSION['allData'] = $existe;
			$existe['token'] =  'XXX-'.$existe['usuario_id'].'-XXX';
			$existe['tipoUser'] =  $existe['rol_id'];
			$existe['UsuarioId'] =  $existe['usuario_id'];
			return array(true,$existe,'Bienvenido');					
		}else{
			return array(false,array(),'No se identifico el usuario');
		}
	}
	
	public function obtenerDatosCliente($getUserId){
	
			if(!$getUserId){
			$getUserId = getUserId();
		}
		
		if($getUserId){
			
			$this->db->select("*");
			$this->db->from("usuarios");
			$this->db->where("usuario_id",$getUserId);
			$consult = $this->db->get()->row_array();
			if ($consult) {
			$data['usuario_id']	 = $consult['usuario_id'];
			$data['nombre'] 	 = $consult['nombre'];
			$data['apellido'] 	 = $consult['apellido'];
			$data['telefono']		 = $consult['telefono'];
			$data['ciudad']		 = $consult['ciudad'];
			$data['direccion'] 	 = $consult['direccion'];
			$data['correo'] 	 = $consult['correo'];
			$data['direccion'] 	 = $consult['direccion'];
			$result['resp'] = true;
			$result['data'] = $data;
		}else{

			$result['resp'] = false;
			$result['data'] = 'No se encontraron Datos';
		}
		return $result;
		}
	}

	public function listarCompras(){
        $getUserId = getUserId();
        if($getUserId){
            $this->db->select('compras.compra_id,
                               compras.fechar,       
                               compras.valor_total,
                               SUM(com_detalle.cantidad) items');
            $this->db->from('com_detalle');
            $this->db->join("compras","compras.compra_id = com_detalle.compra_id");
            $this->db->where("compras.usuario_id",$getUserId);
            $this->db->where('compras.tipo', 1);
            $this->db->group_by("compras.compra_id");
            $this->db->order_by('compras.fechar', 'DESC');
            $consult = $this->db->get()->result_array();
            if($consult){
                $result['resp'] = true;
                $result['data'] = $consult;
            }else{
                $result['resp'] = false;
                $result['data'] = 'No se encontraron Datos';
            }
    return $result;
        }
    }

    public function compra($id){
        $getUserId = getUserId();
        if($getUserId){
            $this->db->select('compras.compra_id,
                               compras.fechar,       
                               compras.valor_total,
                               SUM(com_detalle.cantidad) items');
            $this->db->from('com_detalle');
            $this->db->join("compras","compras.compra_id = com_detalle.compra_id");
            $this->db->where("compras.usuario_id",$getUserId);
            $this->db->where('compras.compra_id', $id);
            $this->db->group_by("compras.compra_id");
            $consult = $this->db->get()->row_array();
            if($consult){
                $data['id']	 = $consult['compra_id'];
                $data['fecha'] 	 = $consult['fechar'];
                $data['total'] 	 = $consult['valor_total'];
                $data['items']		 = $consult['items'];

                $result['resp'] = true;
                $result['data'] = $data;
            }else{
                $result['resp'] = false;
                $result['data'] = 'No se encontraron Datos';
            }
            return $result;
        }
    }


    public function detalleCompra($id){
        $getUserId = getUserId();
        if($getUserId && $id){
            $this->db->select('det.cantidad,det.valor,pro.nombre producto, cat.nombre categoria, pro.img1');
            $this->db->from('com_detalle AS det');
            $this->db->join('productos AS pro','pro.producto_id = det.producto_id');
            $this->db->join('categorias AS cat','cat.categorias_id = pro.categorias_id');
            $this->db->where('det.compra_id',$id);
            $consult = $this->db->get()->result_array();
            if($consult){
                $result['resp'] = true;
                $result['data'] = $consult;
            }else{
                $result['resp'] = false;
                $result['data'] = 'No se encontraron Datos';
            }
            return $result;
        }
    }

}