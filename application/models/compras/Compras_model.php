<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Compras_model extends CI_Model{
	
	public function __construct(){
		parent::__construct();
	}

		
	public function comprasCliente($getUserId){
		if(!$getUserId){
			$getUserId = getUserId();
		}
		
		if($getUserId){
		 	$this ->db->select("*");
			$this->db->from("compras as Comp");
			$this->db->join('usuarios as User' , 'User.usuario_id = Comp.usuario_id');
			//$this->db->join('com_detalle as Conmdetalle' , 'Comp.compra_id = Conmdetalle.compra_id');
			//$this->db->join('productos as Prod' , 'Prod.producto_id = Conmdetalle.producto_id');
			$this->db->where('User.rol_id', 2);
			$this->db->where('Comp.tipo', 1);
			
			$this->db->where('Comp.usuario_id', $getUserId);
			
			//$this->db->order_by("Comp.usuario_id", "desc");
			$data = $this->db->get()->result_array();
			$info = array();
		
			foreach($data as $cl => $vl){
				$info[] = $vl;
			}
			return array(true, $info, 'Listado de compras');
			
		}
		
	}
	
	public function detalleCompras($idCompra){
		
		$this ->db->select("Conmdetalle.*,Comp.*");
		$this->db->from("compras as Comp");
		$this->db->join('usuarios as User' , 'User.usuario_id = Comp.usuario_id');
		$this->db->join('com_detalle as Conmdetalle' , 'Comp.compra_id = Conmdetalle.compra_id');
		$this->db->join('productos as Prod' , 'Prod.producto_id = Conmdetalle.producto_id');
		$this->db->where('Conmdetalle.compra_id', $idCompra);
		$data = $this->db->get()->result_array();
			$info = array();
		
			foreach($data as $cl => $vl){
				$info[] = $vl;
			}
			return array(true, $info, 'Detalle de  la compra');
		
	}
	
	// Compras del cliente
	
	public function comprasClientesss($getUserId){
		if(!$getUserId){
			$getUserId = getUserId();
		}
		
		if($getUserId){
		 	$this ->db->select("User.usuario_id as UsuarioId,Prod.img1 as imagen, Prod.nombre as nombreProducto, Prod.precio as precioProd,Conmdetalle.cantidad as cantProd, Conmdetalle.valor as total,   Comp.fechar as fecha,Comp.compra_id as compra_id");
			$this->db->from("compras as Comp");
			$this->db->join('usuarios as User' , 'User.usuario_id = Comp.usuario_id');
			$this->db->join('com_detalle as Conmdetalle' , 'Comp.compra_id = Conmdetalle.compra_id');
			$this->db->join('productos as Prod' , 'Prod.producto_id = Conmdetalle.producto_id');
			$this->db->where('User.rol_id', 2);
			
			$this->db->where('Comp.usuario_id', $getUserId);
			
			//$this->db->order_by("Comp.usuario_id", "desc");
			$data = $this->db->get()->result_array();
			$info = array();
		
			foreach($data as $cl => $vl){
				$info[] = $vl;
			}
			return array(true, $info, 'Listado de compras');
			
		}
		
	}
	
	

		
}