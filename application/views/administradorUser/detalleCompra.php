<div id="detalleCompras" class="row">
<div class="container">
    <ul class="breadcrumb">
        <li><a href="<?php echo base_url('usuarios/Gest_usuarios/compras') ?>">Listado</a></li>
        <li>Compra #  <?php echo $compra['data']['id']; ?></li>
    </ul>
    <h2>Informacion de la compra # <?php echo $compra['data']['id']; ?></h2>
    <p><b>Fecha:</b> <?php echo $compra['data']['fecha']; ?></p>
    <p><b>Total:</b> $ <?php echo number_format($compra['data']['total'],2,'.',','); ?></p>
    <p><b>Items comprados:</b> <?php echo $compra['data']['items']; ?></p>
<h3>Detalle</h3>
    <table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">Imagen</th>
      <th scope="col">Producto</th>
      <th scope="col">Categoria</th>
      <th scope="col">Cantidad</th>
      <th scope="col">Valor</th>


    </tr>
  </thead>
  <tbody>
  <?php foreach($listar['data'] as $cl ){ ?>
  <tr>
      <td><img src=" <?php echo $cl['img1'] ?>" class="img-responsive" width="100px"></td>
      <td><?php echo $cl['producto'] ?></td>
      <td><?php echo $cl['categoria'] ?></td>
      <td><?php echo $cl['cantidad'] ?></td>
      <td>$ <?php echo number_format($cl['valor'],2,'.',','); ?></td>

  </tr>
  <?php }  ?>
  </tbody>
</table>

</div>
	</div>