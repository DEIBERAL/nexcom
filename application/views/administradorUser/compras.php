<div id="gestCompras" class="row">
<div class="container">
<table class="table">
  <thead>
    <tr>
      <th scope="col">Id compra</th>
      <th scope="col">Fecha</th>
      <th scope="col">Total Compra</th>
      <th scope="col">Items Comprados</th>
        <th scope="col"></th>

    </tr>
  </thead>
  <tbody>
  <?php foreach($listar['data'] as $cl ){ ?>
  <tr>
      <td><?php echo $cl['compra_id'] ?></td>
      <td><?php echo $cl['fechar'] ?></td>
      <td><?php echo number_format($cl['valor_total'],2,'.',','); ?></td>
      <td><?php echo $cl['items'] ?></td>
      <td><a href="<?php echo base_url('usuarios/Gest_usuarios/detalle/'.$cl['compra_id']) ?>">Detalle</a> </td>

  </tr>
  <?php }  ?>
  </tbody>
</table>

</div>
	</div>