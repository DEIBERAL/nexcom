<div id="Gest_Registro">
<section class="main">
	<div class="wrrape-negocio">
			<div class="container">
				<div class="inhalt-negocio">
					<div class="title text-left">
						<h1>REGISTRARSE</h1>
					</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<form  id="formRegistro" method="post" action="" >
									<div class="row">
										<div class="col-xs-12 col-sm-6 hidden">
											<div class="form-group">
												<label for="billing_first_name" class="">Tipo Rol <abbr class="required" title="required">*</abbr></label>
												<select class="form-group" name="rol_id" id="rol_id">
													<option value="2" >Administrador</option>
												
												</select>
									  		</div>
										</div>
										<div class="col-xs-12 col-sm-6">
											<div class="form-group">
												<label for="billing_first_name" class="">Nombre <abbr class="required" title="required">*</abbr></label>
												<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" value="Alejandra"  required>
									  		</div>
										</div>
										<div class="col-xs-12 col-sm-6">
											<div class="form-group">
												<label for="billing_first_name" class="">Apellido <abbr class="required" title="required">*</abbr></label>
												<input type="text" class="form-control" id="apellido" name="apellido" placeholder="Apellido" value="vargas" required>
									 	 	</div>
										</div>
										<div class="col-xs-12 col-sm-6">
											<div class="form-group">
												<label for="billing_first_name" class="">Cédula<abbr class="required" title="required">*</abbr></label>
												<input type="text" class="form-control" id="cedula" name="cedula" placeholder="Cédula" value="1234565" required>
												<p class="msj1"></p>
									  		</div>
										</div>
									
										<div class="col-xs-12 col-sm-6">
											<div class="form-group">
												<label for="billing_first_name" class="">Teléfono<abbr class="required" title="required">*</abbr></label>
												<input type="text" class="form-control" id="telefono" name="telefono" placeholder="Teléfono" value="3134996368" required>
												<p class="msj"></p>
									  		</div>
										</div>
										<div class="col-xs-12 col-sm-6">
											<div class="form-group">
												<label for="billing_first_name" class="">Ciudad<abbr class="required" title="required">*</abbr></label>
												<input type="text" class="form-control" id="ciudad" name="ciudad" placeholder="ciudad" value="medellin" required >
									  		</div>
										</div>
										<div class="col-xs-12 col-sm-6">
											<div class="form-group">
												<label for="billing_first_name" class="">Dirección<abbr class="required" title="required">*</abbr></label>
												<input type="text" class="form-control" name="direccion" id="direccion" placeholder="direccion" value="calle 12a " required>
									  		</div>
										</div>
										<div class="col-xs-12 col-sm-6">
											<div class="form-group">
											<label for="billing_first_name" class="">Correo electrónico<abbr class="required" title="required">*</abbr></label>
											<input type="text" class="form-control" name="correo" id="correo" placeholder="correo" value="alejandra12vargasliz@gmail.com" required>
									  	</div>
										</div>
										<div class="col-xs-12 col-sm-6">
											<div class="form-group">
											<label for="billing_first_name" class="">Contraseña<abbr class="required" title="required">*</abbr></label>
											<input type="password" class="form-control" name="password" id="password" placeholder="correo" value="" required>
									  	</div>
										</div>
										<div class="col-xs-12 col-sm-6">
											<div class="form-group">
											
											<button type="submit" class="btn btn-danger" ><span>Registrarse</span></button>
									  	</div>
										</div>
									
										
								</div>
									<div class="resultados"></div>
									<div class="msg"></div>
						</div>
						</div>
					
				
				
				</form>
				
				
			</div>
		</div>
	</div>

 
</section>
	

</div>