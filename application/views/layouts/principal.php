<?php error_reporting(0); ?>
<!doctype html>
<html lang="es">
<head>
<meta charset="utf-8">
<title>Nexcome</title>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

   <!-- favicon links -->
<link rel="shortcut icon" type="image/ico" href="<?php echo base_url() ?>webroot/themesite/coomerse/images/favicon.png" />
<link rel="icon" type="image/ico" href="<?php echo base_url() ?>webroot/themesite/coomerse/images/favicon.png"/>
	
<!-- main css -->
<link rel="stylesheet" href="<?php echo base_url() ?>webroot/themesite/coomerse/css/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url() ?>webroot/themesite/coomerse/css/main.css">
<link rel="stylesheet" href="<?php echo base_url() ?>webroot/themesite/coomerse/css/mobile.css">
<link rel="stylesheet" href="<?php echo base_url() ?>webroot/themesite/coomerse/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>webroot/themesite/coomerse/css/animate.css">
<link rel="stylesheet" type="<?php echo base_url() ?>webroot/themesite/coomerse/text/css" href="css/lightcase.css"/>

<link rel="stylesheet" href="<?php echo base_url() ?>webroot/themesite/coomerse/css/owl.carousel.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>webroot/themesite/coomerse/css/owl.theme.default.min.css">
	
	
<link rel="stylesheet" href="<?php echo base_url() ?>webroot/themesite/coomerse/css/svg/fontello-codes.css">
<link rel="stylesheet" href="<?php echo base_url() ?>webroot/themesite/coomerse/css/svg/fontello-embedded.css">
<link rel="stylesheet" href="<?php echo base_url() ?>webroot/themesite/coomerse/css/svg/fontello-ie7-codes.css">
<link rel="stylesheet" href="<?php echo base_url() ?>webroot/themesite/coomerse/css/svg/fontello-ie7.css">
<link rel="stylesheet" href="<?php echo base_url() ?>webroot/themesite/coomerse/css/svg/fontello.css">
	
	
<link rel="stylesheet" href="<?php echo base_url() ?>webroot/themesite/coomerse/css/jquery.mCustomScrollbar.css">
<link rel="stylesheet" href="<?php echo base_url() ?>webroot/themesite/coomerse/js/lightgallery.min.js">
	
	
	
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<!-- Smartsupp Live Chat script -->
<script type="text/javascript">
var _smartsupp = _smartsupp || {};
_smartsupp.key = '93fe1da099655c550cfaa8fb0777f4f5a0b899e5';
window.smartsupp||(function(d) {
  var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
  s=d.getElementsByTagName('script')[0];c=d.createElement('script');
  c.type='text/javascript';c.charset='utf-8';c.async=true;
  c.src='https://www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
})(document);
</script>
	
	
	
</head>

<body>
	<script>
		var BASE_URL= '<?php echo base_url(); ?>';
		var TOKEN = '<?php echo getUserId(); ?>';
	
	</script>
<header>
<div id="wrapper">
 
<?php if ($vista == 'loging-head') {  ?>	
		<script>
	
		history.pushState(null, document.title, location.href);
	window.addEventListener('popstate', function (event)
	{
	  history.pushState(null, document.title, location.href);
	});
	</script>
	 <nav class="navbar navbar-inverse navbar-fixed-top menu log-in" role="navegation"></nav>
<?php } else { ?> 
 <nav class="navbar navbar-inverse navbar-fixed-top menu" role="navegation">
	<div class="pre-head">
		<div class="container">
			<div class="pre-header">
				<div class="hed-lft">
					<div class="social">
						
					</div>
					<div class="pbx-header">
						<a href="tel:5716517222"><img src="<?php echo base_url() ?>webroot/themesite/coomerse/images/phone.png" alt="">PBX: 018000116398 Línea Nacional</a>
					</div>
					<div class="mail-header">
						<a href="mailto:marketing@nex.com.co"><img src="<?php echo base_url() ?>webroot/themesite/coomerse/images/mail.png" alt="">marketing@nex.com.co</a>
					</div>
				</div>
				<div class="hed-rigt">
					<div class="cuenta-header">
						<?php if($_SESSION['allData']){  ?>
						<a href="<?php echo base_url(); ?>usuarios/Gest_usuarios/salir"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
						<a href="<?php if($_SESSION['userCurrent']['rol_id']==2){ echo base_url('panel/Home/cliente');}elseif ($_SESSION['userCurrent']['rol_id']==1){echo base_url('panel/Home/admin');} ?>"><i class="fa fa-user" aria-hidden="true"></i></a>
						<?php }else{  ?>
						<a href="<?php echo base_url(); ?>usuarios/Gest_usuarios/login"><img src="<?php echo base_url() ?>webroot/themesite/coomerse/images/menu.png" alt="">Mi cuenta</a>
						<a href="<?php echo base_url(); ?>usuarios/Gest_usuarios/registro"><img src="<?php echo base_url() ?>webroot/themesite/coomerse/images/menu.png" alt="">Registro</a>
						<?php } ?>
						
					</div>
					<div class="carrito-header">
						<!--<a href="#"><img src="images/carrito.png" alt="">Compra</a>-->
					</div>
				</div>
			</div>
	   </div>
	</div>
	 
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#desplegar">
				<span class="sr-only">Desplegar / Ocultar Menú</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<div class="logo">
				<a href="index.php"><img src="<?php echo base_url() ?>webroot/themesite/coomerse/images/logo.png" alt=""></a>
			</div>
		</div>

		<div class="collapse navbar-collapse " id="desplegar">
			<ul class="nav navbar-nav">
					<li><a href="#">Home</a></li>
					<li><a href="#">Empresa</a></li>
					<li><a href="#">Hardware</a></li>
					<li><a href="#">Soluciones TI</a></li>
					<li><a href="#">Blog</a></li>
					<li><a href="#" >Contacto</a></li>
					<li class="cart">
						
						<div class="count">
                            <p>$ <small id="count-total" class="text-right"><?php echo   number_format($cantidadProd['total']); ?></small></p>
                            <p><small id="count-carrito"><?php echo  $cantidadProd['cant']; ?></small> items</p>
						</div>
                        <a href="<?php echo base_url(); ?>productos/GestProductos/carrito"  ><img src="<?php echo base_url() ?>webroot/themesite/coomerse/images/carrito-white.png" alt="">
                        </a>
					</li>
				</ul>
		</div>
	</div>
</nav>
<?php } ?>
	
</div>
</header>
	
	
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog bs-example-modal-md" role="document">
    <div class="modal-content">
      <div class="modal-body">
		<div class="embed-responsive embed-responsive-16by9">
		  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/2MpUj-Aua48"></iframe>
		</div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->	

<section class="main">
		<?php echo $template['body']; ?>      
	</section>
	
<footer>
	<div class="inhalt-content">
		<div class="container">
			
			<div class="row row-container">
				<div class="col-xs-12 col-sm-9">
					<div class="title-footer text-left">
						<h1>Contacto</h1>
					</div>
					
					<div class="row row-footer">
						<div class="col-xs-12 col-sm-4">
							<div class="content-footer">
								<div class="item-footer">
									<img src="<?php echo base_url() ?>webroot/themesite/coomerse/images/icono-footer1.png" alt="">
									<div>
										<h2>Bogotá</h2>
										<p>Cr. 16 # 76-31</p>
									</div>
								</div>
								
								<div class="item-footer">
									<img src="<?php echo base_url() ?>webroot/themesite/coomerse/images/icono-footer2.png" alt="">
									<div>
										<h2>PBX</h2>
										<p>(571) 651 7222</p>
										<p>(571) 552 0777</p>
										<p>+57 305 8628703</p>
										<p>+57 305 8628716</p>
									</div>
								</div>
								
								<div class="item-footer">
									<img src="<?php echo base_url() ?>webroot/themesite/coomerse/images/icono-footer3.png" alt="">
									<div>
										<h2>Email</h2>
										<p>marketing@nex.com.co</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4">
							<div class="content-footer">
								<div class="item-footer">
									<img src="<?php echo base_url() ?>webroot/themesite/coomerse/images/icono-footer1.png" alt="">
									<div>
										<h2>Medellín</h2>
										<p>Edificio Block Centro Empresarial <br> Av. El Poblado Cra. 43a # 19 17 of. 303</p>
									</div>
								</div>
								
								<div class="item-footer">
									<img src="<?php echo base_url() ?>webroot/themesite/coomerse/images/icono-footer2.png" alt="">
									<div>
										<h2>PBX</h2>
										<p>(574) 4031627</p>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="col-xs-12 col-sm-4">
							<div class="content-footer">
								<div class="item-footer">
									<img src="<?php echo base_url() ?>webroot/themesite/coomerse/images/icono-footer1.png" alt="">
									<div>
										<h2>Pereira</h2>
										<p>Centro Comercial Lago Plaza <br> LC. 132 Calle 23 # 8 - 55</p>
									</div>
								</div>
								
								<div class="item-footer">
									<img src="<?php echo base_url() ?>webroot/themesite/coomerse/images/icono-footer2.png" alt="">
									<div>
										<h2>Línea Nacional</h2>
										<p>018000116398</p>
									</div>
								</div>
								
								
							</div>
						</div>
					</div>
				
				</div>
				<div class="col-xs-12 col-sm-3">
					<div class="title-footer text-left">
						<h1>Mapa del sitio</h1>
					</div>
				
					<ul class="map-site">
						<li><a href="#">Home</a></li>
						<li><a href="#">Empresa</a></li>
						<li><a href="#">Hardware</a></li>
						<li><a href="#">Soluciones TI</a></li>
						<li><a href="#">Blog</a></li>
						<li><a href="#">Contacto</a></li>
					</ul>
					
				
				</div>
			</div>
			
			<div class="title-footer text-center">
				<h1>Métodos de pago</h1>
			</div>
			<div class="mediosdepago">
				<img src="<?php echo base_url() ?>webroot/themesite/coomerse/images/paypal.png" alt="">
				<img src="<?php echo base_url() ?>webroot/themesite/coomerse/images/mastercard.png" alt="">
				<img src="<?php echo base_url() ?>webroot/themesite/coomerse/images/visa.png" alt="">
				<img src="<?php echo base_url() ?>webroot/themesite/coomerse/images/payu.png" alt="">
			</div>
		</div>
	</div>
	<div class="zlink">
		<p>© Nexcom 2018</p>
		<p>Habeas Data</p>
		<p>SAGRLAFT</p>
		<p><a href="#">Política de Cumplimiento</a></p>
		<p><a href="#">Recolección selectiva y gestión ambiental</a></p>
		<p><a href="#">Términos y condiciones</a></p>
	</div>
</footer>

<script src="<?php echo base_url() ?>webroot/themesite/coomerse/js/jquery.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBvLywBdvmVazU_ClflbKpV39QpfNZtcFE"></script>
<script src="<?php echo base_url() ?>webroot/themesite/coomerse/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>webroot/themesite/coomerse/smove/dist/jquery.smoove.js"></script>
<script src="<?php echo base_url() ?>webroot/themesite/coomerse/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url() ?>webroot/themesite/coomerse/js/jquery.mCustomScrollbar.js"></script>
<script src="<?php echo base_url() ?>webroot/themesite/coomerse/js/lightgallery.min.js"></script>
<script src="<?php echo base_url() ?>webroot/js/helpers.js"></script>
<script src="<?php echo base_url() ?>webroot/themesite/coomerse/js/UP.js"></script>
<script src="<?php echo base_url() ?>webroot/modules/productos/main.js"></script>
<script src="<?php echo base_url() ?>webroot/modules/usuarios/main.js"></script>



<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker();
  } );
</script>

</body>
</html>

 
