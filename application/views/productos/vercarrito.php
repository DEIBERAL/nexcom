
<div id="Ges_Carrito">
<section class="main">	
	<div class="miguina">
				<div class="container">
					<ol class="breadcrumb">
						<li><a href="<?php echo base_url('productos/GestProductos/productos') ?>">Home</a></li>
						<li class="active">Carrito Compras</li>
					</ol>
				</div>
			</div>
	
<div class="wrrape-negocio">
		<div class="container">
			<div class="inhalt-negocio">
				
				<div class="row">
                    <?php if($datos){ ?>
					<div class="col-xs-12 col-sm-8">
						<h2>Carrito</h2>
						<div class="table-responsive">
							<table class="table table-striped">
					  		<tr>
						<th>Imagen</th>
						<th>Producto</th>
						<th>Precio</th>
						<th>Cantidad</th>
						<th>Total</th>
					  </tr>
						<?php foreach($datos[0] as $cl=> $vl){  ?>
					  		<tr>
								<td><img src="<?php echo $vl['infoGeneral']['img1']  ?>" alt="" width="90px"></td>
								<td><?php echo $vl['infoGeneral']['nombre']  ?></td>
								<td>$ <?php echo number_format($vl['infoGeneral']['precio'])  ?></td>
								<td>
								  <input class="tipoCantidad input-text ActualizarCarrito"  name="producto_<?php echo $vl['infoGeneral']['producto_id']  ?>"   title="Cantidad" type="number" value="<?php echo $vl['cantidad']  ?>">
								  </td>
								<td><span id="TotalProd">$ <?php echo number_format($vl['total'])  ?></span></td>
					  		</tr>
					  <?php } ?>
							
							
							</table>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4">
						<h2>Total del carrito</h2>
						<table class="table table-striped totaliti">
							<tbody>
								<tr><th scope="row">&nbsp;</th><td>&nbsp;</td></tr>
								<tr><th scope="row" width="60%">Subtotal</th><td><span id="subTotal">$ <?php echo number_format($datos[2]['subtotal']) ?></span></td></tr>
								<tr><th scope="row" width="40%">Iva</th><td><span id="iva">$ <?php echo number_format($datos[1]['iva']) ?></span></td></tr> 
								<tr class="fhot"><th scope="row">Total</th><td><span id="total">$ <?php echo number_format($datos[3]['total']) ?></span></td></tr>
							</tbody> 
						</table>
						 <a href="<?php echo base_url(); ?>productos/GestProductos/form_pago" class=bto-orange>Finalizar compra</a>
					</div>
                    <?php } else{
                    ?>
                        <div class="col-lg-12">
                            <div class="alert alert-warning" role="alert">
                                No hay nada en el carrito.
                            </div>
                        </div>

                        <?php
                    }
                    ?>
				</div>

				<div class="row">
					<form>
					<div class="col-xs-12 col-sm-4">
						 <a href="<?php echo base_url(); ?>productos/GestProductos/productos">
							 <button type="button" class="bto-orange"> Seguir comprando</button></a>
					</div>
					<!-- Esto sobra
                        <div class="col-xs-12 col-sm-4 text-right">
						 <button type="submit" class="bto-orange" id="ActualizarCarrito"  >Actualizar</button>
					</div>
					-->
					</form>
                    <code><?php print_r($_SESSION['CarritoCompras']) ?></code>
				</div>
				
			</div>
		</div>
	</div>
</section>
</div>


