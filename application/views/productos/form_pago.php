<div id="Gest_Pago">
	<section class="main">	
		<div class="miguina">
			<div class="container">
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url('productos/GestProductos/productos') ?>">Home</a></li>
					<li class="active">Formulario Pago</li>
				</ol>
			</div>
		</div>	
		<div class="wrrape-negocio">
			<div class="container">
				<div class="inhalt-negocio">
					<div class="title text-left">
						<h1>FINALIZAR COMPRA</h1>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12">
							<form  id="formPagos" method="post" action="" >

								<div class="row">
									<div class="col-xs-12 col-sm-6">
										<div class="form-group">
											<label for="billing_first_name" class="">Nombre <abbr class="required" title="required">*</abbr></label>
											<input type="text" class="form-control hidden" id="usuario_id" name="usuario_id"  value="<?php echo  $_SESSION['allData']['usuario_id'] ?>" >
											<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" value="Maria Alejandra">
										 </div>
									</div>
									<div class="col-xs-12 col-sm-6">
										<div class="form-group">
											<label for="billing_first_name" class="">Apellido <abbr class="required" title="required">*</abbr></label>
											<input type="text" class="form-control" id="apellido" name="apellido" placeholder="Apellido" value="Vargas Liz">
										 </div>
									</div>
									<div class="col-xs-12 col-sm-6">
										<div class="form-group">
											<label for="billing_first_name" class="">Nombre de la empresa<abbr class="required" title="required">*</abbr></label>
											<input type="text" class="form-control" id="empresa" name="empresa" placeholder="Nombre de la empresa" value="Tata consulting">
										 </div>
									</div>
									
									<div class="col-xs-12 col-sm-6">
										<div class="form-group">
											<label for="billing_first_name" class="">País<abbr class="required" title="required">*</abbr></label>
											<select class="form-control" id="pais" name="pais">
												<option value="Colombia">Colombia</option>
												<option value="Peru">Peru</option>
												<option value="Mexico">Mexico</option>
											</select>
										  </div>
									</div>
									<div class="col-xs-12 col-sm-6">
										<div class="form-group">
											<label for="billing_first_name" class="">Direccion<abbr class="required" title="required">*</abbr></label>
											<input type="text" class="form-control" name="direccion" id="direccion" placeholder="Direccion" value="calle 12 a">
										</div>
									</div>
									<div class="col-xs-12 col-sm-6">
										<div class="form-group">
											<label for="billing_first_name" class="">Localidad / Ciudad<abbr class="required" title="required">*</abbr></label>
											<input type="text" class="form-control" id="localidad" name="localidad"  placeholder="Localidad" value="Medellin">
										</div>
									</div>
									
									<div class="col-xs-12 col-sm-6">
										<div class="form-group">
											<label for="billing_first_name" class="">Región / Provincia<abbr class="required" title="required">*</abbr></label>
											<input type="text" class="form-control" id="region" name="region" placeholder="Región / Provincia" value="xxxx">
										</div>
									</div>
									<div class="col-xs-12 col-sm-6">
										<div class="form-group">
											<label for="billing_first_name" class="">Teléfono<abbr class="required" title="required">*</abbr></label>
											<input type="text" class="form-control"  id="telefono" name="telefono" placeholder="Teléfono" value="313323">
										</div>
									</div>
									<div class="col-xs-12 col-sm-6">
										<div class="form-group">
											<label for="billing_first_name" class="">Correo electrónico<abbr class="required" title="required">*</abbr></label>
											<input type="email" class="form-control" name="correo" id="correo" placeholder="Email" value="ale@gmail.com">
										 </div>
									</div>
									<div class="col-xs-12 col-sm-6">
										<div class="radio">
											<label>
												<input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">
												No aplica, entrega en el despacho mas cercano
											</label>
										</div>
									</div>
									
									<div class="col-xs-12 col-sm-3">
										<div class="radio">
											<label>
												<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
												Mensajeros urbanos Bogota
											</label>
										</div>
									</div>
									<div class="col-xs-12 col-sm-3">
										<div class="radio">
											<label>
												<input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
												Contra entrega
											</label>
										</div>
									</div>
									
									
								</div>
							
					</div>
					
					<div class="col-xs-12 col-sm-6 pago-descripcion hidden">
						<h1>Compras 100% seguras</h1>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam doloremque, quo iure maxime adipisci voluptatum omnis asperiores recusandae, quae fugit ut. Asperiores perferendis nostrum voluptatem optio consectetur non in porro.</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam doloremque, quo iure maxime adipisci voluptatum omnis asperiores recusandae, quae fugit ut. Asperiores perferendis nostrum voluptatem optio consectetur non in porro.</p>
						<h1>Política de tratamiento de datos personales</h1>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam doloremque, quo iure maxime adipisci voluptatum omnis asperiores recusandae, quae fugit ut. Asperiores perferendis nostrum voluptatem optio consectetur non in porro.</p>
					</div>
				</div>
				
				<table class="table table-striped">
					<h2>Tu pedido</h2>
					<tbody>
					
					<tr>
						<th>Producto</th>
						<th>Precio</th>									
					</tr>
					
					<?php foreach($datos[0] as $cl=> $vl){  ?>
					<tr>
						<th scope="row"><?php echo $vl['infoGeneral']['nombre']  ?> x <?php echo $vl['cantidad']  ?></th>
						<td>$ <?php echo number_format($vl['infoGeneral']['precio'])  ?></td>
					</tr>
					<?php } ?>
					<tr><th scope="row">Subtotal</th><td>$ <?php echo number_format($datos[2]['subtotal']) ?></td></tr>
					<tr><th scope="row">Valor envio</th><td>$ <?php echo number_format($datos[1]['iva']) ?></td></tr> 
					<tr><th scope="row">Iva</th><td>$134.900</td></tr> 
					<tr class="fhot"><th scope="row">TOTAL</th><td>$ <?php echo number_format($datos[3]['total']) ?></td></tr> 
					</tbody> 
				</table>
				
				<div class="pago-Payu">
					<div class="inhal-pagosPayu">
					
						<label class="radio-inline">
							  <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">Pagos en linea PayU Latinoamérica
							</label>
				
					
					<div class="inhalbacj">
						<p>Pague con tarjeta de crédito, debito o transacción bancaria de forma segura a través de los servidores seguros de PayU Latinoamérica.</p>
					</div>
				</div>
					
				</div>
				<div id="Msgpago"></div>
				<div class="resultados"></div>
				<button type="submit" class="bto-green">Finalizar compra</button>
				</form>
				
			</div>
		</div>
	</div>
</section>

</div>	
	
