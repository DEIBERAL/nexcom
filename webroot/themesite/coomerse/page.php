﻿<?php include ('header.php') ?>

<section class="main">
	
	<?php include ('breadcrumbs.php') ?>

	<div class="wrrape-page">
		<div class="container-fluid">
			<div class="in-page">
				
				<div class="filters">
					<div class="row">
						<div class="col-xs-12 col-sm-6">
							<form>
								<div class="form-group">
									<select name="" id="" class="form-control">
										<option value="" style="display: none">Categorías</option>
										<option value=""></option>
										<option value=""></option>
										<option value=""></option>
										<option value=""></option>
									</select>
								</div>
								<div class="form-group">
									<select name="" id="" class="form-control">
										<option value="" style="display: none">Marcas</option>
										<option value=""></option>
										<option value=""></option>
										<option value=""></option>
										<option value=""></option>
									</select>
								</div>
								<div class="form-group">
									<select name="" id="" class="form-control">
										<option value="" style="display: none">Precio</option>
										<option value=""></option>
										<option value=""></option>
										<option value=""></option>
										<option value=""></option>
									</select>
								</div>
								
								
							</form>
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="in-filtros">
								<aside>
									<form action="">
									<aside class="search">
										<div class="inte-search">
											<input class="form-control" placeholder="Buscar" name="search" type="text">
											<button class="but_sear" type="button"><i class="fa fa-search" aria-hidden="true" id="iconBuscar"></i></button>
										</div>
									</aside>
								</form>
								</aside>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12 col-sm-4">
						<div class="in-filtros">
							<aside>
								<div class="in-content-collapse">
									<div class="conectColapse">
										<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
											<div class="panel panel-default">
												<div class="panel-heading" role="tab" id="headingOne">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
														<h4 class="panel-title">
														 Categorías
														<span class="close-open pull-right"></span>
														</h4>
													</a>
												</div>
												<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
												  <div class="panel-body">
													  <a href="page.php"><i class="demo-icon icon-01"><span>Hardware de Cómputo</span></i></a>
													  <a href="page.php"><i class="demo-icon icon-02"><span>Impresión y escáner</span></i></a>
													  <a href="page.php"><i class="demo-icon icon-03"><span>Vídeo, Audio y Proyección</span></i></a>
													  <a href="page.php"><i class="demo-icon icon-04"><span>Conectividad y redes</span></i></a>
													  <a href="page.php"><i class="demo-icon icon-05"><span>Servidores y almacenamiento</span></i></a>
													  <a href="page.php"><i class="demo-icon icon-06"><span>Software y aplicativos</span></i></a>
													  <a href="page.php"><i class="demo-icon icon-08"><span>Potencia y protección electrica</span></i></a>
													  <a href="page.php"><i class="demo-icon icon-07"><span>Seguridad Perimetral</span></i></a>
													  <a href="page.php"><i class="demo-icon icon-09"><span>Suministros de computo</span></i></a>
													  <a href="page.php"><i class="demo-icon icon-10"><span>Telefonía IP</span></i></a>
												  </div>
												</div>
											  </div><!-- categorias -->
											<div class="panel panel-default">
												<div class="panel-heading" role="tab" id="headingTwo">
													<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
													  <h4 class="panel-title">
														 Marca
														<span class="close-open pull-right"></span>
													  </h4>
													</a>
												</div>
												<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">
													<div class="panel-body content mCustomScrollbar light" data-mcs-theme="minimal-dark">
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													  <a href="#"><span>Marca</span></a>
													</div>
												</div>
											  </div><!-- Marca -->
											<div class="panel panel-default">
												<div class="panel-heading" role="tab" id="headingThree">
													<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
														<h4 class="panel-title">
													  Precio
														<span class="close-open pull-right"></span>
														</h4>
													</a>
												</div>
												<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
												  <div class="panel-body content mCustomScrollbar light" data-mcs-theme="minimal-dark">
													
													  <div class="slide-precio">
														<p> <input type="text" id="amount" readonly style="border:0; color:#003B4A; font-weight:bold;background-color: transparent;">
														</p>
														<div id="slider-range"></div>
													</div>
													  
													  <form>
														<div class="checkbox">
															<label>
																<input type="checkbox">$100.000 - $200.000
															</label>
														</div>
														  <div class="checkbox">
															<label>
																<input type="checkbox">$300.000 - $400.000
															</label>
														</div>
														  <div class="checkbox">
															<label>
																<input type="checkbox">$500.000 - $600.000
															</label>
														</div>
														<div class="checkbox">
															<label>
																<input type="checkbox">$700.000 - $800.000
															</label>
														</div>
														<div class="checkbox">
															<label>
																<input type="checkbox">$900.000 - $1.000.000
															</label>
														</div>
														<div class="checkbox">
															<label>
																<input type="checkbox">$2.000.000 - $3.000.000
															</label>
														</div>
														<div class="checkbox">
															<label>
																<input type="checkbox">$4.000.000 - $5.000.000
															</label>
														</div>
														 <div class="checkbox">
															<label>
																<input type="checkbox">$2.000.000 - $3.000.000
															</label>
														</div>
														<div class="checkbox">
															<label>
																<input type="checkbox">$2.000.000 - $3.000.000
															</label>
														</div>
													</form>
													  
												  </div>
												</div>
											</div><!-- precio -->
										</div>
									</div>
								</div>
							</aside>
						</div>
					</div>
					<div class="col-xs-12 col-sm-8">
						<div class="in-page-productos">
							<div class="in-producto">
								<div class="in-topet">
									<a href="single-producto.php"><img src="images/zona-privada/listado-producto.jpg" alt="">
									<h2>Nombre producto</h2></a>
								</div>
								<div class="in-botep">
									<h2>$ 234.567</h2>
									<a href="" class="bto-orange">añadir</a>
								</div>
							</div>
							<div class="in-producto">
								<div class="in-topet">
									<a href="single-producto.php"><img src="images/zona-privada/listado-producto.jpg" alt="">
									<h2>Nombre producto</h2></a>
								</div>
								<div class="in-botep">
									<h2>$ 234.567</h2>
									<a href="" class="bto-orange">añadir</a>
								</div>
							</div>
							<div class="in-producto">
								<div class="in-topet">
									<a href="single-producto.php"><img src="images/zona-privada/listado-producto.jpg" alt="">
									<h2>Nombre producto</h2></a>
								</div>
								<div class="in-botep">
									<h2>$ 234.567</h2>
									<a href="" class="bto-orange">añadir</a>
								</div>
							</div><div class="in-producto">
								<div class="in-topet">
									<a href="single-producto.php"><img src="images/zona-privada/listado-producto.jpg" alt="">
									<h2>Nombre producto</h2></a>
								</div>
								<div class="in-botep">
									<h2>$ 234.567</h2>
									<a href="" class="bto-orange">añadir</a>
								</div>
							</div><div class="in-producto">
								<div class="in-topet">
									<a href="single-producto.php"><img src="images/zona-privada/listado-producto.jpg" alt="">
									<h2>Nombre producto</h2></a>
								</div>
								<div class="in-botep">
									<h2>$ 234.567</h2>
									<a href="" class="bto-orange">añadir</a>
								</div>
							</div><div class="in-producto">
								<div class="in-topet">
									<a href="single-producto.php"><img src="images/zona-privada/listado-producto.jpg" alt="">
									<h2>Nombre producto</h2></a>
								</div>
								<div class="in-botep">
									<h2>$ 234.567</h2>
									<a href="" class="bto-orange">añadir</a>
								</div>
							</div>
							<div class="in-producto">
								<div class="in-topet">
									<a href="single-producto.php"><img src="images/zona-privada/listado-producto.jpg" alt="">
									<h2>Nombre producto</h2></a>
								</div>
								<div class="in-botep">
									<h2>$ 234.567</h2>
									<a href="" class="bto-orange">añadir</a>
								</div>
							</div>
							<div class="in-producto">
								<div class="in-topet">
									<a href="single-producto.php"><img src="images/zona-privada/listado-producto.jpg" alt="">
									<h2>Nombre producto</h2></a>
								</div>
								<div class="in-botep">
									<h2>$ 234.567</h2>
									<a href="" class="bto-orange">añadir</a>
								</div>
							</div>
							<div class="in-producto">
								<div class="in-topet">
									<a href="single-producto.php"><img src="images/zona-privada/listado-producto.jpg" alt="">
									<h2>Nombre producto</h2></a>
								</div>
								<div class="in-botep">
									<h2>$ 234.567</h2>
									<a href="" class="bto-orange">añadir</a>
								</div>
							</div>
							<div class="in-producto">
								<div class="in-topet">
									<a href="single-producto.php"><img src="images/zona-privada/listado-producto.jpg" alt="">
									<h2>Nombre producto</h2></a>
								</div>
								<div class="in-botep">
									<h2>$ 234.567</h2>
									<a href="" class="bto-orange">añadir</a>
								</div>
							</div>
							<div class="in-producto">
								<div class="in-topet">
									<a href="single-producto.php"><img src="images/zona-privada/listado-producto.jpg" alt="">
									<h2>Nombre producto</h2></a>
								</div>
								<div class="in-botep">
									<h2>$ 234.567</h2>
									<a href="" class="bto-orange">añadir</a>
								</div>
							</div>
							<div class="in-producto">
								<div class="in-topet">
									<a href="single-producto.php"><img src="images/zona-privada/listado-producto.jpg" alt="">
									<h2>Nombre producto</h2></a>
								</div>
								<div class="in-botep">
									<h2>$ 234.567</h2>
									<a href="" class="bto-orange">añadir</a>
								</div>
							</div>
							<div class="in-producto">
								<div class="in-topet">
									<a href="single-producto.php"><img src="images/zona-privada/listado-producto.jpg" alt="">
									<h2>Nombre producto</h2></a>
								</div>
								<div class="in-botep">
									<h2>$ 234.567</h2>
									<a href="" class="bto-orange">añadir</a>
								</div>
							</div>
							<div class="in-producto">
								<div class="in-topet">
									<a href="single-producto.php"><img src="images/zona-privada/listado-producto.jpg" alt="">
									<h2>Nombre producto</h2></a>
								</div>
								<div class="in-botep">
									<h2>$ 234.567</h2>
									<a href="" class="bto-orange">añadir</a>
								</div>
							</div>
						</div>
						<ul class="paginador">
							<li class="active"><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">Next</a></li>
						</ul>
					</div>
				</div>
			
			</div>
		</div>
	</div>
	

</section>

<?php include ('footer.php') ?>