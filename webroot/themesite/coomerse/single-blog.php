﻿<?php include ('header.php') ?>

<section class="main">
<?php include ('breadcrumbs.php') ?>
	
	<div class="wrrape-blog">
		<div class="container">
			<div class="in-blog">
				<div class="row row-single">
					<div class="col-xs-12 col-sm-8">
						<div class="in-box-single">
							<img src="images/interna-blog.jpg" alt="">
							<div class="in-box-captionSingle">
								<span>18 Jul, 2018</span>
								<h2>Las Desktops ThinkCentre de Lenovo, flexibles y modulares, se adaptan a los lugares de trabajo</h2>
								<p>¿Tu lugar de trabajo está optimizado para todo tipo de usuarios? A medida que cambian las prioridades, el lugar de trabajo debe cambiar con ellas para impulsar una mejor colaboración y una mayor productividad de los empleados. Las desktops, si bien son activos fijos, pueden ayudar a optimizar espacios de trabajo más pequeños y reducir la carga y la complejidad de IT mediante la automatización mejorada y la reducción del tiempo de inactividad.</p>
								<p>Los usuarios avanzados agradecerán el rendimiento y la capacidad de actualización de los sistemas de torre y de formato pequeño, capaces de realizar las tareas más exigentes. La modularidad de ThinkCentre Tiny ofrece numerosas opciones de espacio de trabajo sin desorden, incluso en entornos más inhóspitos, gracias a nuestros rigurosos procesos de pruebas.</p>
								<h3>Aspectos destacados de las Desktops ThinkCentre Serie M:</h3>
								<ul>
									<li> Nuevos puertos Intel® Core™ vPro™ de Octava Generación, Thunderbolt™ 3 y USB-C, espacio reducido para la torre y chasis de formato pequeño</li>
									<li> Chip de seguridad del Módulo de Plataforma de Confianza, bloqueo electrónico (E-lock) físico del chasis</li>
									<li>Capacidad de administración habilitada con opciones de tarjetas de expansión Intel vPro, Smart Power-On y PCIe en determinados modelos.</li>
								</ul>
								
								
								<h3>Torre ThinkCentre M920 y M720 (t) y de formato pequeño (s).</h3>

								<p>Con la última generación de Intel® Core™ (Coffee Lake) con capacidad para vPro™ (en M920), las desktops actualizadas de la serie M ofrecen un chasis negro comercial limpio y elegante con profundidad reducida para espacios de trabajo más pequeños. La serie M actualizada añade nuevas características para una mejor experiencia del usuario final. Las tarjetas de expansión PCIe, como Thunderbolt™ o gráficos discretos, ofrecen protección de inversión; el chasis E-Lock protege los componentes clave dentro del chasis detectando intrusiones no autorizadas y bloqueando el chasis desde el interior y Smart Power-On, disponible en determinados modelos, permite a los usuarios pulsar Alt + P para encender una desktop que está fuera de tu alcance.</p>
								<p>ThinkPad M920 y M720 estarán disponibles en julio de 2018. Los precios se darán a conocer más cerca de la fecha de envío.</p>
								
								<h3>ThinkCentre M920 y M720 Tiny (M920x, M920q, M720q)</h3>
								
								<p>Incorporando muchas de las características de sus hermanos mayores, la serie Tiny reduce todo a un formato pequeño de un litro, ahora con aún más opciones de expansión. La familia M720 y M920 Tiny será la primera desktop de formato pequeño de un litro de la industria con opciones de expansión PCIe. M720 Tiny tendrá dos opciones separadas para la expansión de la tarjeta de interfaz de red de puerto cuádruple o serial. Además, la familia M920 Tiny agregará una opción de expansión de puerto Thunderbolt™ y la M920x Tiny también soportará una opción de gráficos discretos.</p>
								<p>Los modelos ThinkCentre M920 Tiny y M720 Tiny estarán disponibles en julio de 2018. Los precios se darán a conocer más cerca de la fecha de envío.</p>
								
								<h3>El pequeño ecosistema</h3>
								<p>Los diferentes kits de montaje y soporte, junto con la capacidad de múltiples monitores, permiten a los usuarios adaptarse a una variedad de casos de uso, tales como puntos de venta al por menor, instituciones financieras u otros entornos con limitaciones de espacio. Las versátiles pantallas Tiny-in-One pueden transformar tu diminuta PC en una PC todo-en-uno táctil. Simplemente ubica a Tiny de la serie M en la parte posterior de la pantalla y en cuestión de segundos tendrás una solución modular que es fácil de instalar.</p>
								
								<p>Para obtener más información sobre nuestros resultados, <a href="https://www3.lenovo.com/us/en/desktops-and-all-in-ones/thinkcentre/c/ThinkCentre?menu-id=Thinkcentre_Desktops" target="_blank">haz clic aquí.</a></p>

							</div>
							
						</div>
					</div>
					<div class="col-xs-12 col-sm-4">
						<div class="in-aside">
							<aside class="search">
								<div class="">
									<form action="" role="search" method="get">
									<div class="form-group">
										<input class="form-control" name="buscador" placeholder="Buscar..." type="search">
										<button class="but_sear" type="submit"><img src="images/lupa.png" alt=""></button>
										</div>
									</form>
								</div>
							</aside>
							<aside>
								<div class="titl-aside">
									<h2>Blogs Recientes</h2>
								</div>
								<div class="media">
								  <div class="media-left">
									<a href="single-blog.php">
									  <img class="media-object" src="images/blog-1.jpg" alt="...">
									</a>
								  </div>
								  <div class="media-body">
									<h4 class="media-heading">18 Jul, 2018</h4>
									<h2>Las Desktops ThinkCentre de Lenovog</h2>
								  </div>
								</div>
								<div class="media">
								  <div class="media-left">
									<a href="single-blog.php">
									  <img class="media-object" src="images/blog-2.jpg" alt="...">
									</a>
								  </div>
								  <div class="media-body">
									<h4 class="media-heading">27 Mar, 2017</h4>
									<h2>Lenovo se propone liderar los ordenadores de los ‘millennials’</h2>
								  </div>
								</div>
								<div class="media">
								  <div class="media-left">
									<a href="single-blog.php">
									  <img class="media-object" src="images/blog-3.jpg" alt="...">
									</a>
								  </div>
								  <div class="media-body">
									<h4 class="media-heading">27 Mar, 2017</h4>
									<h2>Lenovo Desafíos Jedi: sumérgete en el universo ‘Star Wars’</h2>
								  </div>
								</div>
							</aside>
							<aside>
								<div class="titl-aside">
									<h2>Keywords</h2>
								</div>
								<ul class="keywd">
									<li><a href="#">Keyword</a></li>
									<li><a href="#">Keyword</a></li>
									<li><a href="#">Keyword</a></li>
									<li><a href="#">Keyword</a></li>
									<li><a href="#">Keyword</a></li>
									<li><a href="#">Keyword</a></li>
								</ul>
							</aside>
							<aside>
								<div class="titl-aside">
									<h2>Categorías</h2>
								</div>
								<ul class="catg">
									<li><a href="#">Categoría</a></li>
									<li><a href="#">Categoría</a></li>
									<li><a href="#">Categoría</a></li>
									<li><a href="#">Categoría</a></li>
									<li><a href="#">Categoría</a></li>
								</ul>
							</aside>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	

</section>
<?php include ('footer.php') ?>