﻿<?php include ('header.php') ?>

<section class="main">

	<?php include ('breadcrumbs.php') ?>
	
	<div class="wrrape-suministro">
		<div class="title text-center">
			<h1>SOLUCIONES TI</h1>
		</div>
		
		<div class="in-suministro">
		<div class="container">
			<div class="in-content">
				<div class="in-content-collapse">
					<div class="conectColapse">
						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingOne">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
										<i class="demo-icon icon-06-12"></i>
										<h4 class="panel-title">
										Outsoursing de impresión
										<span class="close-open pull-right"></span>
										</h4>
									</a>
								</div>
								<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
								  <div class="panel-body">
									<div class="row">
									 <div class="col-xs-12 col-sm-12">
										<img src="images/somunistros-1.jpg" alt="" width="100%">
										</div>
									 <div class="col-xs-12 col-sm-12">
										<p>Sabemos que la optimización de procesos para la productividad en las empresas es muy importante, Nexcom como consultor en Ti pone a disposición la asesoría oportuna para llevar a cabo el outsourcing de impresión, donde se realiza un levantamiento y análisis previo, ciclo de impresiones Mensuales, Semanales, Diarias, usuarios vinculados por máquina y Software de administración. Para llevar en alto impacto la reducción de costos a su empresa y ver la viabilidad de los diferentes modelos de Outsourcing. </p>
									</div>
									 </div>
								  </div>
								</div>
							  </div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingTwo">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
										<i class="demo-icon icon-07-12"></i> 
									  <h4 class="panel-title">
										Gestión Documental
										<span class="close-open pull-right"></span>
									  </h4>
									</a>
								</div>
								<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">
									<div class="panel-body">
								  	<div class="row">
									 <div class="col-xs-12 col-sm-12">
										<img src="images/ft//GESTION-DOCUMENTAL.jpg" alt="" width="100%">
										</div>
									 <div class="col-xs-12 col-sm-12">
										<p>"Entendemos que la administración y manejo de documentos es cada vez más critica en las compañías, por aspectos de seguridad, protocolos para la búsqueda de documentos físicos, espacios de almacenamiento, políticas para archivar documentos necesarios en el tiempo, entre otros.</p>
										<p>Nexcom como consultor TI y especialista en políticas de administración en el manejo de documentos, realiza previo levantamiento para ofrecerle la mejor solución mediante un servidor documental central, captura y digitalización de documentos por dependencias, control de acceso a los documentos de manera segura y estructurada, integración a los sistemas internos de la compañía y el flujo de trabajo en los procesos de los documentos para las diferentes dependencias."</p>
									</div>
									 </div>
									 </div>
								</div>
							  </div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingThree">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
										<i class="demo-icon icon-08-12"></i> 
										<h4 class="panel-title">
									 	Renta de Equipos de Computo
										<span class="close-open pull-right"></span>
										</h4>
									</a>
								</div>
								<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
								  <div class="panel-body">
									<div class="row">
									 <div class="col-xs-12 col-sm-12">
										<img src="images/ft/RENTA-EQUIPOS-DE-COMPUTO-2.jpg" alt="" width="100%">
										</div>
										 <div class="col-xs-12 col-sm-12">
											<p>¿Comprar o Rentar? Son preguntas que se realizan alrededor de las areas de TI y administrativas, para revisar la viabilidad de las adquiciones de Equipos de computo. Nexcom como consultor TI pone a su disposición los diferentes escenarios que le harán mas facil tomar la decision en términos de factibilidad y funcionalidad a la hora de adquirir un producto o servicio. Contamos con mas de 20 referencias disponibles de las diferentes gamas en Equipos de Escritorio; All in One, Tipo torre SSF, Alto desempeño Workstations, Servidores y componentes de Tecnologia. </p>
										</div>
									 </div>
								  </div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headA">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#sol-a" aria-expanded="false" aria-controls="sol-a">
										<i class="demo-icon icon-09-12"></i>
										<h4 class="panel-title">
									 	Mesa de Ayuda
										<span class="close-open pull-right"></span>
										</h4>
									</a>
								</div>
								<div id="sol-a" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headA" aria-expanded="false">
								  <div class="panel-body">
									<div class="row">
									 <div class="col-xs-12 col-sm-12">
										<img src="images/ft/MESA-DE-AYUDA.jpg" alt="" width="100%">
										</div>
									 <div class="col-xs-12 col-sm-12">
										<p>Entendemos la necesidad de siempre estar conectados, con nuestro valor mas importante  -el cliente- , es por ello que Nexcom como consultor TI junto con su equipo de trabajo, pondrá a su disposición la mejor solución con un canal de comunicacion único que se adapte a su necesidad en terminos de: * Atencion y solución de incidentes en linea ,* Seguimiento de requerimientos, * Reducción impactos de llamadas recurrentes ,* atencion oportuna y medicion de incidentes. </p>
									</div>
									 </div>
								  </div>
								</div>
							  </div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headB">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#sol-b" aria-expanded="false" aria-controls="sol-b">
										<i class="demo-icon icon-04-12"></i>
										<h4 class="panel-title">
									  Servicios de nube
										<span class="close-open pull-right"></span>
										</h4>
									</a>
								</div>
								<div id="sol-b" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headB" aria-expanded="false">
								  <div class="panel-body">
									<div class="row">
									 <div class="col-xs-12 col-sm-12">
										<img src="images/ft/CLOUD-COMPUTING.jpg" alt="" width="100%">
										</div>
									 <div class="col-xs-12 col-sm-12">
										<p>"¿Comprar o suscripción en la nube? Son preguntas que se realizan alrededor de las áreas de TI y administrativas, para revisar la viabilidad de las adiciones de Equipos de cómputo y periféricos.</p>
											<p>Nexcom como su consultor TI pone a su disposición los diferentes escenarios que le harán mas fácil tomar la decisión en términos de factibilidad y funcionalidad a la hora de adquirir un producto o servicio en la nube. Contamos con más de 20 referencias disponibles de las diferentes gamas en Equipos de Escritorio; All in One, Tipo torre SSF, Alto desempeño Worstations, Laptops , Servidores y componentes de Tecnología."</p>
									</div>
									 </div>
								  </div>
								</div>
							  </div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headC">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#sol-c" aria-expanded="false" aria-controls="sol-c">
										<i class="demo-icon icon-05-12"></i>
										<h4 class="panel-title">
									  	Consultoría
										<span class="close-open pull-right"></span>
										</h4>
									</a>
								</div>
								<div id="sol-c" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headC" aria-expanded="false">
								  <div class="panel-body">
									<div class="row">
									 <div class="col-xs-12 col-sm-12">
										<img src="images/ft/CONSULTORIA.jpg" alt="" width="100%">
										</div>
									 <div class="col-xs-12 col-sm-12">
										<p>"Consultoría en la planeación, implementación y direccionamiento de proyectos TI, nuestro enfoque está en la asesoría preventa en donde se realiza un diagnóstico inicial, diseño y ejecución de los proyectos, para brindar las mejores soluciones en Hardware y licenciamiento de aplicativos como resultado para impactar positivamente en los resultados de nuestros clientes, es decir, mayor productividad, disminución de riesgos, ahorro en dinero, esfuerzo y tiempo. "</p>
									</div>
									 </div>
								  </div>
								</div>
							  </div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headD">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#sol-d" aria-expanded="false" aria-controls="sol-d">
										<i class="demo-icon icon-03-12"></i>
										<h4 class="panel-title">
									  	Capacitación
										<span class="close-open pull-right"></span>
										</h4>
									</a>
								</div>
								<div id="sol-d" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headD" aria-expanded="false">
								  <div class="panel-body">
									<div class="row">
									 <div class="col-xs-12 col-sm-12">
										<img src="images/somunistros-1.jpg" alt="" width="100%">
										</div>
									 <div class="col-xs-12 col-sm-12">
										<p>Capacitacion en soluciones TI  para la integracion de tecnologias , aulas virtuales , e-learning , instalacion en aplicativos software , manejo de programas ,  todo esto para aumentar la productividad en las empresas.</p>
									</div>
									 </div>
								  </div>
								</div>
							  </div>
						</div>
					</div>
				</div>
				
				<div class="in-formInfo">
					<div class="in-boxform">
						<h2>OBTÉN MÁS INFORMACIÓN</h2>
						<form>
							<div class="">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Nombre de empresa">
								  </div>
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Nombre">
								</div>
								<div class="form-group">
									<input type="email" class="form-control" placeholder="Email">
								</div>
								<div class="form-group">
									<input type="tel" class="form-control" placeholder="Teléfono">
								</div>
								<div class="form-group">
									<input type="tel" class="form-control" placeholder="Celular">
								</div>
								<div class="form-group">
									<textarea name="" id="" class="form-control"  placeholder="Cuéntenos su proyecto"></textarea>
								</div>

								<div class="checkbox">
								<label>
								  <input type="checkbox"> Acepto términos y condiciones
								</label>
							  </div>
								<div class="captcha">
									<img src="images/captcha.png" alt="" width="80%;">
								</div>
								<button type="submit" class="btn bto-orange">Enviar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>

	
</section>
<?php include ('footer.php') ?>