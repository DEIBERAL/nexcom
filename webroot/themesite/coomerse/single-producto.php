﻿<?php include ('header.php') ?>
<section class="main">
	
	<?php include ('breadcrumbs.php') ?>

	<div class="wrrape-single">
		<div class="in-page-single">
			<div class="container">
				<div class="in-single">
					<div class="row">
						<div class="col-xs-12 col-sm-7">
							<div class="gallery">
								<div id="lightgallery">
									<div class="owl-multimedia owl-carousel owl-theme ">
										<div class="item" data-hash="zero">
											<div class="content-project" data-src="images/zona-privada/producto-single.jpg">
												<img src="images/zona-privada/producto-single.jpg" alt="">
											</div>
										</div>
										<div class="item" data-hash="tow">
											<div class="content-project" data-src="images/zona-privada/producto-single.jpg">
												<img src="images/zona-privada/producto-single.jpg" alt="">
											</div>
										</div>
										<div class="item" data-hash="tre">
											<div class="content-project" data-src="images/zona-privada/producto-single.jpg">
												<img src="images/zona-privada/producto-single.jpg" alt="">
											</div>
										</div>
									</div>	
								</div>
								<div class="other-teals">
									<a href="#zero"><img src="images/zona-privada/producto-single.jpg" alt=""></a>
									<a href="#tow"><img src="images/zona-privada/producto-single.jpg" alt=""></a>
									<a href="#tre"><img src="images/zona-privada/producto-single.jpg" alt=""></a>

								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-5">
							<div class="panel-single">
								<h3>Nombre producto</h3>
								<span>$ 123.456</span>
								<p>100% cotton double printed dress. Black and white striped top and orange high waisted skater skirt bottom.</p>
								<h5 class="referncia">REFERENCIA:<span>HPM02C10FT01</span></h5>

								<div class="panel-price">
									<form class="form-inline">
									  <div class="form-group">
										<label for="exampleInputName2">CANTIDAD:</label>
										<input type="tel" class="form-control" id="exampleInputName2" placeholder="1+">
									  </div>
										<button class="bto-orange">Comprar</button>
										<button class="bto-orange">Cotizar</button>
										<button class="bto-orange">Comparar</button>
									</form>

								</div>
							</div>
						</div>
					</div>
				</div>
				
				
				<div class="conte-tabs">
				<div class="tab-button">
					<button class="active" data-url="content-a">Descripción</button>
					<button class="" data-url="content-b">Especificaciones</button>
				</div>

				<div class="tab-content">
					<div class="info-section" id="content-a">
						<p>Lorem Ipsumin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubianostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut.</p>

						<p>Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci.</p>
					</div>
					<div class="info-section" id="content-b">
						<p>Lorem Ipsumin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubianostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut.</p>

					</div>
				</div>
			</div>
				
			</div>
		</div>
		
		<div class="container">
			
		</div>
		
	</div>
	

</section>
<?php include ('footer.php') ?>