﻿<?php include ('header.php') ?>

<section class="main">

	<?php include ('breadcrumbs.php') ?>
	
	<div class="wrrape-suministro">
		<div class="title text-center">
			<h1>SECTORES</h1>
		</div>
				
		<div class="container">
			<div class="in-suministro">
				<div class="in-content">
					<div class="in-content-collapse">
						<div class="in-sectores">
							<div class="in-sectores-item">
								<div class="title-sector">
									<h2>Gobierno</h2>
								</div>
								
								<div class="in-sector-conte">
									<img src="images/icon-sector-2.png" alt="">

									<div class="in-sectore-text">
										<p>LAS MEJORES ENTIDADES ESTATALES , HaN confiado en Nexcomputer SAS , para ser uno de los proveedores mas importantes a nivel Colombia , con Hardware de computo instalado a  200  Entidades estatales con más de 5.000 Maquinas DURANTE el periodo del año 2017.</p>
									</div>
								</div>
							</div>
							<div class="in-sectores-item">
								<div class="title-sector">
									<h2>Industria</h2>
								</div>
								
								<div class="in-sector-conte">
									<img src="images/icon-sector-1.png" alt="">

									<div class="in-sectore-text">
										<p>Nexcom ha contado con gran experiencia en el Sector Corporativo , Diseñando con Fabricantes y nuestro  equipo de trabajo,  la arquitectura de vanguardia , de acuerdo a la necesidad de las empresas  al ser integrador de soluciones Tecnologicas que brinda gran calidad en sus servicios y dispocision de sus ingenieros. Los niveles de servicio ANS permitiran una infraestructura confiable garantizando la continuidad Tecnologica , en los proyectos que se han desarrollado.</p>
									</div>
								</div>
							</div>
							
							<div class="in-sectores-item">
								<div class="title-sector">
									<h2>Educación</h2>
								</div>
								
								<div class="in-sector-conte">
									<img src="images/icon-sector-4.png" alt="">

									<div class="in-sectore-text">
										<p>La trasmision del conocimiento para el crecimiento Organizacional es pilar de Nexcom dirigido a sus clientes , es por eso que  se han ejecutado diferentes  proyectos en el sector de educación, Universidades Publicas , Privadas , Instituciones educativas y Colegios , Aulas interactivas , Plataforma de e-Learning, e- Meating para permitir trabajo colaborativo sin reunir su equipo en una sola locacion, lo cual permite efectividad y mejora continua de la organizacion.</p>
									</div>
								</div>
							</div>
							<div class="in-sectores-item">
								<div class="title-sector">
									<h2>Salud</h2>
								</div>
								
								<div class="in-sector-conte">
									<img src="images/icon-sector-8.png" alt="">

									<div class="in-sectore-text">
										<p>Los sistemas de informacion y gestion a nivel medico deben permitir descongestionar las salas de espera, centralizacion y seguridad de la informacion. Estamos comprometidos con mejorar los precesos de atencion clinica.</p>
									</div>
								</div>
							</div>
							<div class="in-sectores-item">
								<div class="title-sector">
									<h2>Banca</h2>
								</div>
								
								<div class="in-sector-conte">
									<img src="images/icon-sector-5.png" alt="">

									<div class="in-sectore-text">
										<p>El sector Bancario está en constante crecimiento , Proveemos Hardware  Para Datacenter que brindan agilidad en procesamiento de datos y seguridad de la infraestructura a todo nivel , soluciones en outsoursing de impresión , soporte en linea.</p>
									</div>
								</div>
							</div>
							<div class="in-sectores-item">
								<div class="title-sector">
									<h2>Seguridad</h2>
								</div>
								
								<div class="in-sector-conte">
									<img src="images/icon-sector-6.png" alt="">

									<div class="in-sectore-text">
										<p>El sector de Seguridad cada vez tiene mas relevancia el complento electronico , Nexcom como Consultor  Ti ha participado en diferentes proyectos en donde la integracion de la Tecnologia es fundamental para garantizar que la seguridad electronica sea un complemento para la reduccion de riesgos.</p>
									</div>
								</div>
							</div>
							<div class="in-sectores-item">
								<div class="title-sector">
									<h2>Logística</h2>
								</div>
								
								<div class="in-sector-conte">
									<img src="images/icon-sector-7.png" alt="">

									<div class="in-sectore-text">
										<p>La infraestructura Tecnologica debe ser una aliado esencial para el mejoramiento de procesos , Las compañias exitosas Logisticamente se compromenten con alto nivel de apoyo en HaRDWARE DE Gestion y sistemas de informacion para control y eficiencia en su operación</p>
									</div>
								</div>
							</div>
							<div class="in-sectores-item">
								<div class="title-sector">
									<h2>Alimentos</h2>
								</div>
								
								<div class="in-sector-conte">
									<img src="images/icon-sector-3.png" alt="">

									<div class="in-sectore-text">
										<p> Nuestro equipo humano esta capacitado para asimilar y adaptar las nuevas tecnologías, en busca de una industria de alimentos competitiva y sostenible. Su formación integral le permite manejar los procesos de producción, comercialización, transporte y aseguramiento de la calidad que se llevan a cabo en cualquier empresa del sector de los alimentos , bebidas o afines</p>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="in-formInfo">
						<div class="in-boxform">
							<h2>OBTÉN MÁS INFORMACIÓN</h2>
							<form>
								<div class="">
									<div class="form-group">
										<input type="text" class="form-control" placeholder="Nombre de empresa">
									  </div>
									<div class="form-group">
										<input type="text" class="form-control" placeholder="Nombre">
									</div>
									<div class="form-group">
										<input type="email" class="form-control" placeholder="Email">
									</div>
									<div class="form-group">
										<input type="tel" class="form-control" placeholder="Teléfono">
									</div>
									<div class="form-group">
										<input type="tel" class="form-control" placeholder="Celular">
									</div>
									<div class="form-group">
										<textarea name="" id="" class="form-control"  placeholder="Cuéntenos su proyecto"></textarea>
									</div>

									<div class="checkbox">
									<label>
									  <input type="checkbox"> Acepto términos y condiciones
									</label>
								  </div>
									<div class="captcha">
										<img src="images/captcha.png" alt="" width="80%;">
									</div>
									<button type="submit" class="btn bto-orange">Enviar</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	
</section>
<?php include ('footer.php') ?>