<?php error_reporting(0); ?>
<!doctype html>
<html lang="es">
<head>
<meta charset="utf-8">
<title>Nexcome</title>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

   <!-- favicon links -->
<link rel="shortcut icon" type="image/ico" href="images/favicon.png" />
<link rel="icon" type="image/ico" href="images/favicon.png"/>
	
<!-- main css -->
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/mobile.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" type="text/css" href="css/lightcase.css"/>

<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="css/owl.theme.default.min.css">
	
	
<link rel="stylesheet" href="css/svg/fontello-codes.css">
<link rel="stylesheet" href="css/svg/fontello-embedded.css">
<link rel="stylesheet" href="css/svg/fontello-ie7-codes.css">
<link rel="stylesheet" href="css/svg/fontello-ie7.css">
<link rel="stylesheet" href="css/svg/fontello.css">
	
	
<link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
<link rel="stylesheet" href="js/lightgallery.min.js">
	
	
	
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

	
	
<!-- ZONA DE PIXELES -->	

	
<!-- ZONA DE PIXELES -->
	
	
	
<!-- Smartsupp Live Chat script -->
<script type="text/javascript">
var _smartsupp = _smartsupp || {};
_smartsupp.key = '93fe1da099655c550cfaa8fb0777f4f5a0b899e5';
window.smartsupp||(function(d) {
  var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
  s=d.getElementsByTagName('script')[0];c=d.createElement('script');
  c.type='text/javascript';c.charset='utf-8';c.async=true;
  c.src='https://www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
})(document);
</script>
	
	
	
</head>

<body>

<header>
<div id="wrapper">
 
<?php if ($vista == 'loging-head') {  ?>	
	 <nav class="navbar navbar-inverse navbar-fixed-top menu log-in" role="navegation"></nav>
<?php } else { ?> 
 <nav class="navbar navbar-inverse navbar-fixed-top menu" role="navegation">
	<div class="pre-head">
		<div class="container">
			<div class="pre-header">
				<div class="hed-lft">
					<div class="social">
						<!--<ul>
						<li><a href="#"><img src="images/icon-face.png" alt=""></a></li>
						<li><a href="#"><img src="images/icon-instagram.png" alt=""></a></li>
						<li><a href="#"><img src="images/icon-twitter.png" alt=""></a></li>
						</ul>-->
					</div>
					<div class="pbx-header">
						<a href="tel:5716517222"><img src="images/phone.png" alt="">PBX: 018000116398 Línea Nacional</a>
					</div>
					<div class="mail-header">
						<a href="mailto:marketing@nex.com.co"><img src="images/mail.png" alt="">marketing@nex.com.co</a>
					</div>
				</div>
				<div class="hed-rigt">
					<div class="cuenta-header">
						<a href="login.php"><img src="images/menu.png" alt="">Mi cuenta</a>
					</div>
					<div class="carrito-header">
						<!--<a href="#"><img src="images/carrito.png" alt="">Compra</a>-->
					</div>
				</div>
			</div>
	   </div>
	</div>
	 
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#desplegar">
				<span class="sr-only">Desplegar / Ocultar Menú</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<div class="logo">
				<a href="index.php"><img src="images/logo.png" alt=""></a>
			</div>
		</div>

		<div class="collapse navbar-collapse " id="desplegar">
			<ul class="nav navbar-nav">
					<li><a href="index.php">Home</a></li>
					<li><a href="empresa.php">Empresa</a></li>
					<li><a href="hardware.php">Hardware</a></li>
					<li><a href="soluciones.php">Soluciones TI</a></li>
					<li><a href="blog.php">Blog</a></li>
					<li><a href="contacto.php" >Contacto</a></li>
					<li class="cart">
						<div class="count">
							<p>$ 0.00</p>
							<p>0 items</p>
						</div>
						<img src="images/carrito-white.png" alt="">
					</li>
				</ul>
		</div>
	</div>
</nav>
<?php } ?>
	
</div>
</header>
	
	
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog bs-example-modal-md" role="document">
    <div class="modal-content">
      <div class="modal-body">
		<div class="embed-responsive embed-responsive-16by9">
		  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/2MpUj-Aua48"></iframe>
		</div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->	
	
	