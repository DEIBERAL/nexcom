﻿<?php include ('header.php') ?>
<section class="main">

	<?php include ('breadcrumbs.php') ?>
	
	<div class="wrrape-blog">
		<div class="container">
			<div class="in-blog">
				
				<div class="title text-center">
					<h1>BLOG</h1>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-8">
						<div class="in-box-blog">
							<div class="item-blog">
								<img src="images/blog-3.jpg" alt="">
								<div class="item-captionBlog">
								<span>18 Jul, 2018</span>
								<h2>Las Desktops ThinkCentre de Lenovo, flexibles y modulares.</h2>
								<p>os usuarios avanzados agradecerán el rendimiento y la capacidad de actualización de los sistemas de torre y de formato pequeño, capaces de realizar las tareas más exigentes.</p>
									<a href="single-blog.php" class="bto-orange">Ver más</a>
								</div>
							</div>
							<div class="item-blog">
								<img src="images/blog-1.jpg" alt="">
								<div class="item-captionBlog">
								<span>27 Mar, 2018</span>
								<h2>Lenovo se propone liderar los ordenadores de los ‘millennials’</h2>
								<p>El fabricante chino se prepara para desplazar a HP de la cumbre apoyándose en los nuevos profesionales.</p>
									<a href="single-blog.php" class="bto-orange">Ver más</a>
								</div>
							</div>
							<div class="item-blog">
								<img src="images/blog-2.jpg" alt="">
								<div class="item-captionBlog">
								<span>27 Mar, 2017</span>
								<h2>Lenovo Desafíos Jedi: sumérgete en el universo ‘Star Wars’</h2>
								<p>Probamos este 'kit' que incluye unas gafas de realidad aumentada, espada láser y baliza de rastreo.</p>
									<a href="single-blog.php" class="bto-orange">Ver más</a>
								</div>
							</div>
							<div class="item-blog-video embed-responsive embed-responsive-16by9">
								<iframe width="auto" height="315" class="embed-responsive-item" src="https://www.youtube.com/embed/GYLY3XQIujg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4">
						<div class="in-aside">
							<aside class="search">
								<div class="">
									<form action="" role="search" method="get">
									<div class="form-group">
										<input class="form-control" name="buscador" placeholder="Buscar..." type="search">
										<button class="but_sear" type="submit"><img src="images/lupa.png" alt=""></button>
										</div>
									</form>
								</div>
							</aside>
							<aside>
								<div class="titl-aside">
									<h2>Blogs Recientes</h2>
								</div>
								<div class="media">
								  <div class="media-left">
									<a href="single-blog.php">
									  <img class="media-object" src="images/blog-1.jpg" alt="...">
									</a>
								  </div>
								  <div class="media-body">
									<h4 class="media-heading">18 Jul, 2018</h4>
									<h2>Las Desktops ThinkCentre de Lenovog</h2>
								  </div>
								</div>
								<div class="media">
								  <div class="media-left">
									<a href="single-blog.php">
									  <img class="media-object" src="images/blog-2.jpg" alt="...">
									</a>
								  </div>
								  <div class="media-body">
									<h4 class="media-heading">27 Mar, 2017</h4>
									<h2>Lenovo se propone liderar los ordenadores de los ‘millennials’</h2>
								  </div>
								</div>
								<div class="media">
								  <div class="media-left">
									<a href="single-blog.php">
									  <img class="media-object" src="images/blog-3.jpg" alt="...">
									</a>
								  </div>
								  <div class="media-body">
									<h4 class="media-heading">27 Mar, 2017</h4>
									<h2>Lenovo Desafíos Jedi: sumérgete en el universo ‘Star Wars’</h2>
								  </div>
								</div>
							</aside>
							<aside>
								<div class="titl-aside">
									<h2>Keywords</h2>
								</div>
								<ul class="keywd">
									<li><a href="#">Keyword</a></li>
									<li><a href="#">Keyword</a></li>
									<li><a href="#">Keyword</a></li>
									<li><a href="#">Keyword</a></li>
									<li><a href="#">Keyword</a></li>
									<li><a href="#">Keyword</a></li>
								</ul>
							</aside>
							<aside>
								<div class="titl-aside">
									<h2>Categorías</h2>
								</div>
								<ul class="catg">
									<li><a href="#">Categoría</a></li>
									<li><a href="#">Categoría</a></li>
									<li><a href="#">Categoría</a></li>
									<li><a href="#">Categoría</a></li>
									<li><a href="#">Categoría</a></li>
								</ul>
							</aside>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>
<?php include ('footer.php') ?>