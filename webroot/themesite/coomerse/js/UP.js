
$(document).ready(function () {
	
$( function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 9000500,
      values: [ 0, 2000000 ],
      slide: function( event, ui ) {
        $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
      }
    });
    $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
      " - $" + $( "#slider-range" ).slider( "values", 1 ) );
  } );
	
$('.tab-content .info-section').hide();
$('.tab-content .info-section:nth-child(1)').show();

$('.tab-button button').click(function(v){
	v.preventDefault();
	$('.tab-button button').removeClass('active');
	$(this).addClass('active');
	$('.tab-content .info-section').hide();
	// obtenmos el dato

	var boton = $(this).attr('data-url');  				
	$('#'+boton).show();
	// animate
	//	var activeTab = $(this).attr('href');
	//	$(activeTab).show();
	return false;
});	


jQuery(function(){
if($('.in-boxform').length>0){
	$(window).scroll(function(){
		var pos = $(this).scrollTop();
		var maxScroll = Math.max( document.body.scrollHeight, document.body.offsetHeight, 
                   document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight );
		var cPercent = 	(pos * 100) / maxScroll;
		
		
		if(cPercent > 7 && cPercent < 23 ){
			$('.in-boxform').addClass('flotante');
			$('.in-boxform').fadeIn('slow');
			//$('.in-boxform').removeClass('flotanteFinal');
		}else{
			//$('.in-boxform').removeClass('flotanteFinal');
			$('.in-boxform').removeClass('flotante');
		}
		if(cPercent > 23){
			$('.in-boxform').removeClass('flotante');
			$('.in-boxform').fadeOut('slow');
		}
	});
  }
});

(function($){
	$(window).on("load",function(){
		$(".content").mCustomScrollbar({
			theme:"minimal-dark",
			 axis:"x",
		});
	});
})(jQuery);	


/*$(function () {
	$('.conectColapse .panel-default .panel-heading a').on("click", function () {
		var dest = $(".collapse.in").offset().top;
		$("html, body").animate({scrollTop: dest},500);
	});

});

function(d) {
  var e = a(this);
  e.attr("data-target") || d.preventDefault();
  var f = b(e),
    g = f.data("bs.collapse"),
    h = g ? "toggle" : e.data();
  c.call(f, h)
}
*/
	
$('.owl-home').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
	dots: true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});	
	
	
$('.owl-multimedia').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
	URLhashListener:true,
	autoplayHoverPause:true,
	startPosition: 'URLHash',
	dots: false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});

$('.owl-aliados').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
	dots: true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
});



$('.owl-carousel').owlCarousel({
    loop:false,
    margin:10,
    nav:false,
	autoplay:true,
    autoplayTimeout:1000,
    autoplayHoverPause:true,
	dots:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:6
        }
    }
});

$(document).ready(function(){
	var altura = $('.menu').offset().top;
	
	$(window).on('scroll', function(){
		if ( $(window).scrollTop() > altura ){
			$('.pre-head').addClass('prenav_hidden');
		} else {
			$('.pre-head').removeClass('prenav_hidden');
		}
	});
});

 $(document).ready(function() {
  $('.progress .progress-bar').css("width",
			function() {
				return $(this).attr("aria-valuenow") + "%";
			}
	)
});

	
$(document).ready(function() {
	$('#lightgallery').lightGallery({
		selector: '.content-project',
	});
});

/************************** OPCION UNO MENU *****************************/



$(document).ready(function() {
	$('.menu').addClass('blacki');
	
	$(window).scroll(function () {
	   	if($(this).scrollTop() == 0) {
	       $('.menu').addClass('blacki');
	    } else {
			$('.menu').removeClass('blacki');
		}
	});
});



$(document).ready(function() {
    $('.opaciAni').smoove({moveY:'40%'});
    $('.leftAni').smoove({moveX:'-50%'});
    $('.rightAni').smoove({moveX:'50%'});
	$('.animar_dos').smoove({rotateX:'90deg'});
	$('.block').smoove({offset:'40%'});
	$('.roteiro').smoove({rotate3d:'1,1,1,90deg'});
});


/*************************************    MAPA  *************************************/
/*************************************    MAPA  *************************************/
/*************************************    MAPA  *************************************/

	function initialize() {

		var myLatlng = new google.maps.LatLng(4.6634088, -74.05943960000002);

		var mapCanvas = document.getElementById('map-canvas');

		var mapOptions = {
			center: myLatlng,
			zoom:6,
			zoomControl: true,
			zoomControlOptions: {
				style: google.maps.ZoomControlStyle.SMALL
			},
			panControl: false,
			mapTypeControl: true,
			scrollwheel: false,
			styles: [
				  {
					"featureType": "poi.business",
					"stylers": [
					  {
						"visibility": "off"
					  }
					]
				  },
				  {
					"featureType": "poi.park",
					"elementType": "labels.text",
					"stylers": [
					  {
						"visibility": "off"
					  }
					]
				  }
				]
		};

		var map = new google.maps.Map(mapCanvas, mapOptions);

		new google.maps.Marker({
			position: myLatlng
			, map: map
			, title: ''
			, icon: ''
		}); 
		
		
        var marcador = new google.maps.Marker({
            position: new google.maps.LatLng(6.2208166, -75.56981619999999),
            map: map,
            title: 'Pulsa aqui'
        });
		
		var marcador = new google.maps.Marker({
            position: new google.maps.LatLng(4.8136617, -75.69757229999999),
            map: map,
            title: 'Pulsa aqui'
        });


	}
	google.maps.event.addDomListener(window, 'load', initialize);



/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
	
	 });