<footer>
	<div class="inhalt-content">
		<div class="container">
			
			<div class="row row-container">
				<div class="col-xs-12 col-sm-9">
					<div class="title-footer text-left">
						<h1>Contacto</h1>
						
						<!--<ul class="social-footer">
							<li><a href="#"><img src="images/icon-faceContact.png" alt=""></a></li>
							<li><a href="#"><img src="images/icon-twiterContact.png" alt=""></a></li>
							<li><a href="#"><img src="images/icon-inkedinContact.png" alt=""></a></li>
						</ul>-->
					</div>
					
					<div class="row row-footer">
						<div class="col-xs-12 col-sm-4">
							<div class="content-footer">
								<div class="item-footer">
									<img src="images/icono-footer1.png" alt="">
									<div>
										<h2>Bogotá</h2>
										<p>Cr. 16 # 76-31</p>
									</div>
								</div>
								
								<div class="item-footer">
									<img src="images/icono-footer2.png" alt="">
									<div>
										<h2>PBX</h2>
										<p>(571) 651 7222</p>
										<p>(571) 552 0777</p>
										<p>+57 305 8628703</p>
										<p>+57 305 8628716</p>
									</div>
								</div>
								
								<div class="item-footer">
									<img src="images/icono-footer3.png" alt="">
									<div>
										<h2>Email</h2>
										<p>marketing@nex.com.co</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4">
							<div class="content-footer">
								<div class="item-footer">
									<img src="images/icono-footer1.png" alt="">
									<div>
										<h2>Medellín</h2>
										<p>Edificio Block Centro Empresarial <br> Av. El Poblado Cra. 43a # 19 17 of. 303</p>
									</div>
								</div>
								
								<div class="item-footer">
									<img src="images/icono-footer2.png" alt="">
									<div>
										<h2>PBX</h2>
										<p>(574) 4031627</p>
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="col-xs-12 col-sm-4">
							<div class="content-footer">
								<div class="item-footer">
									<img src="images/icono-footer1.png" alt="">
									<div>
										<h2>Pereira</h2>
										<p>Centro Comercial Lago Plaza <br> LC. 132 Calle 23 # 8 - 55</p>
									</div>
								</div>
								
								<div class="item-footer">
									<img src="images/icono-footer2.png" alt="">
									<div>
										<h2>Línea Nacional</h2>
										<p>018000116398</p>
									</div>
								</div>
								
								
							</div>
						</div>
					</div>
				
				</div>
				<div class="col-xs-12 col-sm-3">
					<div class="title-footer text-left">
						<h1>Mapa del sitio</h1>
					</div>
				
					<ul class="map-site">
						<li><a href="index.php">Home</a></li>
						<li><a href="empresa.php">Empresa</a></li>
						<li><a href="hardware.php">Hardware</a></li>
						<li><a href="soluciones.php">Soluciones TI</a></li>
						<li><a href="blog.php">Blog</a></li>
						<li><a href="contacto.php">Contacto</a></li>
					</ul>
					
				
				</div>
			</div>
			
			<div class="title-footer text-center">
				<h1>Métodos de pago</h1>
			</div>
			<div class="mediosdepago">
				<img src="images/paypal.png" alt="">
				<img src="images/mastercard.png" alt="">
				<img src="images/visa.png" alt="">
				<img src="images/payu.png" alt="">
			</div>
		</div>
	</div>
	<div class="zlink">
		<p>© Nexcom 2018</p>
		<p>Habeas Data</p>
		<p>SAGRLAFT</p>
		<p><a href="#">Política de Cumplimiento</a></p>
		<p><a href="#">Recolección selectiva y gestión ambiental</a></p>
		<p><a href="#">Términos y condiciones</a></p>
	</div>
</footer>

<script src="js/jquery.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBvLywBdvmVazU_ClflbKpV39QpfNZtcFE"></script>
<script src="js/bootstrap.min.js"></script>
<script src="smove/dist/jquery.smoove.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.mCustomScrollbar.js"></script>
<script src="js/lightgallery.min.js"></script>

<script src="js/UP.js"></script>




<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker();
  } );
</script>

</body>
</html>