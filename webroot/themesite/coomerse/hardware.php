﻿<?php include ('header.php') ?>



<section class="main">

	<?php include ('breadcrumbs.php') ?>
	
	<div class="wrrape-suministro">
		<div class="title text-center">
			<h1>HARDWARE</h1>
		</div>
		
		<div class="in-suministro">
		<div class="container">
			<div class="in-content">
				<div class="in-content-collapse">
					<div class="conectColapse">
						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingOne">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
										<i class="demo-icon icon-01"></i>
										<h4 class="panel-title">
										 Hardware de Cómputo
										<span class="close-open pull-right"></span>
										</h4>
									</a>
								</div>
								<div id="collapseOne" class="panel-collapse collapse floting" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
								  <div class="panel-body">
									<div class="row">
									<div class="col-xs-12 col-sm-12">
										<p>Comercializamos las mejores marcas del mercado, para brindarle a su empresa la solucion idonea en Hardware y equipos de computo para el desarrollo tanto de tareas sencillas como de alta complejidad , conjugando un alto desempeño y productividad y respaldo de fabrica.</p>
									</div>
									 <div class="col-xs-12 col-sm-12">
										<img src="images/ft/harware-computo.jpg" alt="" width="100%">
										</div>
									 <div class="col-xs-12 col-sm-12">
										<div class="owl-carousel owl-theme">
									<div class="item"><div class="box-img" style="background-image:url(images/ft/logo/Lenovo.png)"></div></div>
									<div class="item"><div class="box-img" style="background-image:url(images/ft/logo/Dell.png)"></div></div>
									<div class="item"><div class="box-img" style="background-image:url(images/ft/logo/HP.png)"></div></div>
									<div class="item"><div class="box-img" style="background-image:url(images/ft/logo/Asus.png)"></div></div>
									<div class="item"><div class="box-img" style="background-image:url(images/ft/logo/acer.png)"></div></div>
										</div>
									</div>
									 </div>
								  </div>
								</div>
							  </div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingTwo">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
										<i class="demo-icon icon-02"></i>
									  <h4 class="panel-title">
										 Impresión y escáner
										<span class="close-open pull-right"></span>
									  </h4>
									</a>
								</div>
								<div id="collapseTwo" class="panel-collapse collapse floting" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">
									<div class="panel-body">
									<div class="row">
									<div class="col-xs-12 col-sm-12">
										<p>Encontrara las marcas mas reconocidas del mercado, para brindarle a su empresa la mejor solucion en impresión y escaner, si esta pensando en implementar gestion documental en sus procesos, Nexcom sera su mejor aliado para entregarle la mejor solucion y suministros para el desarrollo de esta labor.</p>
									</div>
									 <div class="col-xs-12 col-sm-12">
										<img src="images/impresion.png" alt="" width="100%">
										</div>
									 <div class="col-xs-12 col-sm-12">
										<div class="owl-carousel owl-theme">
										<div class="item"><div class="box-img" style="background-image:url(images/sold/icon-2.png)"></div></div>
										<div class="item"><div class="box-img" style="background-image:url(images/sold/icon-4.png)"></div></div>
										<div class="item"><div class="box-img" style="background-image:url(images/ft/logo/Epson.png)"></div></div>
										<div class="item"><div class="box-img" style="background-image:url(images/ft/logo/HP.png)"></div></div>
										<div class="item"><div class="box-img" style="background-image:url(images/sold/icon-1.png)"></div></div>
										<div class="item"><div class="box-img" style="background-image:url(images/ft/logo/Kodak-alaris.png)"></div></div>
										<div class="item"><div class="box-img" style="background-image:url(images/ft/logo/zebra.png)"></div></div>
										</div>
									</div>
									 </div>
								  </div>
								</div>
							  </div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingThree">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
										<i class="demo-icon icon-03"></i>
										<h4 class="panel-title">
									  Vídeo, Audio y Proyección
										<span class="close-open pull-right"></span>
										</h4>
									</a>
								</div>
								<div id="collapseThree" class="panel-collapse collapse floting" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
								  <div class="panel-body">
									<div class="row">
									<div class="col-xs-12 col-sm-12">
										<p>Suministramos las marcas mas reconocidas del mercado y exclusivas en todo el área de video, audio y proyección, donde podrá integrar su proyecto de acuerdo a su necesidad , especializados en carteleria digital , Video wall , monitores industriales y soluciones interactivas enfocadas en el sector de educacion. </p>
									</div>
									 <div class="col-xs-12 col-sm-12">
										<img src="images/somunistros-2.jpg" alt="" width="100%">
										</div>
									 <div class="col-xs-12 col-sm-12">
										<div class="owl-carousel owl-theme">
										<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/LG.png)"></div></div>
										<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/Samsung.png)"></div></div>
										<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/Epson.png)"></div></div>
										<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/Logitech.png)"></div></div>
										<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/Bose-Profesional.png)"></div></div>
										<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/IQBOARD.png)"></div></div>
										<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/Panasonic.png)"></div></div>
										</div>
									</div>
									 </div>
								  </div>
								</div>
							  </div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headfour">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#sum-four" aria-expanded="false" aria-controls="sum-four">
										<i class="demo-icon icon-04"></i>
										<h4 class="panel-title">
									 Conectividad y redes
										<span class="close-open pull-right"></span>
										</h4>
									</a>
								</div>
								<div id="sum-four" class="panel-collapse collapse floting" role="tabpanel" aria-labelledby="headfour" aria-expanded="false">
								  <div class="panel-body">
									<div class="row">
									<div class="col-xs-12 col-sm-12">
										<p>Sabemos que la conectividad en una empresa es parte fundamental para el desarrollo del dia a dia y desarrollo de las actividades, Nexcom como consultor TIC , ofrece las mejores marcas en conectividad para estar siempre estar conectados.</p>
									</div>
									 <div class="col-xs-12 col-sm-12">
										<img src="images/ft/REDES-Y-CONECTIVIDAD.jpg" alt="" width="100%">
										</div>
									 <div class="col-xs-12 col-sm-12">
										<div class="owl-carousel owl-theme">
									<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/Tp-link.png)"></div></div>
									<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/Aruba.png)"></div></div>
									<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/Cisco.png)"></div></div>
									<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/mikrotik-logo.png)"></div></div>
									<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/Linksys.png)"></div></div>
									<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/Ubiquiti-05.png)"></div></div>
									<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/HUAWEI.png)"></div></div>
										</div>
									</div>
									 </div>
								  </div>
								</div>
							  </div>
							<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headfive">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#sum-five" aria-expanded="false" aria-controls="sum-five">
										<i class="demo-icon icon-05"></i>
										<h4 class="panel-title">
									  Servidores y almacenamiento
										<span class="close-open pull-right"></span>
										</h4>
									</a>
								</div>
								<div id="sum-five" class="panel-collapse collapse floting" role="tabpanel" aria-labelledby="headfive" aria-expanded="false">
								  <div class="panel-body">
									<div class="row">
									<div class="col-xs-12 col-sm-12">
										<p>Proveemos las marcas más reconocidas en servidores de computo con características de alto desempeño para su DataCenter , así como componentes de almacenamiento:  memorias, discos duros, unidades de almacenamiento según las necesidades de su empresa.</p>
									</div>
									 <div class="col-xs-12 col-sm-12">
										<img src="images/ft/SERVIDORES-Y-ALMACENIMIENTO.jpg" alt="" width="100%">
										</div>
									 <div class="col-xs-12 col-sm-12">
										<div class="owl-carousel owl-theme">
										<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/Lenovo.png)"></div></div>
										<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/LOGO-DELL.png)"></div></div>
										<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/HUAWEI.png)"></div></div>
										<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/Hitachi_logo.png)"></div></div>
										<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/LOGO-QNAP.png)"></div></div>
										</div>
									</div>
									 </div>
								  </div>
								</div>
							 </div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headsix">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#sum-six" aria-expanded="false" aria-controls="sum-six">
										<i class="demo-icon icon-06"></i>
										<h4 class="panel-title">
									  	Software y aplicativos
										<span class="close-open pull-right"></span>
										</h4>
									</a>
								</div>
								<div id="sum-six" class="panel-collapse collapse floting" role="tabpanel" aria-labelledby="headsix" aria-expanded="false">
								  <div class="panel-body">
									<div class="row">
									<div class="col-xs-12 col-sm-12">
										<p>INTEGRAMOS  la mejores soluciones de Software y aplicativos como ;  Suite de Antivirus, licencias Microsoft  Windows , Server , Office y aplicaciones de oficina más utilizadas en el mercado diseñadas, pensadas para mejorar la productividad continua de la empresa. </p>
									</div>
									 <div class="col-xs-12 col-sm-12">
										<img src="images/ft/SOFTWARE-APLICATIVOS.jpg" alt="" width="100%">
										</div>
									 <div class="col-xs-12 col-sm-12">
										<div class="owl-carousel owl-theme">
										<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/Microsoft.png)"></div></div>
										<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/MICROSOFT-365.png)"></div></div>
										<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/McAfee.png)"></div></div>
										<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/eset-logo.png)"></div></div>
										</div>
									</div>
									 </div>
								  </div>
								</div>
							  </div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headseven ">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#sum-seven" aria-expanded="false" aria-controls="sum-seven">
										<i class="demo-icon icon-08"></i>
										<h4 class="panel-title">
									  Potencia y protección electrica
										<span class="close-open pull-right"></span>
										</h4>
									</a>
								</div>
								<div id="sum-seven" class="panel-collapse collapse floting" role="tabpanel" aria-labelledby="headseven" aria-expanded="false">
								  <div class="panel-body">
									<div class="row">
									<div class="col-xs-12 col-sm-12">
										<p>Sabemos que proteger los activos de una empresa es fundamental para el desarrollo , funcionalidad y buen funcionamiento de los equipos , Nexcom como consultor TIC ofrece las mejores marcas en proteccion electrcia ; UPS,Generadores electricos. </p>
									</div>
									 <div class="col-xs-12 col-sm-12">
										<img src="images/ft/UPS--REGULADA.jpg" alt="" width="100%">
										</div>
									 <div class="col-xs-12 col-sm-12">
										<div class="owl-carousel owl-theme">
											<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/Vertiv.png)"></div></div>
											<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/Tripp-Lite.png)"></div></div>
											<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/apc.png)"></div></div>
										</div>
									</div>
									 </div>
								  </div>
								</div>
							  </div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headheigt">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#sum-height" aria-expanded="false" aria-controls="sum-height">
										<i class="demo-icon icon-07"></i>
										<h4 class="panel-title">
									  Seguridad Perimetral
										<span class="close-open pull-right"></span>
										</h4>
									</a>
								</div>
								<div id="sum-height" class="panel-collapse collapse floting" role="tabpanel" aria-labelledby="headheigt" aria-expanded="false">
								  <div class="panel-body">
									<div class="row">
									<div class="col-xs-12 col-sm-12">
										<p>Encontrara las marcas más completas del mercado que le permiten a su empresa un concepto de protección integrado en los sistemas de seguridad física, en el control de acceso, circuito cerrado de televisión (CCTV), iluminación y comunicaciones.</p>
									</div>
									 <div class="col-xs-12 col-sm-12">
										<img src="images/ft/SEGURIDAD-PERIMETRAL.jpg" alt="" width="100%">
										</div>
									 <div class="col-xs-12 col-sm-12">
										<div class="owl-carousel owl-theme">
											<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/FORTINET.png)"></div></div>
											<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/SOPHOS.png)"></div></div>
											<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/cctv.png)"></div></div>
										</div>
									</div>
									 </div>
								  </div>
								</div>
							  </div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headnine">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#sum-nine" aria-expanded="false" aria-controls="sum-nine">
										<i class="demo-icon icon-09"></i>
										<h4 class="panel-title">
									  Suministros de computo
										<span class="close-open pull-right"></span>
										</h4>
									</a>
								</div>
								<div id="sum-nine" class="panel-collapse collapse floting" role="tabpanel" aria-labelledby="headnine" aria-expanded="false">
								  <div class="panel-body">
									<div class="row">
									<div class="col-xs-12 col-sm-12">
										<p>Sabemos que mantener los equipos de computo en optimas condiciones para su funcionamiento, es fundamental para el proceso productivo de su empresa, es por ello que Nexcom su consultor TIC pone a su disposicion los componentes para un correcto desempeño en sus equipos de computo y la base instalada de su empresa. Tenemos el respaldo de grandes fabricantes de talla mundial.</p>
									</div>
									 <div class="col-xs-12 col-sm-12">
										<img src="images/ft/HARDWARE.jpg" alt="" width="100%">
										</div>
									 <div class="col-xs-12 col-sm-12">
										<div class="owl-carousel owl-theme">
											<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/Kingston.png)"></div></div>
											<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/CRUCIAL.png)"></div></div>
											<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/ADATA.png)"></div></div>
											<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/segate.png)"></div></div>
											<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/Hitachi_logo.png)"></div></div>
										</div>
									</div>
									 </div>
								  </div>
								</div>
							  </div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headten">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#sum-ten" aria-expanded="false" aria-controls="sum-ten">
										<i class="demo-icon icon-10"></i>
										<h4 class="panel-title">
									  Telefonia IP
										<span class="close-open pull-right"></span>
										</h4>
									</a>
								</div>
								<div id="sum-ten" class="panel-collapse collapse floting" role="tabpanel" aria-labelledby="headten" aria-expanded="false">
								  <div class="panel-body">
									<div class="row">
									<div class="col-xs-12 col-sm-12">
										<p>IMPLEMENTAMOS SOLUCIONES CON los mejores equipos para la integración en comunicaciones de voz, datos y video con el uso de la telefonía IP (VoIP) logrando así la gestión centralizada y optimizada de sus comunicaciones, la integración de las diferentes sedes de su empresa, llamadas internas gratuitas y el acceso a funcionalidades avanzadas (buzones de voz, IVR, entre otros).</p>
									</div>
									 <div class="col-xs-12 col-sm-12">
										<img src="images/ft/TELEFONIA-IP.jpg" alt="" width="100%">
										</div>
									 <div class="col-xs-12 col-sm-12">
										<div class="owl-carousel owl-theme">
											<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/Tp-link.png)"></div></div>
											<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/ast.png)"></div></div>
											<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/Avaya.png)"></div></div>
											<div class="item"><div class="box-img" style="background-image: url(images/ft/logo/YEALINK.png)"></div></div>
											
										</div>
									</div>
									 </div>
								  </div>
								</div>
							  </div>
						</div>
					</div>
				</div>
				
				<div class="in-formInfo">
					<div class="in-boxform">
						<h2>OBTÉN MÁS INFORMACIÓN</h2>
						<form>
							<div class="">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Nombre de empresa">
								  </div>
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Nombre">
								</div>
								<div class="form-group">
									<input type="email" class="form-control" placeholder="Email">
								</div>
								<div class="form-group">
									<input type="tel" class="form-control" placeholder="Teléfono">
								</div>
								<div class="form-group">
									<input type="tel" class="form-control" placeholder="Celular">
								</div>
								<div class="form-group">
									<textarea name="" id="" class="form-control"  placeholder="Cuéntenos su proyecto"></textarea>
								</div>

								<div class="checkbox">
								<label>
								  <input type="checkbox"> Acepto términos y condiciones
								</label>
							  </div>
								<div class="captcha">
									<img src="images/captcha.png" alt="" width="80%;">
								</div>
								<button type="submit" class="btn bto-orange">Enviar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			
			
			
		</div>
	</div>
	</div>


</section>
<?php include ('footer.php') ?>