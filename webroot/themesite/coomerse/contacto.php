﻿<?php include ('header.php') ?>
<section class="main">
	
<?php include ('breadcrumbs.php') ?>
	
	<div class="wrrape-contacto">
		<div class="in-contacto">
			<div class="title text-center">
				<h1>CONTACTO</h1>
			</div>
			
			<div class="container">
				<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div class="call-us">
						<h2>Llámanos</h2>
						<p>¿Quieres hablar ahora mismo? <br> Comunícate al:</p>
					</div>
					<div class="in-contact">
						<div class="in-box-contet">
							<img src="images/pbx.png" alt="">
							<div class="in-contact-info">
								<div class="row">
									<div class="col-xs-12 col-sm-5">
										<p><h2>Bogotá:</h2>&nbsp;</p>
										<p><h2>&nbsp;</h2>(571) 651 7222</p>
										<p><h2>&nbsp;</h2>(571) 552 0777</p>
										<p><h2>&nbsp;</h2>+57 305 8628703</p>
										<p><h2>&nbsp;</h2>+57 305 8628716</p>
									</div>
									<div class="col-xs-12 col-sm-5">
										<p><h2>Medellín:</h2>&nbsp;</p>
										<p><h2>&nbsp;</h2>(574) 403 16 27</p>
										
										<p><h2>Línea Nacional:</h2>&nbsp;</p>
										<p><h2>&nbsp;</h2>018000116398</p>
									</div>
								</div>
							</div>
						</div>
						<div class="call-us">
							<h2>Escríbenos</h2>
						</div>
						<div class="in-box-contet">
							<img src="images/mail-contacto.png" alt="">
							<div class="in-contact-info">
								<p><h2>Email:</h2> marketing@nex.com.co</p>
								<p><h2>Whatsapp:</h2> (57) 3176463426</p>
							</div>
						</div>
					</div>
					<!--<ul class="social-contact">
						<li><a href="#"><img src="images/icon-faceContact.png" alt=""></a></li>
						<li><a href="#"><img src="images/icon-inkedinContact.png" alt=""></a></li>
						<li><a href="#"><img src="images/icon-twiterContact.png" alt=""></a></li>
					</ul>-->
					
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="content-info-form">
						<form>
							<div class="row">
								<div class="col-xs-12 col-sm-12">
								  <div class="form-group">
									<input type="text" class="form-control" placeholder="Nombre de empresa">
								  </div>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="form-group">
										<input type="text" class="form-control" placeholder="Nombre de contacto">
								  	</div>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="form-group">
										<input type="email" class="form-control" placeholder="Email">
								  	</div>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="form-group">
										<input type="tel" class="form-control" placeholder="Teléfono">
								  	</div>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="form-group">
										<input type="tel" class="form-control" placeholder="Celular">
								  	</div>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="form-group">
										<textarea name="" id="" class="form-control"  placeholder="Cuéntenos su proyecto"></textarea>
								  	</div>
									
									<div class="checkbox">
									<label>
									  <input type="checkbox"> Acepto términos y condiciones
									</label>
								  </div>
									
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="captcha">
										<img src="images/captcha.png" alt="" width="80%;">
									</div>
						  		</div>
								<button type="submit" class="btn bto-white">Enviar</button>
							</div>
						</form>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="in-contentContact">
		<a href="https://goo.gl/maps/6zF3Tyx8jtk" target="_blank"><img src="images/maps-contacto.jpg" alt="" width="100%"></a>
	</div>
	

</section>
<?php include ('footer.php') ?>