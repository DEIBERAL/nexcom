﻿<?php include ('header.php') ?>

<section class="main">

	<div class="wrrape-home">
		<div class="in-slider">
			<div class="owl-home owl-carousel owl-theme">
				<div class="item">
					<div class="content-img" style="background-image: url(images/01.jpg)">
						<div class="content-text">
							<h1>Improve & <br> boost your <br> traffic with score</h1>
							<a href="#" class="bto-white">Ver más</a>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="content-img" style="background-image: url(images/01.jpg)">
						<div class="content-text">
							<h1>Improve & <br> boost your <br> traffic with score</h1>
							<a href="#" class="bto-white">Ver más</a>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="content-img" style="background-image: url(images/01.jpg)">
						<div class="content-text">
							<h1>Improve & <br> boost your <br> traffic with score</h1>
							<a href="#" class="bto-white">Ver más</a>
						</div>
					</div>
				</div>

			</div>	
		</div>	
	</div>
	
	<div class="wrrape-nosotros">
		<div class="container">
			<div class="title text-center">
				<h2>NOSOTROS</h2>
			</div>
			<div class="content-nosot">
				<div class="box-nosot">
					<h3>Trayectoria</h3>
					<div class="progress skill-bar">
						<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="60" aria-valuemax="100" >
							<span class="skill"><i class="val">16 años</i></span>
						</div>
          		  </div>
					
					<h3>Consultora en TIC’s</h3>
				</div>
				<div class="box-sector">
					<h3>Sectores</h3>
					<div class="conten-sector">
						<div class="item-sector">
							<img src="images/icon-nos1.png" alt="">
							<span>Gobierno</span>
						</div>
						<div class="item-sector">
							<img src="images/icon-nos2.png" alt="">
							<span>Industrial</span>
						</div>
						<div class="item-sector">
							<img src="images/icon-nos3.png" alt="">
							<span>Educación</span>
						</div>
						<div class="item-sector">
							<img src="images/icon-nos4.png" alt="">
							<span>Salud</span>
						</div>
					</div>
					<a href="sectores.php" class="bto-orange">Ver más</a>
				</div>
			</div>
		</div>
	</div>
	
	

	
	<div class="wrrape-infografia">
		<div class="container">
			<div class="in-infografi">
				<div class="title text-center">
					<h2>QUE HACEMOS</h2>
				</div>
				<!-- Button trigger modal -->
				<button type="button" class="" data-toggle="modal" data-target="#myModal">
				 <img src="images/infografia-1.png" alt="">
				</button>
				<div class="text-center">
					<a href="empresa.php" class="bto-orange">Ver más</a>
				</div>
			</div>
		</div>
	</div>
	
	<div class="wrrape-categoria">
		<div class="container">
			<div class="title text-center">
				<h2>CATEGORÍAS</h2>
			</div>
			<div class="">
				<div class="in-categoria">
					<div class="in-box-cat">
						<img src="images/icon-cat1.png" alt="">
						<span>Equipos de Cómputo</span>
					</div>
					<div class="in-box-cat">
						<img src="images/icon-cat2.png" alt="">
						<span>Impresión y escáner</span>
					</div>
					<div class="in-box-cat">
						<img src="images/icon-cat3.png" alt="">
						<span>Redes e infraestructura</span>
					</div>
					<div class="in-box-cat">
						<img src="images/icon-cat4.png" alt="">
						<span>Servidores y almacenamiento</span>
					</div>
					<div class="in-box-cat">
						<img src="images/icon-cat5.png" alt="">
						<span>Software y aplicativos</span>
					</div>
				</div>
				<div class="text-center">
					<a href="suministros.php" class="bto-orange">Ver más</a>
				</div>
			</div>
		</div>
	</div>
	
		<div class="wrrape-info">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div class="content-info">
						<p>Para Nexcom es muy importante, conocerte y ayudarte con suministros y soluciones TI que mejoren la competitividad de tu empresa</p>
						<span>¡Cuéntanos <br> tu proyecto!</span>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="content-info-form">
						<div class="title text-left">
							<h2>OBTÉN MÁS INFORMACIÓN</h2>
						</div>
						<form>
							<div class="row">
								<div class="col-xs-12 col-sm-12">
								  <div class="form-group">
									<input type="text" class="form-control" placeholder="Nombre de empresa">
								  </div>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="form-group">
										<input type="text" class="form-control" placeholder="Nombre">
								  	</div>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="form-group">
										<input type="email" class="form-control" placeholder="Email">
								  	</div>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="form-group">
										<input type="tel" class="form-control" placeholder="Teléfono">
								  	</div>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="form-group">
										<input type="tel" class="form-control" placeholder="Celular">
								  	</div>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="form-group">
										<textarea name="" id="" class="form-control"  placeholder="Cuéntenos su proyecto"></textarea>
								  	</div>
									
									<div class="checkbox">
									<label>
									  <input type="checkbox"> Acepto términos y condiciones
									</label>
								  </div>
									
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="captcha">
										<img src="images/captcha.png" alt="" width="80%;">
									</div>
						  		</div>
								<button type="submit" class="btn bto-orange">Enviar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>


</section>
<?php include ('footer.php') ?>