﻿<?php include ('header.php') ?>

<section class="main">

	<?php include ('breadcrumbs.php') ?>
	
	<div class="wrrape-we">
		<div class="title text-center">
			<h1>EMPRESA</h1>
		</div>
		
		<div class="in-we">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<p>Nexcom S.A.S compañía consultora en TIC’s por más de 16 años en el sector de tecnología en Colombia, goza de reconocimiento a nivel Nacional, por la ejecución de múltiples proyectos destacándose en el sector Gobierno y Corporativo.</p>

						<p>Nexcom S.A.S espera ser su mayor aliado estratégico, en la integración de soluciones tecnológicas.</p>

						<p>Contamos con profesionales certificados en las soluciones que ofrecemos. </p>

						<p>Gracias a la experiencia, nos respaldan marcas de talla mundial y nos diferenciamos por la integración eficiente y solidez en la estructuración de proyectos exitosos, obteniendo como resultado reconocimiento de los siguientes fabricantes; Lenovo categoría Platinum, Microsoft categoría Silver en DataCenter, Intel Platinum, Ricoh y Kodak .</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="wrrape-nosotros in-page-nosotros">
		<div class="container">
			<div class="content-nosot">
				<div class="box-img">
					<img src="images/crokis.png" alt="">
				</div>
				<div class="con-boxis">
				<div class="box-nosot">
					<h2>Trayectoria</h2>
					<div class="progress skill-bar">
						<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="60" aria-valuemax="100" >
							<span class="skill"><i class="val">16 años</i></span>
						</div>
          		  </div>
					
					<h2>Consultora en TIC’s</h2>
				</div>
				<div class="box-sector">
					<h2>Sectores</h2>
					<div class="conten-sector">
						<div class="item-sector">
							<img src="images/icon-nos1.png" alt="">
							<span>Gobierno</span>
						</div>
						<div class="item-sector">
							<img src="images/icon-nos2.png" alt="">
							<span>Industrial</span>
						</div>
						<div class="item-sector">
							<img src="images/icon-nos3.png" alt="">
							<span>Educación</span>
						</div>
						<div class="item-sector">
							<img src="images/icon-nos4.png" alt="">
							<span>Salud</span>
						</div>
					</div>
					<a href="sectores.php" class="bto-orange">Ver más</a>
				</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="wrrape-infografia">
		<div class="container">
			<div class="in-infografi info-2">
				<div class="title text-center">
					<h2>QUE HACEMOS</h2>
				</div>
				<img src="images/infografia-2.png" alt="">
				<br>
				<br>
			</div>
		</div>
	</div>
	
	<!--<div class="wrrape-certificacion">
		<div class="container">
			<div class="title text-center">
				<h1>CERTIFICACIONES</h1>
			</div>
			<div class="in-award">
				<div class="in-box-award">
					<span>Nombre Certificación</span>
					<span>2018</span>
				</div>
				<div class="in-box-award">
					<span>Nombre Certificación</span>
					<span>2018</span>
				</div>
				<div class="in-box-award">
					<span>Nombre Certificación</span>
					<span>2018</span>
				</div>
				<div class="in-box-award">
					<span>Nombre Certificación</span>
					<span>2018</span>
				</div>
				<div class="in-box-award">
					<span>Nombre Certificación</span>
					<span>2018</span>
				</div>
			</div>
		</div>
	</div>
	-->
	
	<div class="wrrape-reconocimiento">
		<div class="container">
				<div class="title text-center">
					<h2>RECONOCIMIENTOS</h2>
				</div>
				<div class="in-reconocimiento">
					<img src="images/reconocimiento-1.jpg" alt="">
					<img src="images/reconocimiento-2.jpg" alt="">
					<img src="images/reconocimiento-3.jpg" alt="">
				</div>
			</div>
		</div>
	
	
</section>
<?php include ('footer.php') ?>