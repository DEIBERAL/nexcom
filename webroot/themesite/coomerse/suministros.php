﻿<?php include ('header.php') ?>



<section class="main">

	<?php include ('breadcrumbs.php') ?>
	
	<div class="wrrape-suministro">
		<div class="title text-center">
			<h1>HARDWARE</h1>
		</div>
		
		<div class="in-suministro">
		<div class="container">
			<div class="in-content">
				<div class="in-content-collapse">
					<div class="conectColapse">
						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingOne">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
										<i class="demo-icon icon-01"></i>
										<h4 class="panel-title">
										 Hardware de Cómputo
										<span class="close-open pull-right"></span>
										</h4>
									</a>
								</div>
								<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
								  <div class="panel-body">
									<div class="row">
									 <div class="col-xs-12 col-sm-5">
										<img src="images/somunistros-1.jpg" alt="" width="100%">
										</div>
									 <div class="col-xs-12 col-sm-7">
										<p>Comercializamos las mejores marcas del mercado, para brindarle a su empresa la solucion idonea en Hardware y equipos de computo para el desarrollo tanto de tareas sencillas como de alta complejidad , conjugando un alto desempeño y productividad.</p>
									</div>
									 </div>
								  </div>
								</div>
							  </div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingTwo">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
										<i class="demo-icon icon-02"></i>
									  <h4 class="panel-title">
										 Impresión y escáner
										<span class="close-open pull-right"></span>
									  </h4>
									</a>
								</div>
								<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">
									<div class="panel-body">
									  <div class="row">
										 <div class="col-xs-12 col-sm-5">
											<img src="images/somunistros-1.jpg" alt="" width="100%">
										</div>
										 <div class="col-xs-12 col-sm-7">
											<p>Comercializamos las mejores marcas del mercado, para brindarle a su empresa la solucion idonea en Hardware y equipos de computo para el desarrollo tanto de tareas sencillas como de alta complejidad , conjugando un alto desempeño y productividad.</p>
										</div>
										 </div>
									</div>
								</div>
							  </div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingThree">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
										<i class="demo-icon icon-03"></i>
										<h4 class="panel-title">
									  Vídeo, Audio y Proyección
										<span class="close-open pull-right"></span>
										</h4>
									</a>
								</div>
								<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
								  <div class="panel-body">
									<div class="row">
									 <div class="col-xs-12 col-sm-5">
										<img src="images/somunistros-1.jpg" alt="" width="100%">
										</div>
									 <div class="col-xs-12 col-sm-7">
										<p>Comercializamos las mejores marcas del mercado, para brindarle a su empresa la solucion idonea en Hardware y equipos de computo para el desarrollo tanto de tareas sencillas como de alta complejidad , conjugando un alto desempeño y productividad.</p>
									</div>
									 </div>
								  </div>
								</div>
							  </div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headfour">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#sum-four" aria-expanded="false" aria-controls="sum-four">
										<i class="demo-icon icon-04"></i>
										<h4 class="panel-title">
									 Conectividad y redes
										<span class="close-open pull-right"></span>
										</h4>
									</a>
								</div>
								<div id="sum-four" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headfour" aria-expanded="false">
								  <div class="panel-body">
									<div class="row">
									 <div class="col-xs-12 col-sm-5">
										<img src="images/somunistros-1.jpg" alt="" width="100%">
										</div>
									 <div class="col-xs-12 col-sm-7">
										<p>Comercializamos las mejores marcas del mercado, para brindarle a su empresa la solucion idonea en Hardware y equipos de computo para el desarrollo tanto de tareas sencillas como de alta complejidad , conjugando un alto desempeño y productividad.</p>
									</div>
									 </div>
								  </div>
								</div>
							  </div>
							<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headfive">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#sum-five" aria-expanded="false" aria-controls="sum-five">
										<i class="demo-icon icon-05"></i>
										<h4 class="panel-title">
									  Servidores y almacenamiento
										<span class="close-open pull-right"></span>
										</h4>
									</a>
								</div>
								<div id="sum-five" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headfive" aria-expanded="false">
								  <div class="panel-body">
									<div class="row">
									 <div class="col-xs-12 col-sm-5">
										<img src="images/somunistros-1.jpg" alt="" width="100%">
										</div>
									 <div class="col-xs-12 col-sm-7">
										<p>Comercializamos las mejores marcas del mercado, para brindarle a su empresa la solucion idonea en Hardware y equipos de computo para el desarrollo tanto de tareas sencillas como de alta complejidad , conjugando un alto desempeño y productividad.</p>
									</div>
									 </div>
								  </div>
								</div>
							 </div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headsix">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#sum-six" aria-expanded="false" aria-controls="sum-six">
										<i class="demo-icon icon-06"></i>
										<h4 class="panel-title">
									  	Software y aplicativos
										<span class="close-open pull-right"></span>
										</h4>
									</a>
								</div>
								<div id="sum-six" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headsix" aria-expanded="false">
								  <div class="panel-body">
									<div class="row">
									 <div class="col-xs-12 col-sm-5">
										<img src="images/somunistros-1.jpg" alt="" width="100%">
										</div>
									 <div class="col-xs-12 col-sm-7">
										<p>Comercializamos las mejores marcas del mercado, para brindarle a su empresa la solucion idonea en Hardware y equipos de computo para el desarrollo tanto de tareas sencillas como de alta complejidad , conjugando un alto desempeño y productividad.</p>
									</div>
									 </div>
								  </div>
								</div>
							  </div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headseven">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#sum-seven" aria-expanded="false" aria-controls="sum-seven">
										<i class="demo-icon icon-07"></i>
										<h4 class="panel-title">
									  Potencia y protección electrica
										<span class="close-open pull-right"></span>
										</h4>
									</a>
								</div>
								<div id="sum-seven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headseven" aria-expanded="false">
								  <div class="panel-body">
									<div class="row">
									 <div class="col-xs-12 col-sm-5">
										<img src="images/somunistros-1.jpg" alt="" width="100%">
										</div>
									 <div class="col-xs-12 col-sm-7">
										<p>Comercializamos las mejores marcas del mercado, para brindarle a su empresa la solucion idonea en Hardware y equipos de computo para el desarrollo tanto de tareas sencillas como de alta complejidad , conjugando un alto desempeño y productividad.</p>
									</div>
									 </div>
								  </div>
								</div>
							  </div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headheigt">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#sum-height" aria-expanded="false" aria-controls="sum-height">
										<i class="demo-icon icon-08"></i>
										<h4 class="panel-title">
									  Seguridad Perimetral
										<span class="close-open pull-right"></span>
										</h4>
									</a>
								</div>
								<div id="sum-height" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headheigt" aria-expanded="false">
								  <div class="panel-body">
									<div class="row">
									 <div class="col-xs-12 col-sm-5">
										<img src="images/somunistros-1.jpg" alt="" width="100%">
										</div>
									 <div class="col-xs-12 col-sm-7">
										<p>Comercializamos las mejores marcas del mercado, para brindarle a su empresa la solucion idonea en Hardware y equipos de computo para el desarrollo tanto de tareas sencillas como de alta complejidad , conjugando un alto desempeño y productividad.</p>
									</div>
									 </div>
								  </div>
								</div>
							  </div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headnine">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#sum-nine" aria-expanded="false" aria-controls="sum-nine">
										<i class="demo-icon icon-09"></i>
										<h4 class="panel-title">
									  Suministros de computo
										<span class="close-open pull-right"></span>
										</h4>
									</a>
								</div>
								<div id="sum-nine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headnine" aria-expanded="false">
								  <div class="panel-body">
									<div class="row">
									 <div class="col-xs-12 col-sm-5">
										<img src="images/somunistros-1.jpg" alt="" width="100%">
										</div>
									 <div class="col-xs-12 col-sm-7">
										<p>Comercializamos las mejores marcas del mercado, para brindarle a su empresa la solucion idonea en Hardware y equipos de computo para el desarrollo tanto de tareas sencillas como de alta complejidad , conjugando un alto desempeño y productividad.</p>
									</div>
									 </div>
								  </div>
								</div>
							  </div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headten">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#sum-ten" aria-expanded="false" aria-controls="sum-ten">
										<i class="demo-icon icon-10"></i>
										<h4 class="panel-title">
									  Telefonia IP
										<span class="close-open pull-right"></span>
										</h4>
									</a>
								</div>
								<div id="sum-ten" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headten" aria-expanded="false">
								  <div class="panel-body">
									<div class="row">
									 <div class="col-xs-12 col-sm-5">
										<img src="images/somunistros-1.jpg" alt="" width="100%">
										</div>
									 <div class="col-xs-12 col-sm-7">
										<p>Comercializamos las mejores marcas del mercado, para brindarle a su empresa la solucion idonea en Hardware y equipos de computo para el desarrollo tanto de tareas sencillas como de alta complejidad , conjugando un alto desempeño y productividad.</p>
									</div>
									 </div>
								  </div>
								</div>
							  </div>
						</div>
					</div>
				</div>
				
				<div class="in-formInfo">
					<div class="in-boxform">
						<h2>OBTÉN MÁS INFORMACIÓN</h2>
						<form>
							<div class="">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Nombre de empresa">
								  </div>
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Nombre">
								</div>
								<div class="form-group">
									<input type="email" class="form-control" placeholder="Email">
								</div>
								<div class="form-group">
									<input type="tel" class="form-control" placeholder="Teléfono">
								</div>
								<div class="form-group">
									<input type="tel" class="form-control" placeholder="Celular">
								</div>
								<div class="form-group">
									<textarea name="" id="" class="form-control"  placeholder="Cuéntenos su proyecto"></textarea>
								</div>

								<div class="checkbox">
								<label>
								  <input type="checkbox"> Acepto términos y condiciones
								</label>
							  </div>
								<div class="captcha">
									<img src="images/captcha.png" alt="" width="80%;">
								</div>
								<button type="submit" class="btn bto-orange">Enviar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			
			
			
		</div>
	</div>
	</div>


</section>
<?php include ('footer.php') ?>