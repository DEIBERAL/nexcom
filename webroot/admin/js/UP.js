
$(document).ready(function() {

	$(".submenu").hide();	
	$( ".submenu" );
		$('.submenu')
		//despliega solo el submenu de ese menu concreto
		$('.menu__enlace').click(function(event){
			var elem = $(this).next();

			if(elem.is('ul')){          
				event.preventDefault();
				elem.slideToggle();
			}
		});

});//fin de la funcion ready


/****** SINGLE *****/

$(document).ready(function(){
	$("#descripcioon").click(function () {	 
			$('.caption_desc').toggle("showOrHide");
			});
	    });



///***** Ir arriba 

$(document).ready(function(){
  
        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
                $('.ir-arriba').fadeIn();
            } else {
                $('.ir-arriba').fadeOut();
            }
        });
  
        $('.ir-arriba').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 600);
            return false;
        });
  
    });

//-******************* MAPA */
            function initialize() {
        
                var myLatlng = new google.maps.LatLng(4.7532983, -74.05254739999998);

                var mapCanvas = document.getElementById('map-canvas');
        
                var mapOptions = {
                    center: myLatlng,
                    zoom: 13,
                    zoomControl: true,
                    zoomControlOptions: {
                        style: google.maps.ZoomControlStyle.SMALL
                    },
                    panControl: false,
                    mapTypeControl: true,
                    scrollwheel: false,
                    styles: [
                        {
                            "featureType": "administrative",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#333333"
                                },
						{
						 "-webkit-text-stroke": "1.0px #333333"
						},
						{
						 "-webkit-text-fill-color": "#333333" 
						}
                            ]
                        },
                        {
                            "featureType": "landscape",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                }
                            ]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "on"
                                }
                            ]
                        },
                        {
                            "featureType": "poi.business",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "visibility": "on"
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "saturation": -100
                                },
                                {
                                    "lightness": 10
                                },
        //{
                                   //"color": "#CCCCCC"
                                //},
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "simplified"
                                }
                            ]
                        },
                        {
                            "featureType": "road.arterial",
                            "elementType": "labels.icon",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "transit",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "on"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "color": "#353535"
                                },
                                {
                                    "visibility": "on"
                                }
                            ]
                        }
                    ]
                };
        
                var map = new google.maps.Map(mapCanvas, mapOptions);
				
				var marcador = new google.maps.Marker({  
				  position: new google.maps.LatLng(4.6391922, -74.07107339999999),
				  map: map,
					title: 'Pulsa aquí'
					, icon: ''
				});
        
				new google.maps.Marker({
							position: myLatlng
							, map: map
							, title: 'Pulsa aquí'
							, icon: ''
							, cursor: 'default'
							
						}); 
				
			}
            google.maps.event.addDomListener(window, 'load', initialize);

