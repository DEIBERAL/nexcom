jQuery(document).ready(function() {
   $("ul li.menu-item-has-children:has(ul.sub-menu)").hover(
      function()
      {
         $(this).find('ul.sub-menu').css({display: "block"});
      },
      function()
      {
         $(this).find('ul.sub-menu').css({display: "none"});
      }
   );
	
	$("ul li.menu-item-has-children ul.sub-menu li.mod:has(ul.sub-level)").hover(
      function()
      {
         $(this).find('ul.sub-level').css({display: "block"});
      },
      function()
      {
         $(this).find('ul.sub-level').css({display: "none"});
      }
   );
	
	$(".navbar-nav li.notif:has(ul.sub-menu)").hover(
      function()
      {
         $(this).find('ul.sub-menu').css({display: "block"});
      },
      function()
      {
         $(this).find('ul.sub-menu').css({display: "none"});
      }
   );
	
});