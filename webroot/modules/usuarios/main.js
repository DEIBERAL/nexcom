
$(function(){
	if($('#Gest_Registro').length > 0){OBJ_Registro.INIT();}
	if($('#GestLogin').length > 0){OBJ_Login.INIT();}


})

var OBJ_Registro = {
	INIT:function(){
		console.log('Modulo Registro');
		this.EVENTS();
	},
	EVENTS:function(){
	
		$("#formRegistro").on('submit',OBJ_Registro.MODELS.registro);
	},
	MODELS:{
		
		registro:function(){
			
			var datos = {}
			
			datos.rol_id = $("#rol_id").val(),
			datos.nombre = $("#nombre").val(),
			datos.apellido = $("#apellido").val(),
			datos.cedula = $("#cedula").val(),
			datos.telefono = $("#telefono").val(),
			datos.ciudad = $("#ciudad").val(),
			datos.direccion = $("#direccion").val(),
			datos.correo = $("#correo").val(),
			datos.password = $("#password").val(),
			
			Ajax('services/Usuarios_serv/registrar_serv', datos, false, function(r){
				
					$('.resultados').html(r.msg);
					$('.resultados').removeClass('hidden');
				if(r.res){
					
					  $('.resultados').removeClass("alert alert-danger");
					    $('.resultados').addClass("alert alert-success");
					    OBJ_Registro.MODELS.redirec();
					
					$('.msg').text(r.msg).animate({ 'right' : '50' }, 300).fadeOut(5000).removeAttr('style');
				}else{
					$('.msg').text(r.msg).animate({ 'right' : '50' }, 300).fadeOut(5000).removeAttr('style');
				}
			});	
			
			return false;
		},
		
		redirec: function(){
			setTimeout(function(){ $('.resultados').html("Recargando página..."); setTimeout(function(){window.location.replace(BASE_URL+'productos/GestProductos/carrito');}, 1000);}, 2000);
		},
		
		
		
		
	},
	VIEWS:{
		
	}
};


var OBJ_Login = {
	INIT:function(){
		console.log('Modulo Login');
		this.EVENTS();
	},
	EVENTS:function(){
	
		$("#form_login").on('submit',OBJ_Login.MODELS.login);
	},
	MODELS:{
		
		login:function(){
			
			var datos = {}
			
			datos.correo = $("#correo").val();
			datos.password = $("#password").val();
			
			Ajax('services/Usuarios_serv/login_serv', datos, false, function(r){	
				$('.resultados').html(r.msg);
				$('.resultados').removeClass('hidden');
			if(r.dataObj.rol_id == 1){
					$('.resultados').removeClass("alert alert-danger");
					$('.resultados').addClass("alert alert-success");
					OBJ_Login.MODELS.redirectAdmin();	
					}else if (r.dataObj.rol_id == 2){
					    $('.resultados').removeClass("alert alert-danger");
					    $('.resultados').addClass("alert alert-success");
					    OBJ_Login.MODELS.redirectUser();	
						console.log("Bienvenido al panel administrativo")
					}
			});	
			console.log(datos);
			
	
			return false;
		},
		
		redirectAdmin: function(){
			setTimeout(function(){ $('.resultados').html("Recargando página Admin..."); setTimeout(function(){window.location.replace(BASE_URL+'panel/Home/admin');}, 1000);}, 2000);
		},
		redirectUser: function(){
			setTimeout(function(){ $('.resultados').html("Recargando página Cliente..."); setTimeout(function(){window.location.replace(BASE_URL+'panel/Home/cliente');}, 1000);}, 2000);
		},
		
		
	},
	VIEWS:{
		
	}
};







