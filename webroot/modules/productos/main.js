
$(function(){
	if($('#GestProductos').length > 0){OBJ_Productos.INIT();}
	if($('#Gest_Interna').length > 0){OBJ_Interna.INIT();}
	if($('#Ges_Carrito').length > 0){OBJ_CARRITO.INIT();}
	if($('#Gest_Pago').length > 0){OBJ_PAGO.INIT();}

})

var OBJ_Productos = {
	INIT:function(){
		console.log('Modulo Productos');
		this.EVENTS();
	},
	EVENTS:function(){
		
		$('.agregar').on('click', function(e){
			e.preventDefault();
			OBJ_Productos.MODELS.cargarCesta(this);	
		});
	},
	MODELS:{
		
		cargarCesta: function(This){
			
			var datos = {}
			datos.producto_id = $(This).data('id');
			Ajax('services/Cesta_serv/addCesta_serv', datos, false, function(resp){
				if(resp.res){
					$('.msg_'+datos.producto_id).text(resp.msg).animate({ 'right' : '0' }, 300).fadeOut(5000).removeAttr('style'); 
						setTimeout(function(){
							window.location.reload();
						}, 1500);
					
					}
				
			});	
		
		}
	},
	VIEWS:{
		
	}
};

var OBJ_Interna = {
	INIT:function(){
		console.log('Interna Productos');
		this.EVENTS();
	},
	EVENTS:function(){
	$("#ForCesta").on("submit",OBJ_Interna.MODELS.agregarCesta)
	},
	MODELS:{
		agregarCesta:function(){
			
			var datos = {}
			
			datos.producto_id = $("#ProdId").val();
			datos.cantidad    = $("#cantidad").val();
			console.log(datos);
			Ajax('services/Cesta_serv/addCesta_serv', datos, false, function(resp){
				if(resp.res){
					$('#msgCompra').text(resp.msg).animate({ 'right' : '150' }, 300).fadeOut(5000).removeAttr('style'); 
						
					}
				
			});	
			
			return false;
		}
	},
	VIEWS:{
		
	}
};

var OBJ_CARRITO = {
	INIT:function(){
		console.log('Carrito compras');
		this.EVENTS();
	},
	EVENTS:function(){
		$('.ActualizarCarrito').on('change',OBJ_CARRITO.MODELS.actualizarCarritoInterna);
	},
	MODELS:{
		actualizarCarritoInterna:function(){

			var datos = {};
			datos.producto = $('.tipoCantidad').serialize();
			console.log(datos);
			Ajax('services/Cesta_serv/cargaProd_serv', datos, false, function(resp){
				if(resp.res){
					   var iva = (resp.dataObj[1][1].iva);
					   var subtotal = (resp.dataObj[1][2].subtotal);
					   var total = (resp.dataObj[1][3].total);
					   //var totalProd = (resp.dataObj.total);
						console.log(subtotal);
						$("#subTotal").html('$ ' + subtotal.toLocaleString(['ban', 'id']));
						$("#iva").html('$ ' + iva.toLocaleString(['ban', 'id']));
						$("#total").html('$ ' + total.toLocaleString(['ban', 'id']));
						//$("#totalProd").html('$ ' + totalProd.toLocaleString(['ban', 'id']));
						
						
				}
				
			});	
			
			return false;
		}
	},
	VIEWS:{
		
	}
};

var OBJ_PAGO = {
	INIT:function(){
		console.log('Formulario Pago');
		this.EVENTS();
	},
	EVENTS:function(){
	
		$("#formPagos").on("submit",OBJ_PAGO.MODELS.gestPago)
	},
	MODELS:{
	
		gestPago: function(){
			
			var datos = {}
			
			datos.usuario_id = $("#usuario_id").val(),
			datos.nombre = $("#nombre").val(),
			datos.apellido = $("#apellido").val(),
			datos.empresa = $("#empresa").val(),
			datos.pais = $("#pais").val(),
			datos.direccion = $("#direccion").val(),
			datos.localidad = $("#localidad").val(),
			datos.region = $("#region").val(),
			datos.telefono = $("#telefono").val(),
			datos.correo = $("#correo").val(),
				
			Ajax('services/Pago_serv/registrarCompra_serv', datos, false, function(r){
				$('.resultados').html(r.msg);
					$('.resultados').removeClass('hidden');
					if(r.res){
						$('.resultados').removeClass("alert alert-danger");
						$('.resultados').addClass("alert alert-success");
						OBJ_PAGO.MODELS.redirect();
					}
				
				
			});	
			console.log(datos);
			
			return false;
		},
		redirect: function(){
			setTimeout(function(){ $('.resultados').html("Recargando página..."); setTimeout(function(){window.location.replace(BASE_URL+'productos/GestProductos/productos');}, 1000);}, 2000);
		},
	},
	VIEWS:{
		
	}
};






