// JavaScript Document
// JavaScript Document
function Ajax(urlSnd, datos, opt, cb) {
    if (!datos) {
        datos = {};
    }
    datos.TOKEN = TOKEN;
	var baseUrl = BASE_URL + urlSnd;
	if(opt){
		baseUrl = urlSnd;
	}
    $.ajax({
        url: baseUrl,
        type: "POST",
        dataType: 'json',
        data: datos,
        success: function (msg) {
            cb(msg);
        }
    });
}

function limpiarCampos($selector){
    
    $selector.find('input').val('');
    $selector.find('textarea').val('');
}

function msg(cont, texto, tipo) {
    $('.textomsg', cont).html(texto);
    $(cont).addClass('alert');
    if (tipo == 'error') {
        $(cont).addClass('alert-warning');
    } else if (tipo == 'complete') {
        $(cont).addClass('alert-success');
    }

    $(cont).fadeIn(1000).delay(1500).fadeOut(1000);
}
function cargo_pagina() {
    $("#contenidoGen").css({"display": "block", "font-size": "100%"});
    $("#cargando_pagina").css({"display": "none", "font-size": "100%"});
}
setTimeout(function () {
    cargo_pagina();
}, 10000);

function respData(resp){
	var data = {};
	if(resp.res){
		data.estado = 'complete';
		data.mensaje = resp.dataObj;
	}else{
		data.estado = 'error';
		data.mensaje = resp.dataObj;
	}
	return data;
}


const Formulario = (FormularioData) => {
    limpiarMensaje();
    $(FormularioData[0].elements).each(function(index, el){
        if ($(el).attr('tipo') == 'numerico') {
            validacionFormulario(el, 'numerico');
        }

        if ($(el).attr('tipo') == 'texto') {
            validacionFormulario(el, 'texto');
        }
    })
}


const validacionFormulario = (elemento, type) =>{
    let error = false;

    error = EsVacio($(elemento).val(), elemento);

    if (error) {
        return error;
    }

    if (type == 'numerico') {
        if (!$.isNumeric($(elemento).val())) {
            $(elemento).after('<span class="alert alert-danger mensaje">Debe ser númerico</span>');
            error = true; 
        }
    }

    if (error) {
        return error;
    }
}

const limpiarMensaje = () => $('.mensaje').remove();

const EsVacio = (valor, elemento) => {
    if (valor == '' || valor == undefined || !valor) {
        $(elemento).after('<span class="alert alert-danger mensaje">vacio</span>'); 
        return true;
    }
}

    $('.imagenLoad').on('change',function(event){
        var input = $(this);
        var reader = new FileReader();
        reader.onload = function(){
          var dataURL = reader.result;
          $(input).attr('data-src',dataURL);
        };
        reader.readAsDataURL($(this).context.files[0]);     
        return false;   
    })