<?php
/**
 * SuperBox Library
 *
 * Build your CodeIgniter pages much easier with partials, breadcrumbs, layouts and themes
 *
 * @package			CodeIgniter
 * @subpackage		Libraries
 * @category		Libraries
 * @author			Ing. Andres Castillo
 * @year			2016
 */

class SuperGrid2{
	
	var $customFields = array();
	var $rCampos = array();
	private $paramsPag =array();
	private $_ci = array();
	private $sResult = array('labels'=>'','campos'=>'');
	private $paramsGen = array();
	private $paramsPost = array();
	private $respuestaCrud = array();
	private $relacionInsert = array();	

	function __construct($config = array())
	{
		$this->_ci =& get_instance();
		if (!empty($config))
		{
			$this->initialize($config);
		}
	}
	public function setParamsGen($params=array(),$paramsP=array()){
		$this->paramsGen = $params;
		$this->paramsPost = $paramsP;
	} 
	public function setParamsPagi($val=array()){
		if(empty($val['p'])){$val['p']=1;}		// Pagina Actual
		if(empty($val['rxp'])){$val['rxp']=10;} // Resultados por pagina
		$this->paramsPag = $val;	
	}	
	public function addSource($name,$source){
		$this->customFields[$name] = $source;
	}
	public function relCampos($rCamp){
		$this->rCampos	= $rCamp;
	}
	public function addRowInfo($key,$label){
		$this->sResult[$key][] = $label;
	}
	public function manageResulset($iniQuery,$cb){
		$rxp = $this->paramsPag['rxp'];// Resultados por pagina
		$pgActual = $this->paramsPag['p'];// Pagina Actual
		$orden = $this->paramsGen['ordenList']; // Ordenar el resultado
		
		$paramsPag = $this->paramsPag;
		$allDatos = $this->_ci->db->query($iniQuery)->result_array();
		
		$this->paramsPag['allResults'] = count($allDatos);
		$this->paramsPag['tPages'] = ceil($this->paramsPag['allResults'] / $rxp);
		$this->paramsPag['rowIni'] = ($pgActual -1) * $rxp;
		if(!$this->paramsPag['rowIni']){$this->paramsPag['rowIni']=1;}
		
		$this->paramsPag['printVars'] = '||'.$rxp.'||'.$this->paramsPag['allResults'].'||'.$this->paramsPag['tPages'] ;
		
		
		$s = $this->paramsGen['busq'];
		$filbusq = $this->getCampoDeClave($this->paramsGen['filbusq']);
		$tipfilbusq = $this->paramsGen['tipfilbusq'];
		

		// AGREGAR WHERE
		$w = '';
		if($s || $this->paramsGen['filbusq'] || $this->paramsGen['valCampoFil']){
			$iniQuery  .= ' WHERE ';	
		}
		
		// FILTRAR
		if($this->paramsGen['filbusq']!='' && ($this->paramsGen['valCampoFil']!='' && $this->paramsGen['valCampoFil']!='null') ){
			$iniQuery .= ' '.$this->getCampoDeClave($this->paramsGen['filbusq']).' = "'.$this->paramsGen['valCampoFil'].'" ';	
		}
		
		// BUSCAR
		if($s){
			if($tipfilbusq==1){	$iniQuery .= ' '.$filbusq.' LIKE "%'.$s.'%" '; }
			if($tipfilbusq==2){	$iniQuery .= ' '.$filbusq.' LIKE "'.$s.'%" '; }
		}
		
		// ORDENAR
		if(!empty($orden)){
			$orden = explode('-',$orden);
			$iniQuery .= ' ORDER BY '.$this->getCampoDeClave($orden[0]).' '.$orden[1].'';
		}
		// LIMITAR
		$ini = (($pgActual - 1) * $rxp);
		$fin = $ini + $rxp;
		$iniQuery .=' LIMIT '.$ini.','.$rxp.'';
				
		
		$datos    = $this->_ci->db->query($iniQuery)->result_array();
		foreach($datos as $cl => $vl){ 
			$dataFormt = $cb($vl);
			$this->addRowInfo('campos',$dataFormt['campos']);
			$this->addRowInfo('labels',$dataFormt['labels']);
		}
	}
	public function outputInfo(){
		$response = array();
		$resultResorurce = array();
		
		if(is_array($this->sResult['campos']))
		foreach($this->sResult['campos'] as $cl => $vl){
			foreach($this->rCampos as $clC => $nameC){
				$nameC=empty($nameC)?$clC:$nameC;
				$resultResorurce[$cl][$clC] = $vl[$nameC];
			}
		}
		$this->sResult['campos'] = $resultResorurce;
		$response = $this->sResult;
		$response['rowIni'] = $this->paramsPag['rowIni']; // Numero del resultado en l que inicia
		$response['tRows'] = $this->paramsPag['allResults'];	// Cnatidad de registros del universo
		$response['rowsXpage'] = $this->paramsPag['rxp'];		// Filas por pagina
		$response['tPages'] = $this->paramsPag['tPages'];		// Total de paginas
		$response['cPage'] = $this->paramsPag['p'];		// Total de paginas
		$response['printVars'] = $this->paramsPag['printVars'];		// Total de paginas
		$response['custom']  = $this->customFields;
		$response['respuestaCrud'] = $this->respuestaCrud;
		$response['relacionInsert'] = $this->relacionInsert;
		
		return json_encode($response);
	}

	public function guardarDatos($info,$call){
	if(is_array($info)){
		foreach($info as $cl => $datos){
			if(is_numeric($cl)){
				$datos['ID_KEY'] = $cl;
			}else{
				$datos['ID_KEY'] = $cl;
			}
			$call($datos);
		}
	}
	}
	
	private function getCampoDeClave($clave){
		return $this->rCampos[$clave];
	}
	
	public function addRespuesta($idCamp,$res,$label,$mensaje){
		if(empty($label)){ $label = 'Nuevo'; }
		$this->respuestaCrud[$idCamp] = array($res,$label,$mensaje);	
	}
	
	/*
		Relcionar Inser con Row
		Al insertarse un nuevo resitro se relacionara con la row de la que  proviene
	*/
	public function addRelNuevo($kn,$idI){
		$this->relacionInsert[$kn] = $idI;	
	}	
	
	/*
		ES INSERTAR
		Valida  si los datos son para insertar o pára editar
	*/
	public function isInsert($edita){
		if(is_numeric($edita)){	return false;	}
		return true;
	}
	/*
		ELIMINAR
		Eliminar un item de la tabla
	*/
	public function delete($cb){
		
		if($this->paramsGen['deleteItem']){
			if(is_array($this->paramsPost['cDelete']))
			foreach($this->paramsPost['cDelete'] as $cl => $idDel){
				$cb($idDel);
			}
		}
	}
	
}

?>