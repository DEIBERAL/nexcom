(function(a){

	var $THIS;

	$.fn.supergridv2 = function (options) {
		$THIS = $(this);
 		var settings = $.extend({
            // These are the defaults.
            columnas: [],
			url:'',
			rxp:10,
			p:1,
			getData:'',			
        }, options );
		
		$THIS.data('settings',settings);
		$THIS.data('atributos',{
							   cDato:1, // Posicion inicial del puntero
							   renglonesSend:[],
							   resulsetAjax:[],// Datos  retornados por el servicio
							   stringBusq:'', // String de busqueda
							   campoFil:'',	  // Campo a filtrar
							   campoInfo:'',  // Valor seleccionado de un campo
							   tipFil:''	  // Tipo de filtro
		});
			
		// Inicializar  la tabla con la informacion cargada
		$THIS.loadDatapage(1);
		$THIS.Init();
	}
	
	$.fn.loadDatapage = function(page){
		var dataG = '?';
			dataG += 'rxp='+$THIS.data('settings').rxp;
			dataG += '&p='+$THIS.data('settings').p;
			dataG += '&busq='+$THIS.data('atributos').stringBusq;
			dataG += '&filbusq='+$THIS.data('atributos').campoFil;
			dataG += '&valCampoFil='+$THIS.data('atributos').campoInfo;
			dataG += '&tipfilbusq='+$THIS.data('atributos').tipFil;
			dataG += $THIS.data('settings').getData;
			
		$.ajax({
		  url: $THIS.data('settings').url+dataG,
		  type: "GET",
		  dataType: "json",
		  data: {},
		  success: function(result){
			  console.log(result);
			  	$THIS.data('atributos').resulsetAjax = result;
				//Pintar Tabla
				$THIS.drawTable(result);
				//Puntero
				$('#dato_1 input',$THIS).focus();
				$THIS.puntero();				
		  }
		});
	}
	
	$.fn.Init = function(){
		
		// Guardar Renglon
		$THIS.on('keyup','input',function(e){
			if(e.keyCode!=37 && e.keyCode!=38 && e.keyCode!=39 && e.keyCode!=40){
				var renglon = $(this).closest('tr').attr('idEdit');
				addRenglon(renglon);
			}
		})
		
		// Guardar Informacion
		$THIS.on('click','#GuardarInfo',function(){
			$THIS.Guardar();
		})

		$THIS.on('click','.pageSel',function(){
			var paginaSel = $(this).attr('rel');
			$THIS.data('settings').p = paginaSel;
			$THIS.loadDatapage(paginaSel);
			return false;
		})
		
		// Nuevo
		$THIS.on('click','#NuevoRegistro',function(){
			$THIS.find('.new:first').find('input:first').focus();
		})
		// Buscar
		$THIS.on('click','#selBusca',function(){
			$THIS.data('atributos').stringBusq = $('#buscar',$THIS).val();
			$THIS.data('atributos').campoFil = $('#campoFil',$THIS).val();
			$THIS.data('atributos').tipFil =  $('#tipFil',$THIS).val();
			$THIS.data('atributos').campoInfo =  $('#campoInfo',$THIS).val();
			$THIS.loadDatapage();
		})
		$THIS.on('change','#campoFil',function(){
			var keyCamp = $(this).val();
			var infoCustom  = $THIS.data('atributos').resulsetAjax.custom;
			var columnas = $THIS.data('settings').columnas;
			var vista = '';
			$.each(columnas,function(i,j){
				if(keyCamp == j.key && j.tipo == 'select'){
					console.log(infoCustom[j.key]);
					$.each(infoCustom[j.key],function(k,l){
						vista += '<option value="'+k+'">'+l+'</option>';
					});
					$('#campoInfo').fadeIn(100);
				}
			});
			if(!vista){	
				$('#campoInfo').fadeOut(100);
				$('#buscar').show();
				$('#campoInfo').html('');			
			}else{
				$('#buscar').hide();
				$('#campoInfo').html(vista);	
			}
			
		})
		// Eliminar
		$THIS.on('click','.boxSuperGrid #Eliminar',function(){
			if(confirm('Esta seguro que desea  eliminar los datos?')){
				//.eliminaRow
				var camposDel = $('.eliminaRow:checked');
				var eliminar = [];
				$.each(camposDel,function(i,j){
					eliminar[i] = $(j).val();	
				})
				$.ajax({
					  url: $THIS.data('settings').url+'?deleteItem=enable',
					  type: "POST",
					  dataType: "json",
					  data: {cDelete:eliminar},
					  success: function(result){	
							$.each(camposDel,function(i,j){
								$(j).closest('tr').remove();
							})
							messageSB('Items Eliminados con exito.');
					  }
				});
			}
		})
		
		// Eliminar
		$THIS.on('click','.boxSuperGrid #Eliminar',function(){
			
		})
		
		// Custom MultiSelect
		$THIS.on('click','.labelText',function(){
			$(this).parent('.headCtr').hide();
			var bodyS = $(this).closest('.containerMS').find('.bodyCtr');
			bodyS.show();
		})

		$THIS.on('click','.labelText',function(){
			$(this).parent('.headCtr').hide();
			var bodyS = $(this).closest('.containerMS').find('.bodyCtr');
			bodyS.show();
		})		

		$(window).on('keyup',function(e){
			if(e.which == 27){
				$('.headCtr').css('display','block');
				$('.containerMS').css('z-index','2');
				$('.bodyCtr').css('display','none');
			}
		})
		
	}
				
	$.fn.Guardar = function(){
		
		var renglonesSend = $THIS.data('atributos').renglonesSend;
		var theUrl  = $THIS.data('settings').url;
		var columnas  = $THIS.data('settings').columnas;
		var infoDatos = {};
				
		for(var i=0;i<renglonesSend.length;i++){
			var theKey = renglonesSend[i];
				infoDatos[theKey] = {};
				
				for(var a=0; a<columnas.length;a++){
					if(columnas[a].tipo=="file"){
						infoDatos[theKey][columnas[a].key] = $('#'+columnas[a].key+'_'+theKey).val();
					}else if(columnas[a].tipo=="multiselect"){
						infoDatos[theKey][columnas[a].key] = {};
						var idf = 1;
						$('#'+columnas[a].key+'_'+theKey+' input:checkbox:checked').each(function(i,j) {
							infoDatos[theKey][columnas[a].key][idf] = $(this).val();
							idf++;
						});	
					}else{
						infoDatos[theKey][columnas[a].key] = $('#'+columnas[a].key+'_'+theKey).val();
					}
				}
		}
		
//		console.log(renglonesSend);
//		console.log(infoDatos);
		
		$.ajax({
		  url: $THIS.data('settings').url,
		  type: "POST",
		  dataType: "json",
		  data: {sourceD:infoDatos,modePlug:'senDataCrud'},
		  success: function(result){
			// Set IDEDIT  to row
			$.each(result.relacionInsert,function(i,j){
				$('#_'+i).attr('idedit',j);
				var campoInput = $('#_'+i).find('input');

				$.each($(campoInput),function(k,l){
					var name = $(l).attr('name');
					var partname =  name.replace(/[0-9]/g, '');
					partname = partname.substring(0,partname.length-1);
					var newName = partname+j;
					$(l).attr('name',newName);
					$(l).attr('id',newName);
				})
				$('#_'+i).attr('id','_'+j);
				
				// Resaltar errores
			})
			//console.log(result.respuestaCrud,'<<<<<<<<<<<<');
			
			$.each(result.respuestaCrud,function(i,j){
				if(j[0]===false){
					$('#_'+i).css('background-color','red');
					$('#'+i).css('background-color','red');
				}else{
					$('#_'+i).css('background-color','green');
					$('#'+i).css('background-color','green');		
				}
			})
			var msg  = '<ul>';
			$.each(result.respuestaCrud,function(i,j){
				msg  += '<li class="liResult">';
					msg  += '<div class="clA">';
						msg  += j[1];
					msg  += '</div>';
					msg  += '<div  class="clB">';
						msg  += j[2];
					msg  += '</div>';
					msg  += '<div style:"clear:both;"></div>';
				msg  += '</li>';
			})
			msg  += '</ul>';
			// PoupUp de respuestas
			messageSB(msg);
		  }
		});
		/**/
		$THIS.data('atributos').renglonesSend = [];
	}	
	
	
	$.fn.drawTable = function(infoServ){
		var vista = '';
		var campos = infoServ.campos;
		var columnas = $THIS.data('settings').columnas;
		var theKey = $THIS.data('settings').primaryK;
		var idCamp = 1;
		var cPage = infoServ.cPage; // Pagina Actual
		var tPages = infoServ.tPages; // Total de paginas
		var nRow = 1;// Numero de la  fila
		var customCampos = infoServ.custom;
		var ancho = '';
		var vistaColsBus=''; // Columnas  del buscador para el select
		// Buscador
		$.each( $THIS.data('settings').columnas ,function(i,j){
			vistaColsBus += '<option value="'+j.key+'">'+j.titulo+'</option>';	
		})
		
		// Paginador
		var vistaPag = '<ul class="paginacion">';
		for(var i=1;i<=tPages;i++){
			if(parseInt(cPage)==i){
				vistaPag += '<li><a href="#" class="pageSel cPage" rel="'+i+'">'+i+'</a></li>';		
			}else{
				vistaPag += '<li><a href="#" class="pageSel" rel="'+i+'">'+i+'</a></li>';		
			}
		}
		vistaPag += '</ul><div style="clear:both"></div>';
		
		vista += '<div class="boxSuperGrid">';
				vista += '<div class="messageBox">';					
				vista += '</div>';
				
				vista += '<div class="cabezote">';
					vista += '<div>';
								vista += '<input type="button" value="Guardar" id="GuardarInfo" />';
								vista += '<input type="button" value="Nuevo" id="NuevoRegistro" />';
								vista += '<input type="button" value="Eliminar" id="Eliminar" />';
					vista += '</div>';
					vista += '<div class="buscar">';
								vista += '<input type="text" value="" name="buscar" id="buscar" placeholder="Buscar" />';
								vista += '<select name="campoFil" id="campoFil">'+vistaColsBus+'</select>';
								vista += '<select name="campoInfo" id="campoInfo"></select>';
								vista += '<select name="tipFil" id="tipFil"><option value="1">General</option><option value="2">Filtro</option></select>';
								vista += '<input type="button" name="selBusca" id="selBusca" value="BUSCAR">';
					vista += '</div>';
				vista += '</div>';
				
					vista += '<div class="scrollingtable">';
					vista += '	<div>';
					vista += '		<div>';
					vista += '			<table>';
					vista += '				<thead>';
					vista += '					<tr>';
					vista += '						<th width="1%"></th>';			
					$.each(columnas,function(i,propCol){
						ancho = propCol.w;
						vista += '						<th  width="'+ancho+'"><div label="'+propCol.titulo+'"/></th>';
					});
					vista += '						<th class="scrollbarhead"/>';
					vista += '					</tr>';
					vista += '				</thead>';
					vista += '				<tbody>';
					
					console.log(campos);
					console.log(nRow);
					for(var r = (($THIS.data('settings').p * $THIS.data('settings').rxp) - $THIS.data('settings').rxp) +1; r < ( $THIS.data('settings').p * $THIS.data('settings').rxp );r++){
						
						
						var idGuardado  = ''; // Obtener la primary Key del campo
						var isNew = '';				
						try{  idGuardado  = campos[nRow-1][theKey]; }catch(e){ idGuardado  = '_'+nRow; isNew = 'new'; }		
						
						vista += '<tr idEdit="'+idGuardado+'" id="_'+idGuardado+'" class="'+isNew+'">';
						if(isNew){
							vista += '<td><img src="img/remove.png" width="100%" class="deleteRow" style="cursor:pointer;display:none;" /></td>';
						}else{
							vista += '<td><input type="checkbox" id="elimina_'+idGuardado+'" class="eliminaRow" value="'+idGuardado+'" /></td>';
						}
		
						$.each(columnas,function(i,propCol){
							ancho = propCol.w;
							try{
								var valPred  = campos[nRow -1][propCol.key]; // Valor actual del campo
								var datosPreRow = campos[nRow -1]; // Datos precargados de la fila
								var vistaLoad   = $THIS.getCampoHTML(idGuardado,propCol,propCol.tipo,valPred,idGuardado,datosPreRow,customCampos);
								vista += '<td id="dato_'+idCamp+'" reg="'+idCamp+'">';
									vista += vistaLoad;
								vista += '</td>';
									idCamp++;	
							}catch(e){
								var vistaLoad   = $THIS.getCampoHTML(idGuardado,propCol,propCol.tipo,'',idGuardado,datosPreRow,customCampos);
									vista += '<td id="dato_'+idCamp+'" reg="'+idCamp+'">';
										vista += vistaLoad;
									vista += '</td>';
									idCamp++; 
							}
						});
						vista += '</tr>';			
						
						nRow++;
					}
					
					
					
					
					vista += '				</tbody>';
					vista += '			</table>';
					vista += '		</div>';
					vista += '	</div>';
					vista += '</div><!-- .scrollingtable -->';
		vista += '</div>  <!-- .boxSuperGrid -->';

		vista += vistaPag;
		
		$THIS.html(vista);
	}
	
	$.fn.getCampoHTML = function(idGuardado,propCol,tipo,valPred,idGuardado,datosPreRow,customCampos){
		if(tipo=='fijo'){ return valPred; }
		if(tipo=='texto'){ return '<input name="'+propCol.key+'_'+idGuardado+'" id="'+propCol.key+'_'+idGuardado+'" type="text" class="texto" style="border:none;" value="'+valPred+'">'; }
		if(tipo=='select'){ return controlSelectSimple(idGuardado,propCol.key,customCampos[propCol.source],valPred); }
		if(tipo=='multiselect'){return controlMultiSelect(idGuardado,propCol.key,customCampos[propCol.source],valPred);}
	}

			function controlSelectSimple(keyDb,nombre,iSource,valorPred){
				var sel = '';
				var vista = '';	
					vista += '<div class="containerSelect">';
						vista += '<select name="'+nombre+'[]" id="'+nombre+'_'+keyDb+'">';
						vista +='<option value="">Seleccione</option>';
							$.each(iSource,function(i,val) {
								if(valorPred==i){ sel = 'selected="selected"'; }else{ sel = '';	}
								vista +='<option value="'+i+'" '+sel+'>'+val+'</option>';
							});
						vista += '</select>';
					vista += '</div>';
				return vista;
			}
			
			function controlMultiSelect(keyDb,nombre,iSource,valorPred){
				if(valorPred){
					var nVal = [];
					$.map( valorPred, function( val, i ) { nVal[i] = val;});
					valorPred = nVal;
				}else{
					valorPred =[];	
				}
				var sel = '';
				var vista = '';	
					vista += '<div class="containerMS" id="'+nombre+'_'+keyDb+'">';
						vista += '<div class="headCtr">';
								vista +='<span class="labelText">Abrir</span>';
						vista += '</div>';
						vista += '<div class="bodyCtr">';
						vista += '<div class="contentCtrl">';
							$.each(iSource,function(i,val) {
								if(isValueArray(valorPred,i)){ sel = 'checked="checked"'; }else{ sel = '';	}
								vista +='<label><input type="checkbox" name="'+nombre+'_'+i+'" value="'+i+'" '+sel+' />'+val+'</label>';
							});
						vista += '</div>';
						vista += '</div>';
					vista += '</div>';
				return vista;
			}			
	
	$.fn.puntero = function(e){
		$($THIS).on('keyup',function(e){
			var cDato = $THIS.data('atributos').cDato;
			// Primer foco
			if(e.keyCode==39){
				cDato += 1;	
				$('#dato_'+cDato+' input',$THIS).focus();
			}
			if(e.keyCode==37){
				cDato -= 1;	
				$('#dato_'+cDato+' input',$THIS).focus();	
			}
			if(e.keyCode==38){
				var tCols = $THIS.data('settings').columnas.length;
				cDato -= tCols;	
				$('#dato_'+cDato+' input',$THIS).focus();	
			}	
			if(e.keyCode==40){
				var tCols = $THIS.data('settings').columnas.length;
				cDato += tCols;	
				$('#dato_'+cDato+' input',$THIS).focus();	
			}
			$THIS.data('atributos').cDato = cDato;			
		});				
	}

	function messageSB(texto){
		var vista = '';
			vista += '<div class="boxMensaje">';
				vista += '<div class="contenidoMSG">';
					vista += texto;
				vista += '</div>';
			vista += '</div>';
			$THIS.find('.messageBox').delay(500).fadeIn(500);
			$THIS.find('.messageBox').html(vista);
			$THIS.find('.messageBox').delay(2000).fadeOut(500);
	}
	
	function addRenglon(renglon){
		var info = $THIS.data('atributos').renglonesSend;
		if(!isValueArray(info,renglon)){
			info.push(renglon);
			$THIS.data('atributos').renglonesSend = info;
			console.log( $THIS.data('atributos').renglonesSend );
		}				
	}	

	function isValueArray(source,value){
			for(var i=0; i<source.length;i++){
				if(source[i]==value){
					return true;
				}	
			}
		return false;
	}
})(jQuery)