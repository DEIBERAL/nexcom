// JavaScript Document
function Ajax(urlSnd, datos, opt, cb) {
    if (!datos) {
        datos = {};
    }
    datos.TOKEN = TOKEN;
    $.ajax({
        url: BASE_URL + urlSnd,
        type: "POST",
        dataType: 'json',
        data: datos,
        success: function (msg) {
            cb(msg);
        }
    });
}

function limpiarCampos($selector) {

    $selector.find('input').val('');
    $selector.find('textarea').val('');
}

/*
 * Recibe como parámetro el selector del elemento o
 * todo el elmento html,  la condicion se activo = false o
 * desactivo = true, opcional el texto del btn que tendrá al terminar la petición
 */
function msj_espera_btn($btn, $discapacitado, $texto = false) {

    if ($discapacitado == true) {
        $texto = 'En proceso...';
    } else {
        if($texto != false){
            
        }else{
            $texto = 'Enviar';
        }
    }
    
    $btn.prop('disabled', $discapacitado);
    $btn.text($texto);
}

function msg(cont, texto, tipo) {
    $('.textomsg', cont).html(texto);
    $(cont).addClass('alert');
    if (tipo == 'error') {
        $(cont).addClass('alert-warning');
    } else if (tipo == 'complete') {
        $(cont).addClass('alert-success');
    }

    $(cont).fadeIn(1000).delay(1500).fadeOut(1000);
}
function cargo_pagina() {
    $("#contenidoGen").css({"display": "block", "font-size": "100%"});
    $("#cargando_pagina").css({"display": "none", "font-size": "100%"});
}
setTimeout(function () {
    cargo_pagina();
}, 10000);