function msg(cont,texto,tipo){
	$('.textomsg',cont).html(texto);
	$(cont).addClass('alert');
	if(tipo=='error'){
		$(cont).addClass('alert-warning');
	}else if(tipo=='complete'){
		$(cont).addClass('alert-success');	
	}
	
	$(cont).fadeIn(1000).delay(1500).fadeOut(1000);
}