<?php
class SuperGrid{
	
	var $customFields = array();
	var $rCampos = array();
	var $sResult = array();
	
	public function addCustomField($name,$source){
		$this->customFields[$name] = $source;
	}
	public function relCampos($rCamp){
		$this->rCampos	= $rCamp;
	}
	public function setResultSet($srl){
		$this->sResult = $srl;
	}
	public function outputInfo(){
		$response = array();
		$response = $this->sResult;
		$response['custom']  = $this->customFields;
		return json_encode($response);	
	}
	public function guardarDatos($info,$call){
	if(is_array($info)){
		foreach($info as $cl => $datos){
			if(is_numeric($cl)){
				$datos['ID_KEY'] = $cl;
			}else{
				$datos['ID_KEY'] = false;
			}
			$call($datos);
		}
	}
	}
	
}

function pre($info){
	echo "<pre>";
		print_r($info);
	echo "</pre>";	
}

/*
	Devolver los formatos
*/
function obtenerFormatos(){
	$resultSet = array();
	$resultSet['rowIni'] = 10;
	$resultSet['tRows'] = 20;	
	for($i=30;$i<40;$i++){
		$ia = $i + 5;
		$resultSet['campos'][] = array(
			'iddocumento'=>$ia,
			'codigo'=>7,
			'nombre'=>'Nom documento '.$ia,
			'descrip'=>'Descripcion del documento '.$ia,
			'usuarios'=>array(20,1),
			'tipos'=>array(7,8)
		);
		$resultSet['labels'][] = array(
			'usuarios'=>'Andres Castillo, Samanta..',
			'tipos'=>'Ventas,Asesores....'
		);
		
	}
	return $resultSet;		
}

function listarTablaLMD(){
		header('Content-type: application/json; charset=iso-8859-1');
		$SuperGrid = new SuperGrid();
		$SuperGrid->relCampos(array('iddocumento'=>'documento_id','nombre'=>'nomb','descrip'=>'descrip','usuarios'=>'usuariosCar','tipos'=>'tiposCar'));
		$SuperGrid->setResultSet(obtenerFormatos());
		$SuperGrid->guardarDatos(@$_POST['sourceD'],function($datos){
			crudFormat($datos);
		});
		$SuperGrid->addCustomField('usuarios',usuariosSistema());
		$SuperGrid->addCustomField('codigos',codigoSistema());
		$SuperGrid->addCustomField('tipos',tiposSistema());		
		echo $SuperGrid->outputInfo();
}
	
	/*
		CRUD FORMATO
		Guarda o edita la informacion de un formato
	*/
	function crudFormat($data=array()){	
		$codigo = @$data['codigo'];
		$nombre = @$data['nombre'];
		$descrip = @$data['descrip'];
		$usuarios = @$data['usuarios'];
		$tipos = @$data['tipos'];
		
	}

	function usuariosSistema(){
		return array(1=>'Andres Castillo',20=>'Juan Perez');
	}
	function tiposSistema(){
		return array(8=>'Proveedor',7=>'Empleado');	
	}
	function codigoSistema(){
		return array(5=>'CCB',7=>'C-170');	
	}
	

if(@$_GET['modeCall']=="ini" or !empty($_POST['modePlug'])){	listarTablaLMD();	}
	
	
	
?>