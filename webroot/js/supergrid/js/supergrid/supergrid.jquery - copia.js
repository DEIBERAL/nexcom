// JavaScript Document
(function ($) {
  	
	var $THIS;
	
	$.fn.supergrid = function (options) {
		$THIS = $(this);
 		var settings = $.extend({
            // These are the defaults.
            columnas: [],
			rxp:10,
			p:1,
			timeResp:5 * 1000, //Tiempo de cierre para el Box de respuesta
			tableWidth:100, //Tiempo de cierre para el Box de respuesta
			ordenList:''
        }, options );
		
		$(this).data('settings',settings);
		$THIS.data('renglonesSend',[]);
		/*
			Cargar Tablero
			Carga la interface del tablero acorde a las configuraciones
		*/
		function loadTablero(){
				$.ajax({
				  url: settings.url,
				  type: "GET",
				  dataType: "json",
				  data: {modeCall:'ini',rxp:settings.rxp,p:settings.p,ordenList:settings.ordenList},
				  success: function(infoPre){
					$THIS.data('infoPre',infoPre);
					$THIS.generarTabla(infoPre);			 
				  }
				});					
		}loadTablero();
		
		/*
			EVENTOS
		*/
		$(this).on('click','.pageSel',function(){
			var paginaSel = $(this).attr('rel');
			var st = $($THIS).data('settings');
			st.p = paginaSel;
			$THIS.data('settings',st);
			loadTablero();
			return false;
		})		
		
		$(this).on('click','.cargarFile',function(){
			var theObjLc = $(this);
			var Button = $(this).parent('.containerFile').find('.frameLoad').contents().find('#archivo');
			var Form = $(this).parent('.containerFile').find('.frameLoad').contents().find('#formFile');
			Button.trigger('click');
			$(Button).on('change',function(){
				$(Form).submit();
			})
			$(this).parent('.containerFile').find('.frameLoad').on('load',function(){
				var nameFile = $(this).contents().find('#nombreFile').val();
				$(theObjLc).parent('.containerFile').find('.nameFileLoad').val(nameFile);
				$(theObjLc).parent('.containerFile').find('.cargarFile').css('display','none');
				$(theObjLc).parent('.containerFile').find('.quitarFile').css('display','inline');
				$(theObjLc).parent('.containerFile').find('.verFile').css('display','inline');
				$(theObjLc).parent('.containerFile').find('.verFile').attr('href',settings.baseLoadFiles+'/'+nameFile);
			})
		})
		$(this).on('click','.quitarFile',function(){
			console.log('Quitando File');
			$(this).parent('.containerFile').find('.cargarFile').css('display','inline');
			$(this).parent('.containerFile').find('.quitarFile').css('display','none');
			$(this).parent('.containerFile').find('.verFile').css('display','none');
			$(this).parent('.containerFile').find('.verFile').attr('href','');
			$(theObjLc).parent('.containerFile').find('.nameFileLoad').val("");
		})		
		
		$(this).on('click','.containerMS',function(){
			$('.headCtr',this).css('display','none');
			$(this).css('z-index','20');
			$('.bodyCtr',this).css('display','block');
		})
		// Asignar renglon	
		$(this).on('keyup','.wrapperSuperGrid input',function(){
			var renglon = $(this).closest('tr').find('.renglon').val();
			addRenglon(renglon);
		});
		$(this).on('change','.wrapperSuperGrid select',function(){
			var renglon = $(this).closest('tr').find('.renglon').val();
			addRenglon(renglon);
		});
		$(this).on('change','.wrapperSuperGrid [type=date]',function(){
			var renglon = $(this).closest('tr').find('.renglon').val();
			addRenglon(renglon);
		});
		$(this).on('change','.bodyCtr [type=checkbox]',function(){
			var renglon = $(this).closest('.trRow').find('.renglon').val();
			addRenglon(renglon);
		});
			function addRenglon(renglon){
				var info = $THIS.data('renglonesSend');
				if(!isValueArray(info,renglon)){
					info.push(renglon);
					$THIS.data('renglonesSend',info);
				}				
			}
			
		$($(this)).on('click','.Nuevo',function(){
		})
		// GUARDADO
		$($(this)).on('click','.Guardar',function(){
			//var renglonesSend = [230,231];
			var renglonesSend = $THIS.data('renglonesSend');
			var theUrl  = $THIS.data('settings').url;
			var columnas  = $THIS.data('settings').columnas;
			
			var infoDatos = {};
			
			for(var i=0;i<renglonesSend.length;i++){
				var theKey = renglonesSend[i];
					infoDatos[theKey] = {};
					
					for(var a=0; a<columnas.length;a++){
						if(columnas[a].tipo=="file"){
							infoDatos[theKey][columnas[a].key] = $('#'+columnas[a].key+'_'+theKey+'_name').val();
						}else if(columnas[a].tipo=="multiselect"){
							infoDatos[theKey][columnas[a].key] = {};
							var idf = 1;
							$('#'+columnas[a].key+'_'+theKey+' input:checkbox:checked').each(function(i,j) {
								infoDatos[theKey][columnas[a].key][idf] = $(this).val();
								idf++;
							});	
						}else{
							infoDatos[theKey][columnas[a].key] = $('#'+columnas[a].key+'_'+theKey).val();	
						}
					}
			}
						
			$.ajax({
			  url: theUrl,
			  type: "POST",
			  dataType: "json",
			  data: {sourceD:infoDatos,modePlug:'senDataCrud'},
			  success: function(datos){
				/*
					RELOAD TABLERO
				*/
				$('.respCrud').fadeIn(200);	
					$THIS.VWresultadoCrud(datos.respuestaCrud);
				$('.respCrud').delay($THIS.data('settings').timeResp).fadeOut(1000,function(){
					loadTablero();
				});
			  }
			});
			
		})
		$($(this)).on('click','.Descartar',function(){

		})
		
		$(window).on('keyup',function(e){
			if(e.which == 27){
				$('.headCtr').css('display','block');
				$('.containerMS').css('z-index','2');
				$('.bodyCtr').css('display','none');
			}
		})
		return this;
    }
	
	$.fn.generarTabla = function (infoPre){
		var conf  = $(this).data('settings');
		var columnas  = $(this).data('settings').columnas;
		var table = ''; 
		var datosRow = {};// Datos para la presente fila
		var attrCol = '';// Atributos HTML 	
		// Pintar Registros	
		var nRow = infoPre.rowIni;	// Registro inicial
		var tRows = infoPre.tRows;	// Total de registros devueltos
		var rowsXpage = infoPre.rowsXpage;	// Total de registros devueltos
		var keyRowDB = '';			// Id del rgistro en la DB
		var valPred = '';			// Valor predeterminado del campo		
		var tPages = infoPre.tPages; // Total de paginas
		var rowsXpage = infoPre.rowsXpage; // Paginas por Pagina
		var cPage = infoPre.cPage; // Pagina Actual
		var iRow = 0;
		var anchoT = $THIS.data('settings').tableWidth;
		
		console.log('Ancho Table',anchoT);
		
		// Paginador
		var vistaPag = '<ul class="paginacion">';
		for(var i=1;i<=tPages;i++){
			if(parseInt(cPage)==i){
				vistaPag += '<li><a href="#" class="pageSel cPage" rel="'+i+'">'+i+'</a></li>';		
			}else{
				vistaPag += '<li><a href="#" class="pageSel" rel="'+i+'">'+i+'</a></li>';		
			}
		}
		vistaPag += '</ul><div style="clear:both"></div>';
		
		table += '<div class="wrapperSuperGrid"><form name="formTable" method="post" class="formTable"><div class="respCrud" style="display:none"></div>';
		table += '<div class="contOptions">';
			table += '<input type="button" class="Nuevo btn btn-primary" value="Nuevo"> ';
			table += '<input type="button" class="Guardar btn btn-success" value="Guardar"> ';
			table += '<input type="button" class="Descartar btn btn-danger" value="Descartar"> ';
			table += '<span class="alert alert-success" style="padding:5px"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>  Tienes (n) registros por guardar  ';
			table += '<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>  Andres,Gilberto y juan estan trabajando aqui tambien</span>';
		table +='</div>';// #Div.contOptions
				table += '<div class="tablaR"><table width="'+anchoT+'%" border="0" cellspacing="0" cellpadding="0" class="">';
					table += '<tr>';
						table += '<tr>';
							table += '<td  width="1%" class="hCol"><b>A</b></td>';
							for(var a=0; a<columnas.length;a++){
								if(columnas[a].w){ attrCol += 'width="'+columnas[a].w+'"';}
								table += '<td '+attrCol+' class="hCol"><b>'+obtenerLetra(a)+'</b></td>';
								attrCol = '';
							}						
						table += '</tr>';
						table += '<td class="colTit"><b class="tituloCol">No</b></td>';
						
						for(var a=0; a<columnas.length;a++){
							table += '<td  class="colTit"><b class="tituloCol">'+columnas[a].titulo+'</b></td>';
						}
					table += '</tr>';

				// Dibujar Filas		
				for(var i=0; i < ( parseInt(rowsXpage) + 5); i++){
					// Definir la Key del renglon
					try{
						if(typeof infoPre['campos'][iRow] != 'undefined'){
								keyRowDB = infoPre['campos'][iRow][conf.primaryK];
						}else{
								keyRowDB = '_'+i;	
						}
						var valorCampo = infoPre['campos'][iRow];
						var labelCampo = infoPre['labels'][iRow];
					}catch (e){
						keyRowDB = '_'+i;							
					}

					table += '<tr class="trRow">';						
						table += '<td class="hRow"> <input type="hidden" name="renglon[_'+i+']" class="renglon" id="renglon_'+i+'" value="'+keyRowDB+'" /> <b>'+nRow+'</b></td>';
						
						for(var a=0;a<columnas.length;a++){							
							if(typeof valorCampo != "undefined"){
								// Celda precargada
								valPred = valorCampo[columnas[a].key];		
									try{
										labelCampo = labelCampo[columnas[a].key];
									}catch (e){
										labelCampo = '';
									}
								table += '<td>'+$(this).obtenerCampo(keyRowDB,columnas[a].key,columnas[a].tipo,valPred,columnas[a].source,columnas[a],labelCampo)+'</td>';
							}else{
								table += '<td>'+$(this).obtenerCampo(keyRowDB,columnas[a].key,columnas[a].tipo,'',columnas[a].source,columnas[a],'')+'</td>';
							}
						}
						
					table += '</tr>';
				nRow++;
				iRow++;
				}
				table += '</table></div>';
				table += '<div class="contPaginacion">';
					table += vistaPag;
				table += '</div>';
				
				
		table += '</form></div>';
		
		console.log(table);
		$(this).html(table);
    }
	
		$.fn.obtenerCampo = function(keyDb,nombre,tipo,valorPred,source,objField,labelCampo){
			var customField =  $THIS.data('infoPre').custom;
			if(tipo=='texto'){ return '<input name="'+nombre+'[]" id="'+nombre+'_'+keyDb+'" type="text" class="texto" style="border:none;" value="'+valorPred+'">'; }
			if(tipo=='fijo'){ return '<div class="valPred">'+valorPred+'</div>'; }
			if(tipo=='fecha'){
				var info = new String(valorPred);
				valorPred = info.substr(0,10);
				return '<input type="date" name="'+nombre+'_'+keyDb+'" id="'+nombre+'_'+keyDb+'" value="'+valorPred+'">'; }
			if(tipo=='multiselect'){ return controlMultiSelect(keyDb,nombre,customField[source],valorPred,'',labelCampo);}
			if(tipo=='select'){ return controlSelectSimple(keyDb,nombre,customField[source],valorPred,''); }
			if(tipo=='file'){ return fileControl(keyDb,nombre,customField[source],valorPred,objField); }
			return '';
		}
		
		function obtenerLetra(i){
			i = i + 1;
			var letras = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','Ñ','O','P','Q','R','S','T','U','V','X','Y','Z']
			return letras[i];
		}
		
		/*
			CUSTOM CONTROLS
		*/
		function controlMultiSelect(keyDb,nombre,iSource,valorPred,objField,labelCampo){

			if(valorPred){
				var nVal = [];
				$.map( valorPred, function( val, i ) { nVal[i] = val;});
				valorPred = nVal;
			}else{
				valorPred =[];	
			}
			var sel = '';
			var vista = '';	
				vista += '<div class="containerMS" id="'+nombre+'_'+keyDb+'">';
					vista += '<div class="headCtr">';
							vista +='<span class="labelText">Abrir</span>';
					vista += '</div>';
					vista += '<div class="bodyCtr">';
						$.each(iSource,function(i,val) {
							if(isValueArray(valorPred,i)){ sel = 'checked="checked"'; }else{ sel = '';	}
							vista +='<label><input type="checkbox" name="'+nombre+'_'+i+'" value="'+i+'" '+sel+' />'+val+'</label>';
						});
					vista += '</div>';
				vista += '</div>';
			return vista;
		}
		
		function controlSelectSimple(keyDb,nombre,iSource,valorPred,objField){
			var sel = '';
			var vista = '';	
				vista += '<div class="containerSelect">';
					vista += '<select name="'+nombre+'[]" id="'+nombre+'_'+keyDb+'">';
					vista +='<option value="">Seleccione</option>';
						$.each(iSource,function(i,val) {
							if(valorPred==i){ sel = 'selected="selected"'; }else{ sel = '';	}
							vista +='<option value="'+i+'" '+sel+'>'+val+'</option>';
						});
					vista += '</select>';
				vista += '</div>';
			return vista;
		}
		function fileControl(keyDb,nombre,iSource,valorPred,objField){	
			var Iframe = $('#'+nombre+'_'+keyDb+'_2').contents();
			var nombreFile = $(this).parent('.containerFile').find('.frameLoad').contents().find('#nombreFile');				
			var sel = '';
			var vista = '';	
				vista += '<div class="containerFile" id="'+nombre+'_'+keyDb+'">';
					if(valorPred){
						var conf  = $THIS.data('settings');
						vista += '<input type="button" value="Cargar" class="cargarFile" style="display:none">';
						vista += '<input type="button" value="Quitar" class="quitarFile" style="display:inline">';
						vista += '<a href="'+conf.baseLoadFiles+'/'+valorPred+'" class="verFile" target="_blank">Ver</a>';	
					}else{
						vista += '<input type="button" value="Cargar" class="cargarFile">';
						vista += '<input type="button" value="Quitar" class="quitarFile" style="display:none">';
						vista += '<a href="#" class="verFile"  style="display:none" target="_blank">Ver</a>';	
					}
					vista += '<input type="hidden" id="'+nombre+'_'+keyDb+'_name" class="nameFileLoad" >';
					vista += '<iframe src="'+objField.url+'" width="0" height="0" frameborder="0" scrolling="no" class="frameLoad"  ></iframe>';
				vista += '</div>';
			return vista;
		}
		
		// VIEWS
		$.fn.VWresultadoCrud = function(Datos){
			var tipo = 'falseResp'; // Si la respuesta es verdadera o falsa
			var keyPu = '';   // Ente del renglon que lo caracteriza
			var mensaje = ''; // Mensaje que dio el CRUD
			var vista = '<h2 style="color:#fff">Respuesta de su consulta.</h2><br/>';
			
			//Importar Alto
			var altoCont = $('.wrapperSuperGrid').height();
						   $('.respCrud').css({'height':altoCont+'px',"background":"rgba(62, 69, 76, 0.78)"});
			var anchoCont = $('.wrapperSuperGrid').width();
						   $('.respCrud').css('width',anchoCont+'px');						   
			
			if(Datos.length<1){
				vista += '<p style="color:#fff">No se detectaron cambios.</p>';
			}else{
				vista += '<ul id="listadoRespuesta" class="list-group" style="width:90%;margin:auto">';
				$.each(Datos,function(i,v){
					tipo = v[0];
					keyPu = v[1];
					mensaje	 = v[2];
					if(tipo){tipo='list-group-item-success trueResp';}else{tipo='list-group-item-danger falseResp';}
					vista += '<li class="list-group-item '+tipo+'" ><span class="badge">'+keyPu+'</span>'+mensaje+' </li>';
				})
				vista += '</ul>';
			}
			$('.respCrud').html(vista);
		}

		/*
			--CUSTOM CONTROLS
		*/	
		/*
			Helpers
		*/
			function isValueArray(source,value){
					for(var i=0; i<source.length;i++){
						if(source[i]==value){
							return true;
						}	
					}
				return false;
			}
		/*
			--Helpers
		*/	
	
}(jQuery));