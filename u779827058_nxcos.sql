-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 19-12-2018 a las 21:46:54
-- Versión del servidor: 10.2.17-MariaDB
-- Versión de PHP: 7.0.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `u779827058_nxcos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE IF NOT EXISTS `categorias` (
  `categorias_id` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `padre` int(11) DEFAULT 0
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`categorias_id`, `nombre`, `padre`) VALUES
(1, 'Hardware de Cómputo', 0),
(2, 'Impresión y escáner', 0),
(3, 'Vídeo, Audio y Proyección', 1),
(5, 'Conectividad y redes', 1),
(6, 'Servidores y almacenamiento', 1),
(7, 'Software y aplicativos', 2),
(8, 'Potencia y protección electrica', 0),
(9, 'Seguridad Perimetral', 0),
(10, 'Suministros de computol', 0),
(11, 'Telefonía IP', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE IF NOT EXISTS `compras` (
  `compra_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `apellido` varchar(150) NOT NULL,
  `empresa` varchar(150) NOT NULL,
  `pais` varchar(150) NOT NULL,
  `direccion` varchar(150) NOT NULL,
  `localidad` varchar(150) NOT NULL,
  `region` varchar(150) DEFAULT NULL,
  `telefono` varchar(150) DEFAULT NULL,
  `correo` varchar(150) DEFAULT NULL,
  `valor_total` varchar(150) DEFAULT NULL,
  `tipo` varchar(150) DEFAULT '1',
  `descripcion` varchar(150) DEFAULT '1',
  `fechar` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `compras`
--

INSERT INTO `compras` (`compra_id`, `usuario_id`, `nombre`, `apellido`, `empresa`, `pais`, `direccion`, `localidad`, `region`, `telefono`, `correo`, `valor_total`, `tipo`, `descripcion`, `fechar`) VALUES
(85, 2, 'Maria Alejandra', 'Vargas Liz', 'Tata consulting', 'Colombia', 'calle 12 a', 'Medellin', 'xxxx', '313323', 'ale@gmail.com', '29491992.93', '1', '1', '2018-10-24 16:17:51'),
(86, 2, 'Marina', 'lopez hernandez', 'Tata consulting', 'Colombia', 'calle 12 a', 'Medellin', 'xxxx', '313323', 'ale@gmail.com', '605998.99', '1', '1', '2018-10-24 16:21:38'),
(87, 2, 'Maira', 'vargas', '1081418201', '3132233223', 'Bogota', 'calle 12 a', ' ale1317@gmail.com ', '1324', '1324', NULL, '1', '1', '2018-10-24 21:01:28'),
(88, 2, 'Maira', 'vargas', '', 'vargas', 'calle 12 a', '3132233223', 'vargas', ' ale1317@gmail.com ', '3132233223', NULL, '2', '1', '2018-10-24 21:40:07'),
(89, 2, 'Maira', 'vargas', '1081418201', '3132233223', 'Bogota', 'calle 12 a', ' ale1317@gmail.com ', '1324', '1324', NULL, '1', '1', '2018-10-24 22:15:19'),
(90, 2, 'Maira', 'vargas', '', '3132233223', 'vargas', 'calle 12 a', ' ale1317@gmail.com ', '3132233223', 'a@gmail.com', NULL, '2', 'quiero cotizar ', '2018-10-24 22:18:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `com_detalle`
--

CREATE TABLE IF NOT EXISTS `com_detalle` (
  `idcom_detalle` int(11) NOT NULL,
  `compra_id` int(11) NOT NULL,
  `producto_id` int(11) NOT NULL,
  `cantidad` int(20) NOT NULL,
  `valor` int(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `com_detalle`
--

INSERT INTO `com_detalle` (`idcom_detalle`, `compra_id`, `producto_id`, `cantidad`, `valor`) VALUES
(34, 85, 1, 7, 4199993),
(35, 85, 5, 5, 25000000),
(36, 86, 1, 1, 599999),
(38, 90, 2, 5, 250),
(39, 90, 2, 5, 250);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cotizaciones`
--

CREATE TABLE IF NOT EXISTS `cotizaciones` (
  `cotizaciones_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cotiz_detalles`
--

CREATE TABLE IF NOT EXISTS `cotiz_detalles` (
  `detalles_id` int(11) NOT NULL,
  `cotizaciones_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pqr`
--

CREATE TABLE IF NOT EXISTS `pqr` (
  `usuario_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE IF NOT EXISTS `productos` (
  `producto_id` int(11) NOT NULL,
  `categorias_id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `descripcion` varchar(250) DEFAULT NULL,
  `img1` varchar(250) DEFAULT NULL,
  `img2` varchar(250) DEFAULT NULL,
  `img3` varchar(250) DEFAULT NULL,
  `precio` varchar(45) DEFAULT NULL,
  `fechar` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`producto_id`, `categorias_id`, `nombre`, `descripcion`, `img1`, `img2`, `img3`, `precio`, `fechar`) VALUES
(1, 6, 'Portátil HP - 14-AX026LA - Intel Celeron N306', 'Disponibilidad: En existencia*', 'https://media.aws.alkosto.com/media/catalog/product/cache/6/image/660x441/69ace863370f34bdf190e4e164b6e123/1/4/14-ax026la_3_.jpg', 'https://media.aws.alkosto.com/media/catalog/product/cache/6/image/660x441/69ace863370f34bdf190e4e164b6e123/1/4/14-ax026la_2_.jpg', 'https://media.aws.alkosto.com/media/catalog/product/cache/6/image/660x441/69ace863370f34bdf190e4e164b6e123/1/4/14-ax026la_4_.jpg', '599999', '2018-10-09 19:08:27'),
(2, 5, 'PC All in One LENOVO - 520-22IKU - Intel Core', 'Mejoramos el diseño del computador Lenovo Todo en Uno AIO 520, con bordes ultra-delgados, un fino monitor FHD de 21.5” y un soporte curvo de acero. Así que ahora se verá mucho más elegante en tu oficina o en el estudio de tu casa', 'https://media.aws.alkosto.com/media/catalog/product/cache/6/image/660x441/69ace863370f34bdf190e4e164b6e123/1/9/192563037422_1.jpg', 'https://media.aws.alkosto.com/media/catalog/product/cache/6/image/660x441/69ace863370f34bdf190e4e164b6e123/1/9/192563037422_4.jpg', 'https://media.aws.alkosto.com/media/catalog/product/cache/6/image/660x441/69ace863370f34bdf190e4e164b6e123/1/9/192563037422_2.jpg', '69000', '2018-10-09 19:08:27'),
(3, 3, 'Convertible 2 en 1 LENOVO - YOGA330 - Intel C', 'Si tienes que estudiar, trabajar o simplemente quieres descansar viendo una película, este convertible 2 en 1 Lenovo tiene 4 modos de uso que se adaptan a todo lo que haces en un día sin perder ni un poco de potencia gracias a su disco duro de estado', 'https://media.aws.alkosto.com/media/catalog/product/cache/6/image/660x441/69ace863370f34bdf190e4e164b6e123/1/9/192330548960_4.jpg', 'https://media.aws.alkosto.com/media/catalog/product/cache/6/image/660x441/69ace863370f34bdf190e4e164b6e123/1/9/192330548960_13.jpg', 'https://media.aws.alkosto.com/media/catalog/product/cache/6/image/660x441/69ace863370f34bdf190e4e164b6e123/1/9/192330548960_17.jpg', '40000', '2018-10-09 19:08:27'),
(4, 2, 'PC All in One HP - 20-C217 - Intel Celeron - ', 'Con Magic Dekstop tus hijos aprenden, juegan', 'https://media.aws.alkosto.com/media/catalog/product/cache/6/image/660x441/69ace863370f34bdf190e4e164b6e123/i/m/img20-c217la_1.1_.jpg', 'https://images.modalia.com.co/images/articulos-nuevo/225/105130480003-906/3-original.jpg', 'https://images.modalia.com.co/images/articulos-nuevo/225/105130480003-906/4-original.jpg', '1390000', '2018-10-09 19:08:27'),
(5, 2, 'Portátil LENOVO - Idea320 - AMD A12 - 15.6" P', 'Comparte tu mundo a través de una pantalla de 15,6” con alta definición y mira tus películas favoritas con mucho más detalle que antes.', 'https://media.aws.alkosto.com/media/catalog/product/cache/6/image/660x441/69ace863370f34bdf190e4e164b6e123/1/9/191927756900_7.jpg', 'https://media.aws.alkosto.com/media/catalog/product/cache/6/image/660x441/69ace863370f34bdf190e4e164b6e123/1/9/191927756900_8.jpg', 'https://media.aws.alkosto.com/media/catalog/product/cache/6/image/660x441/69ace863370f34bdf190e4e164b6e123/1/9/191927756900_11.jpg', '5000000', '2018-10-09 19:08:27'),
(6, 2, 'Portátil ASUS - R420SA - Intel Celeron - 14" ', 'Camisetas para hombre, en tela unicolor, con bolsillo de parche al tono, y estampado frontal en contraste', 'https://media.aws.alkosto.com/media/catalog/product/cache/6/image/660x441/69ace863370f34bdf190e4e164b6e123/1/9/192545170499_3.jpg', 'https://media.aws.alkosto.com/media/catalog/product/cache/6/image/660x441/69ace863370f34bdf190e4e164b6e123/1/9/192545170499_2.jpg', 'https://media.aws.alkosto.com/media/catalog/product/cache/6/image/660x441/69ace863370f34bdf190e4e164b6e123/1/9/192545170499_3.jpg', '2500000', '2018-10-09 19:08:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_rol`
--

CREATE TABLE IF NOT EXISTS `tipo_rol` (
  `tiporol_id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_rol`
--

INSERT INTO `tipo_rol` (`tiporol_id`, `nombre`) VALUES
(1, 'Administrador'),
(2, 'Usuario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `usuario_id` int(11) NOT NULL,
  `rol_id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `cedula` varchar(45) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `ciudad` varchar(45) DEFAULT NULL,
  `direccion` varchar(45) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `fechar` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`usuario_id`, `rol_id`, `nombre`, `apellido`, `cedula`, `telefono`, `ciudad`, `direccion`, `correo`, `password`, `fechar`) VALUES
(2, 2, 'Alejandra', 'vargas', '1234565', '3134996368', 'medellin', 'calle 12a ', 'alejandra12vargasliz@gmail.com', 'bb7946e7d85c81a9e69fee1cea4a087c', '2018-10-10 19:52:43'),
(3, 2, 'Maira', 'vargas', '1081418201', '3132233223', 'Bogota', 'calle 12 a', ' ale1317@gmail.com ', 'bb7946e7d85c81a9e69fee1cea4a087c', '2018-10-10 23:37:23'),
(4, 2, 'Alejandra', 'vargas', '1234565', '3134996368', 'medellin', 'calle 12a ', 'alejandra12vargasliz17@gmail.com', '93fb9d4b16aa750c7475b6d601c35c2c', '2018-10-16 23:33:09'),
(5, 2, 'Alejandra', 'vargas', '1234565', '3134996368', 'medellin', 'calle 12a ', 'alejandra12vargasliz1212121212121212121212@gm', 'd41d8cd98f00b204e9800998ecf8427e', '2018-10-17 00:18:17'),
(6, 2, 'victor', 'manuel', '1234565', '3134996368', 'bogota', 'calle 12a ', 'victor.carrillo@360ms.co', 'e10adc3949ba59abbe56e057f20f883e', '2018-10-22 17:42:01');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`categorias_id`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`compra_id`),
  ADD KEY `fk_compras_usuarios1_idx` (`usuario_id`);

--
-- Indices de la tabla `com_detalle`
--
ALTER TABLE `com_detalle`
  ADD PRIMARY KEY (`idcom_detalle`),
  ADD KEY `fk_com_detalle_compras_idx` (`compra_id`),
  ADD KEY `fk_com_detalle_productos1_idx` (`producto_id`);

--
-- Indices de la tabla `cotizaciones`
--
ALTER TABLE `cotizaciones`
  ADD PRIMARY KEY (`cotizaciones_id`);

--
-- Indices de la tabla `cotiz_detalles`
--
ALTER TABLE `cotiz_detalles`
  ADD PRIMARY KEY (`detalles_id`),
  ADD KEY `fk_cotiz_detalles_cotizaciones1_idx` (`cotizaciones_id`);

--
-- Indices de la tabla `pqr`
--
ALTER TABLE `pqr`
  ADD PRIMARY KEY (`usuario_id`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`producto_id`),
  ADD KEY `fk_productos_categorias1_idx` (`categorias_id`);

--
-- Indices de la tabla `tipo_rol`
--
ALTER TABLE `tipo_rol`
  ADD PRIMARY KEY (`tiporol_id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`usuario_id`),
  ADD KEY `fk_usuarios_tipo_rol1_idx` (`rol_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `categorias_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `compra_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=91;
--
-- AUTO_INCREMENT de la tabla `com_detalle`
--
ALTER TABLE `com_detalle`
  MODIFY `idcom_detalle` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT de la tabla `cotizaciones`
--
ALTER TABLE `cotizaciones`
  MODIFY `cotizaciones_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cotiz_detalles`
--
ALTER TABLE `cotiz_detalles`
  MODIFY `detalles_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pqr`
--
ALTER TABLE `pqr`
  MODIFY `usuario_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `producto_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `usuario_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `compras`
--
ALTER TABLE `compras`
  ADD CONSTRAINT `fk_compras_usuarios1` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`usuario_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `com_detalle`
--
ALTER TABLE `com_detalle`
  ADD CONSTRAINT `fk_com_detalle_compras` FOREIGN KEY (`compra_id`) REFERENCES `compras` (`compra_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_com_detalle_productos1` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`producto_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cotiz_detalles`
--
ALTER TABLE `cotiz_detalles`
  ADD CONSTRAINT `fk_cotiz_detalles_cotizaciones1` FOREIGN KEY (`cotizaciones_id`) REFERENCES `cotizaciones` (`cotizaciones_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `fk_productos_categorias1` FOREIGN KEY (`categorias_id`) REFERENCES `categorias` (`categorias_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_usuarios_tipo_rol1` FOREIGN KEY (`rol_id`) REFERENCES `tipo_rol` (`tiporol_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
